# Thème 4 :  Algorithmique

1. [Extremums et moyennes](../01_chapitre_1/cours/)
2. [Complexité](../02_chapitre_2/cours/)
3. [Tri par insertion](../03_chapitre_03/cours/)
4. [Tri par sélection](../04_chapitre_4/cours/)
5. [Dichotomie](../05_chapitre_5/cours/)
6. [Algorithmes gloutons](../06_chapitre06/cours/)
7. [Algorithme KNN](../07_chapitre07/cours/)