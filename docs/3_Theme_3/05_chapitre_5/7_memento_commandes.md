
Ce chapitre est un extrait remis en forme d'un document dont l'auteur est Frédéric Junier. Celui-ci a précisé : 
*Memento directement inspiré des livres [La ligne de commande par
l’exemple](https://www.eyrolles.com/Informatique/Livre/la-ligne-de-commande-par-l-exemple-9782351410721/)
de Vincent Fourmond et [Parlez-vous Shell
?](https://www.eyrolles.com/Informatique/Livre/parlez-vous-shell--9782729877590/)
de Thomas Hugel.*

Dans ce memento, nous présentons des commandes du *shell*
[BASH](https://fr.wikipedia.org/wiki/Bourne-Again_shell) sous licence
libre, qui est le *shell* par défaut sur la plupart des distributions du
système d’exploitation libre
[Linux](https://fr.wikipedia.org/wiki/Linux). On distinguera parfois
fichiers et répertoires mais on rappelle que les répertoires sont juste
des fichiers spéciaux, qui contiennent d’autres fichiers. Un memento en
ligne est disponible sur <https://juliend.github.io/linux-cheatsheet/>.

## I. Généralités

Pour faciliter la saisie des commandes avec le
clavier, [BASH](https://fr.wikipedia.org/wiki/Bourne-Again_shell) offre
quelques raccourcis clavier bien pratiques :

???+ note dépliée "la touche de tabulation"

    La touche de tabulation permet d’appeler la complétion automatique
    qui propose de compléter la commande avec les choix possibles
    (fichiers ou commandes existants). Par exemple si on saisit `pw`,
    l’appui sur la touche de tabulation nous propose plusieurs commandes
    commençant par ce préfixe :

         junier@fredportable:~$ pw
         pwck      pwconv    pwd       pwdx      pwgen     pwunconv 


???+ note dépliée "Les flèches de direction Haut et Bas"

    Les flèches de direction Haut et Bas permettent de naviguer dans
    l’historique des commandes.


???+ note dépliée "documentation : `man`"

    La plupart des commandes du **shell** sont dotées d’une documentation
    accessible depuis l’interpréteur avec la commande `man`. Par exemple
    pour afficher l’aide de la commande `ls`, on écrira `man ls`.


!!! info "commande, arguments et options"

    Une commande **shell** est constituée du nom de la commande suivi d’un ou
    plusieurs arguments. Des options précédées peuvent modifier le
    comportement de la commande :

    nom_commande -option1 -option2 ... arg1 arg2 arg3 ... 


## II. Les commandes usuelles

!!! info "La commande `ls`"

    Ainsi, la commande `ls` permet d’afficher des informations sur
    répertoire ou un fichier :

    * Sans argument, ni option `ls` liste le contenu du répertoire courant :

          junier@fredportable:~/sandbox$ ls
          fichier1  fichier2  fichier3  fichier4  rep1  rep2  

    * Avec l’option `-l` elle affiche des informations détaillées sur chacun des fichiers contenus dans le répertoire :

          junier@fredportable:~/sandbox$ ls -l  
          total 8  
          -rw-rw-r-- 1 junier junier    0 août  16 21:43 fichier1  
          ...........  
          drwxrwxr-x 2 junier junier 4096 août  16 21:44 rep1  
          drwxrwxr-x 2 junier junier 4096 août  16 21:44 rep2  

   
    * Si on passe un répertoire en argument à la commande, elle affiche son contenu.

          junier@fredportable:~/sandbox$ ls -l rep1  
          total 0  
          -rw-rw-r-- 1 junier junier 0 août  16 21:44 photo1.jpg  
          -rw-rw-r-- 1 junier junier 0 août  16 21:44 photo2.jpg  


### Naviguer dans l’arborescence du système de fichiers

!!! info "La commande `pwd`"

    La commande `pwd` pour `print work directory` permet d’afficher le
    **répertoire courant dit de travail**. Le symbole tilde **`~`** est un
    raccourci pour désigner le répertoire personnel de l’utilisateur, en
    général `/home/utilisateur`.

         junier@fredportable:~/sandbox$ pwd
         /home/junier/sandbox


!!! info "La commande `cd`"

    La commande `cd` pour `change directory` permet de changer de
    répertoire courant.

    * Sans argument ou avec `cd ~` elle ramène l’utlisateur dans son
        répertoire personnel `/home/utilisateur`. `cd -` ramène dans le
        répertoire précédent

                  junier@fredportable:~/sandbox$ cd
                  junier@fredportable:~$ pwd
                  /home/junier
                  junier@fredportable:~$ cd -
                  /home/junier/sandbox

    * `cd ..` permet de remonter dans le répertoire parent :

                 junier@fredportable:~/sandbox$ pwd
                 /home/junier/sandbox
                 junier@fredportable:~/sandbox$ cd ..
                 junier@fredportable:~$ pwd
                 /home/junier

    * On peut fournir à `cd` un chemin absolu ou relatif mais il faut
        que le chemin soit uniquement constitué de répertoires !

                  junier@fredportable:~/sandbox$ cd /home/junier/sandbox/rep1
                  junier@fredportable:~/sandbox/rep1$ cd ..
                  junier@fredportable:~/sandbox$ cd rep1
                  junier@fredportable:~/sandbox/rep1$ cd photo1.jpg
                  bash: cd: photo1.jpg: N'est pas un dossier 


### Copier, supprimer, déplacer un fichier

!!! info "La commande `mv`"

    La commande `mv` pour `move` sert à déplacer ou renommer des
    fichiers ou des répertoires. Elle prend deux arguments *source* et
    *cible* : si la *cible* est un répertoire, alors la *cible* est
    copiée dedans sinon elle est renommée.

         junier@fredportable:~/sandbox$ ls
         fichier1  fichier2  fichier3  fichier4  rep1  rep2  rep3
         junier@fredportable:~/sandbox$ mv fichier1 fichier1-copie
         junier@fredportable:~/sandbox$ ls
         fichier1-copie  fichier2  fichier3  fichier4  rep1  rep2  rep3
         junier@fredportable:~/sandbox$ ls rep1
         photo1.jpg  photo2.jpg
         junier@fredportable:~/sandbox$ mv fichier1-copie rep1
         junier@fredportable:~/sandbox$ ls rep1
         fichier1-copie  photo1.jpg  photo2.jpg
         junier@fredportable:~/sandbox$ mv rep1 rep2
         junier@fredportable:~/sandbox$ ls rep2
         rep1  son1.jpg  son2.jpg


!!! info "La commande `cp`"

    La commande `cp` permet de copier des fichiers. Elle s’utilise comme
    `mv`, sauf que le fichier *source* n’est pas supprimé. Par défaut
    `cp` ne copie que des fichiers, pour copier un répertoire et son
    contenu, il faut lui passer l’option `-R` pour `recursive`.

         junier@fredportable:~/sandbox$ ls
         fichier2  fichier3  fichier4  rep2  rep3
         junier@fredportable:~/sandbox$ ls rep2
         rep1  son1.jpg  son2.jpg
         junier@fredportable:~/sandbox$ cp fichier2 rep2
         junier@fredportable:~/sandbox$ ls rep2
         fichier2  rep1  son1.jpg  son2.jpg
         junier@fredportable:~/sandbox$ cp rep2 rep3
         cp: -r non spécifié ; omission du répertoire 'rep2'
         junier@fredportable:~/sandbox$ cp -R rep2 rep3
         junier@fredportable:~/sandbox$ ls rep3
         rep2

!!! info "La commande `rm`"

    La commande `rm` permet de supprimer les fichiers qu’on lui passe en
    argument. Pour supprimer un répertoire et son contenu, il faut lui
    passer l’option `-R` comme pour `cp`. 
    
    🌵 **Attention, `rm` ne déplace pas les fichiers vers une corbeille, ils sont supprimés
    définitivement !**

         junier@fredportable:~/sandbox$ ls
         fichier2  fichier3  fichier4  rep2  rep3
         junier@fredportable:~/sandbox$ rm fichier2
         junier@fredportable:~/sandbox$ ls
         fichier3  fichier4  rep2  rep3
         junier@fredportable:~/sandbox$ rm rep3
         rm: impossible de supprimer 'rep3': est un dossier
         junier@fredportable:~/sandbox$ rm -r rep3
         junier@fredportable:~/sandbox$ ls
         fichier3  fichier4  rep2


## III. Gestion des droits sur les fichiers

!!! info "Lecture des droits"

    Considérons le contenu du répertoire `~/sandbox` affiché de façon détaillée avec la commande `ls -l` :

        junier@fredportable:~/sandbox$ ls -l
        total 8
        -rwxrw-r-- 1 junier junier    0 août  16 21:43 fichier3
        -rw-rw-r-- 1 junier junier    0 août  16 21:43 fichier4
        drwxrwxr-x 3 junier junier 4096 août  16 23:29 rep2
        drwxrwxr-x 2 junier junier 4096 août  16 23:33 rep3

    👀 Les 10 premiers caractère d’une ligne représentent les droits sur le
    fichier (ou le répertoire) :

    * Pour `fichier3` on a `-rw-rw-r--` :
        -   le premier caractère `-` indique qu’il s’agit d’un fichier
        -   le premier bloc de trois caractères `rwx` représente les
            droits pour le *propriétaire (u)* du fichier :
            *lecture (r)*, *écriture (w)* et *exécution (x)*.
        -   le second bloc de trois caractères `rw-` représente les
            droits pour le *groupe (g)* du fichier : *lecture (r)*,
            *écriture (w)* et un tiret `-` qui marque l’absence de droit
            d’exécution
        -   le dernier bloc de trois caractères `rw-` représente les
            droits pour les *autres (o)* utilisateurs du fichier : ce
            sont les mêmes que pour le *groupe*.

    * Pour `rep2` on a `drwxrwxr-x` :
        -   le premier caractère `d` indique qu’il s’agit d’un
            répertoire
        -   les trois blocs de trois caractères suivants énumèrent les
            droits en *lecture (r)*, *écriture (w)*, *exécution (x)* des
            trois types d’utilisateurs du répertoire : *propriétaire*,
            *groupe* et *autres*.

!!! info "La commande `chmod`"

    Le propriétaire d’un fichier ou le superutilisateur `root` peut
    changer les droits d’un fichier ou d’un répertoire avec la commande
    `chmod` dont la syntaxe est :

         chmod [-R] [ugoa][+-=][rwx] fichier

    * Les options entre crochets désignent :
        -   `u` : le propriétaire
        -   `g` : le groupe
        -   `o` : les autres utilisateurs
        -   `a` : tous les utilisateurs
        -   `+` : ajouter le(s) droit(s)
        -   `-` : enlever le(s) droit(s)
        -   `=` : fixer le(s) droit(s)
        -   `r` : droit de lecture
        -   `w` : droit d’écriture
        -   `x` : droit d’exécution
        -   `-R` : récursivement (nécessaire pour agir sur un
            répertoire)

    * Quelques exemples :

        -   Fixer les droits à w pour tous les utilisateurs sur `fichier3` :

                    junier@fredportable:~/sandbox$ chmod a=w fichier3

        -   Donner le droit d’écriture aux autres utilisateurs sur
            `fichier4`:

                    junier@fredportable:~/sandbox$ chmod o+w fichier4

        -   Enlever le droit de lecture à tous les utilisateurs sur
            `fichier4` :

                    junier@fredportable:~/sandbox$ chmod ugo-r fichier4

        -   Enlever le droit de lecture au groupe sur le répertoire
            `rep2` :

                    junier@fredportable:~/sandbox$ chmod -R g-w rep2

        -   Affichage des droits après les modifications précédentes :

                    junier@fredportable:~/sandbox$ ls -l
                    total 8
                    ---x--x--x 1 junier junier    0 août  16 21:43 fichier3
                    --w--w--w- 1 junier junier    0 août  16 21:43 fichier4
                    drwxrwxr-x 3 junier junier 4096 août  16 23:29 rep2
                    drwxrwxr-x 2 junier junier 4096 août  16 23:33 rep3

