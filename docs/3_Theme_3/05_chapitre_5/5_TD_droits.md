
## I. Prise en main 

Pour ce TP, nous allons utiliser un émulateur du système Linux qui nous donnera l’accès à une invite de
commande mis à disposition par l'université de la réunion :[Emulateur Linux](http://weblinux.univ-reunion.fr){ .md-button target="_blank" rel="noopener" }

Attendre d'obtenir ceci : 

![accueil](images/weblinux.PNG){ width=70% }



## II. Les groupes

!!! info "Les groupes"

	Un utilisateur fait partie de groupe(s), dont un principal (qui peut être attribué par défaut).

	En fait, le système d’exploitation l’identifie par un numéro (UID, identifiant d’utilisateur ou user ID) ainsi que ses groupes (par leur GID).

???+ question "La commande`id`"

    Utiliser la commande id afin de déterminer les groupes auxquels appartient alice .
	
	Noter les renseignements obtenus :

	* Numéro d’identification et son nom :
	* groupe principal :
	* groupes secondaires :

    ??? success "Solution"

    	![alice](images/alice_2.PNG){ width=70% }

    	* Numéro d’identification **uid** : 1000 et son nom : alice
		* groupe principal **gid**: 1000 et son nom : user
		* groupes secondaires **groups** : 1000 (user) et 1001 (developer)


???+ question "Remonter l'arborescence"

	Remonter d'un niveau, utiliser la commande pour voir où vous êtes.  
	Recommencer.

	??? success "Solution"

		![remonter](images/remonter.PNG){ width=20% }


???+ question "Le dossier `etc`"

	* Lister les dossiers et fichiers de la racine.

	* Utiliser les commandes `cat /etc/passwd` puis `cat /etc/group`.

	??? success "Solution"

		![groupes](images/groupes.PNG){ width=80% }

		On voit qu'en plus de root, il existe deux groupes user et developer auxquels alice et bob appartiennent.


## III. Gérer les droits

### Voir les droits

???+ question "Les droits"

	Aller dans `/home/alice` et exécuter `ls -l`

	??? success "Solution"

		![droits alice](images/droits_alice.PNG){ width=80% }

??? note pliée "Rappel sur les droits"

	Exemple : drwxr-xr-x

	|d|rwx|r-x|r-x|
	|:--:|:--:|:--:|:--:|
	|répertoire|u : propriétaire|g : groupe principal|o : autres|

	* le premier symbole d signifie que l’on a affaire à un répertoire. dans le cas d’un fichier, nous aurions un -
	* les 3 symboles suivants rwx donnent les droits du propriétaire du fichier : lecture autorisée (r), écriture autorisée (w), exécution autorisée ( x)
	* les 3 symboles suivants r-x donnent les droits du groupe lié au fichier : lecture autorisée (r), écriture interdite (- ), exécution autorisée ( x)
	* les 3 symboles suivants r-x donnent les droits des autres utilisateurs : lecture autorisée (r), écriture interdite (- ), exécution autorisée ( x)

	Pour plus de précisions, vous pouvez vous reporter au TP précédant.


???+ question "Les droits"

	Aller dans `/home/alice/Compagnon/A33`

	Détailler les droits pour `fichier1.txt`

	??? success "Solution"

		![A33](images/A33.PNG){ width=80% }

		Il s'agit d'un simple fichier. alice et le groupe principal ont les droits en lecture et écriture mais pas en exécution. Les autres ont les droits seulement en lecture.


### Modifier les droits

!!! abstract "A savoir"

	Le propriétaire d’un fichier(ou l’utilisateur "root") peut en modifier les droits avec `chmod`(change mode).  

	Seul lui peut le faire, ainsi que le « super-utilisateur » (ou administrateur système) root qui a les pleins pouvoirs sur la machine (ce qui est donc dangereux : on ne l’utilise que quand cela est strictement nécessaire)

!!! info "`chmod`"

	La syntaxe est `chmod modifications fichier`, où `modifications` est composé :

	* du public (une ou plusieurs lettres parmi u, g et o définis ci-dessus, voire a pour « tous », soit all), puis
	* d’un opérateur (= pour attribuer des droits et seulement ceux-là, + pour ajouter des droits à ceux déjà donnés et - pour en ôter à ceux qui existent) et enfin 
	* du ou des droits désignés par l’une ou plusieurs des lettres parmi r, w et x.


!!! example "Exemples"

	* chmod `o-wx mon_fichier` ôte les droits en écriture et en exécution aux « autres utilisateurs ».
	* chmod `a+x mon_fichier` donne les droits en exécution à tous les utilisateurs,
	
	👉 On peut même donner plusieurs séries d’attributs, séparées par des virgules.

	`chmod ug=rwx,o=r mon_fichier` donne tous les droits à l’utilisateur et au groupe, mais seulement le droit en lecture aux autres utilisateurs.

	
???+ question "A vous de jouer"

	* Ecrire la commande qui permet d’ajouter les droits en exécution au groupe et aux autres utilisateurs, pour `fichier1.txt` Vérifier le résultat après exécution.
	* Essayer ensuite d’autres modifications de droits sur ce fichier, et vérifier à chaque fois le résultat. Pour cela vous pouvez utiliser `ls -l fichier1.txt` . N’oubliez pas que la flèche vers le haut du clavier permet de rappeler les dernières commandes utilisées.

	??? success "Solution"

		![chmod](images/chmod.PNG){ width=80% }


???+ question "Créer un fichier"

	* Créer un fichier `test1` vide dans le répertoire d’alice. 
	* Noter les permissions de ce fichier.
	* Ecrire la commande pour que les "autres utilisateurs" aient la permission "écriture", puis vérifier

	??? success "Solution"

		![test1](images/test1.PNG){ width=80% }

		Pour le fichier `test1` : avant le changement alice avait les droits en lecture et écriture seulement, le groupe principal et les autres avaient les droiits seulement en lecture 


???+ note dépliée "Octal"

	Il existe une autre manière de décrire les droits à appliquer : le codage "octal". 

	Par exemple : 

	l’écriture "symbolique" : rwx r-x r-x  
	correspond à l’écriture binaire 111 101 101. (On met 1 quand il y a r, ou w, ou x, et 0 quand il y a -)

	Sur 3 bits les nombres vont de 0 (000 en binaire) à 7 = 4 + 2 + 1 (111 en binaire) . On peut donc considérer que l'on travaille en base 8 (d'où le nom octal)

	Il suffit de convertir chaque groupe de 3 bits en octal, pour obtenir la notation "octal"

	Ici 111 en binaire correspond à 4 + 2 + 1 = 7, 101 en binaire correspond à 4 + 1 = 5.

	L'écriture "symbolique" `rwx r-x r-x` correspond à `755` en "octal". C'est plus rapide à écrire


???+ question "Octal"

	Indiquer quels droits sont attribués par `chmod 644 fichier1.txt` puis vérifier.

	??? success "Solution"

		6  = 4 + 2 s'écrit en binaire sur 3 bits 110  
		4 s'écrit en binaire sur 3 bits 100  
		644 correspond donc à 110 100 100  
		C'est à dire à `rw- r-- r--`  

		![chmod 644](images/chmod644.PNG){ width=80% }

## IV. Compléments

### Lire un fichier

???+ question "Lire un fichier"

	Aller dans le répertoire `A33` Tester `cat` sur le fichier `fichier2.txt`. Que se passe-t-il ?

	??? success "Solution"

		Le fichier est trop long, on n'en voit que la fin, ce qui était au début a disparu de la console

???+ question "less"	

	`less` charge au fur et à mesure la zone du fichier qui est affichée, ce qui lui permet de démarrer
	très rapidement même avec des fichiers énormes.
	Pour quitter, il faut taper `q`

	Tester `less fichier2.txt`. Appuyez plusieurs fois sur la touche "Entrée", puis quitter.

	Il existe de nombreuses options possibles avec less.

### Aide

Le plus important à retenir, surtout quand on connaît une commande mais pas ses options, ou si
l’on découvre une commande inconnue dans un exemple, est de savoir accéder à l’information
intégrée au shell :

!!! info "man"
	* `man la_commande`  renvoie une aide complète (souvent plus longue qu’une page, on peut ajouter à la suite |less pour se déplacer dedans avec les flèches, puis quitter avec q. 
	* `info la_commande` offre un service comparable.

!!! info "help"

	Plus simplement, la plupart des commandes ont une option help(en général, double tiret pour une option qui s’écrit sur plus d’un caractère) ou -h qui décrit leur utilisation.

???+ question "Tester"

	* Tester `ls --help`
	* Trouver comment lister le contenu d’un répertoire en triant les éléments par ordre décroissant de taille.

	??? success "Solution"

		`ls -S`

		![tailles](images/tailles.PNG){ width=80% }









