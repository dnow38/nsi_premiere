
## exercice 1 :

![](images/arbo_1.PNG){ width=40% }

???+ question "Question 1"

    Pour connaitre le répertoire dans lequel je me trouve, quelle commande dois-je utiliser ?

    ??? success "Solution"

        `pwd`


???+ question "Question 2"

    Cette commande renvoie `/home/Zoé` et je suis Zoé. Pour créer un fichier `mon_texte.txt`, quelle commande dois-je utiliser ?

    ??? success "Solution"

        `touch mon_texte.txt`



???+ question "Question 3"

    Pour créer un répertoire `Photos` puis un fichier `listePhotos` dans ce répertoire, quelle commande dois-je utiliser ?

    ??? success "Solution"

        `mkdir Photos`

        `touch Photos/listePhotos`


???+ question "Question 4"

    Zoé a voulu faire cela, mais voici ce qu'elle à fait :

    ```
    mkdir Films
	cd Films
	touch listePhotos
	```

	Comme vous le voyez, elle s'est trompée, et veut maintenant déplacer le fichier `listePhotos` dans le répertoire `Photos`.

	Pour déplacer ce fichier sans le renommer, quelle commande Zoé doit-elle utiliser ?

    ??? success "Solution"

        `mv listePhotos ../Photos`


???+ question "Question 5"

	Zoé est donc toujours dans le répertoire `Film` et elle saisit `mkdir 2020`

	Elle veut maintenant copier le fichier `listePhotos` dans ce nouveau répertoire mais sous un nouveau nom : `listeFilms2020`. Quelle commande Zoé doit-elle utiliser ?

    ??? success "Solution"

        `cp ../Photos/listePhotos ./2020/listeFilms2020`


???+ question "Question 6"

	Malheureusement Zoé s'est trompée et elle a tapé : `cp ../Photos/listePhotos ~`. Ou a-t-elle copié le fichier ? Donner le chemin absolu

    ??? success "Solution"

        `/home/Zoé`


???+ question "Question 7"

	Comment peut elle effacer ce fichier maintenant (elle est toujours dans le répertoire `Film`) ? Donner le chemin relatif. 
	 
    ??? success "Solution"

        `rm ../listePhotos`


## Exercice 2

Eric travaille et le prompt indique : **~$**. il tape `ls -l` et obtient cette réponse :

`-rw-rw-r--       1      user     0      Apr 15 09:35 toto`

???+ question "Question 1"

    Peut-il lire ce fichier ? le modifier ? l'exécuter ?

    ??? success "Solution"

        Il peut le lire (r), le modifier (w), mais pas l'exécuter (pas de x)


???+ question "Question 2"

    Son amie Zoé qui n'est pas dans son groupe d'utilisateurs peut elle lire ce fichier ? modifier son contenu ?

    ??? success "Solution"

        Elle peut le lire (r), mais pas le modifier (pas de w).


???+ question "Question 3"

    Eric veux exécuter `toto`, il tape `./toto` et obtient ce message `-sh: ./toto: Permission denied`

    === "Cocher la ou les affirmations correctes"
        
        - [ ] Eric n'a pas la permission de voir le fichier
        - [ ] Eric n'a pas la permission d'exécuter le fichier
        - [ ] Eric n'est pas utilisateur root
        - [ ] Le fichier toto n'existe pas
        - [ ] Personne ne peut lire le fichier toto

    === "Solution"
        
        - :x: ~~Eric n'a pas la permission de voir le fichier~~
        - :white_check_mark: Eric n'a pas la permission d'exécuter le fichier
        - :x: ~~Eric n'est pas utilisateur root~~
        - :x: ~~Le fichier toto n'existe pas~~
        - :x: ~~Personne ne peut lire le fichier toto~~


???+ question "Question 4"

    a. Pour que toto devienne exécutable pour lui même, quelle commande Eric doit-il utiliser ?

    b. S'il veux que lui, mais aussi tout les autres utilisateurs, puissent aussi l'exécuter quelle commande Eric doit-il utiliser ?

    c. s'il veux le rendre exécutable pour lui et pour les utilisateurs de sont groupe, quelle commande Eric doit-il utiliser ?

    ??? success "Solution"

        a. `chmod u+x toto`

        b. `chmod a+x toto`

        c. `chmod ug+x toto`


## Exercice 3

Travailler le tutoriel sur Linux et les exercices sur Linux proposés par Thomas Castanet :

[Emulateur Linux chinginfo](https://chinginfo.fr/dossier/weblinux/){ .md-button target="_blank" rel="noopener" }

