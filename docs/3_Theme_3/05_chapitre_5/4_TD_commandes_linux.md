
## I. Prise en main et login

Pour explorer le monde de Linux, nous allons utiliser un émulateur du système Linux qui nous donnera l’accès à une invite de
commande [Emulateur Linux](https://chinginfo.fr/chapitre/dossier/weblinux){ .md-button target="_blank" rel="noopener" }
Lors du lancement, vous aurez un écran d’accueil de la forme suivante :

![nom image](images/invite.png){ width=80% }

Les différents utilisateurs et mot de passe pour la connexion **sont inscrits en haut de la page**.

Nous les rappelons ci-dessous :

|Utilisateur|alice|bob|eve|root|user|
|:--:|:--:|:--:|:--:|:--:|:--:|
| Mot de passe|alice22|bob2022|2eve2|admin2022|22user|

Pour nous identifier avec l’utilisateur alice :

* d’abord saisir le login alice
* valider le choix en appuyant sur “Enter”
* saisir le mot de passe alice22 (attention rien n’apparait à l’écran lors de la saisie du mot de passe)
* valider la saisie en appuyant sur “Enter”

👉 Nous voyons que l’authentification a réussi en observant la ligne `alice:~$ `

!!! info "`alice:~$ `"

	* alice : indique le nom de l’utilisateur courant
	* ~ : indique le répertoire courant. Ici “~” indique le répertoire personnel de l’utilisateur courant (ici, alice).
	* $ : permet de séparer le préambule, des instructions saisies par l’utilisateur.

## II. Explorer les dossiers

!!! info "Les commandes `cd` et `ls`"

	La commande `cd` (change directory) permet, comme son nom l’indique, de changer de répertoire.
	La commande `ls` (list) permet de lister le contenu du répertoire courant.

???+ question "Tester"

	Saisir `cd ..` (ne pas oublier l'espace avant `..`), puis saisir `ls`

	??? success "Solution"

		Vous devez obtenir : 
		
		![cd et ls](images/cd_ls.PNG){ width=50% }



* l’instruction “cd ..” permet de remonter d’un dossier dans l’arborescence des fichiers. Voila pourquoi le préfixe du terminal
passe de `alice :~$` à `alice :/home`

* Le dossier courant était `~`, c’est-à-dire `/home/alice`. En remontant d’un niveau, on arrive dans le dossier `/home`

* La commande `ls` liste le contenu du dossier courant qui est maintenant `/home`.  
Nous voyons que le dossier `/home` contient lui-même cinq autres dossiers (car affichés en bleu) :
`alice` ; `bob` ; `eve` ; `rep_a` ; `rep_b`  ; `user`

???+ question "Tester"

	Saisir `cd rep_b`, puis saisir `cd rep_a`
	Que remarquez-vous ?

	??? success "Solution"

		Vous devez obtenir : 
		
		![cd](images/cd_rep.PNG){ width=50% }

		Nous remarquons :

		* que l’utilisateur alice n’a pas les droits pour accéder au dossier `/home/rep_b`,
		* alors qu’elle peut accéder au dossier `/home/rep_a`.

## III. L'utilisateur root

!!! info "Le super utilisateur"

	Dans tous les systèmes d’exploitation Linux, il existe un utilisateur root, appelé également “super-utilisateur”, qui est l’administrateur du système d’exploitation.

	Il a pour particularité de pouvoir accéder à tous les fichiers, de changer les droits d’accès, les mots de passe de tous les utilisateurs.

	Son numéro d’identification est toujours 0.

	👉 On s’authentifie à l’aide de la commande `su` abbréviation de “super user” 


???+ question "Tester `su` "

	Saisir `su` 
	Le mot de passe demandé est `admin2022`
	Que remarquez-vous ?

	??? success "Solution"

		Nous remarquons que a l’invite de commande s’est modifiée. Particulièrement son préfixe :  
		A la place de `alice :/home/rep_a$` nous avons maintenant `root :~#`

???+ question "Tester `id` et `pwd`"

	Saisir `id` puis `pwd`
	Que remarquez-vous ?

	??? success "Solution"

		Vous devez obtenir : 
		
		![root](images/root.PNG){ width=50% }

		Nous remarquons :

		* que l’identifiant de l’utilisateur root est 0 (uid=0(root)) et que son groupe est le groupe root (groups=0(root)
		* son dossier personnel est un dossier nommé root situé à la racine / de l’arborescence de fichiers.

## IV. Gestion des utilisateurs

!!! info "Trouver les utilisateurs"

	Avec Linux, la plupart des configurations du système passe par des fichiers textes que seul l’utilisateur root a le droit de consulter et de modifier.

	👉 Pour la gestion des utilisateurs, ce sera le fichier `/etc/passwd` et pour la gestion des groupes d’utilisateurs, ce sera le fichier `/etc/group`


???+ question "Tester"

	Saisir `cd /etc` puis `ls`
	
	??? success "Solution"

		Vous devez obtenir : 
		
		![root](images/etc.PNG){ width=60% }

### Editer le fichier `passwd`

Nous allons éditer le fichier passwd à l’aide de l’instruction nano.

👉 Saisir `nano passwd`

Voici le contenu du fichier `/etc/passwd` affiché. 

👉 Modifier la fin de la ligne d’alice :

A la place de `/usr/bin/bash`  on veut  `/usr/bin/false`. Il faudra déplacer le curseur avec les flèches du clavier.

🌵 Cette modification empéchera à l’utilisateur alice de s’authentifier.

👉 Quitter le logiciel `nano` par la combinaison de touches Ctrl+X  
👉 Saisir Y (Yes pour sauvegarder la modification)  
👉 Appuyer sur la touche "entrée"

Vous devez obtenir ceci : 

![passwd](images/passwd.PNG){ width=70% }

### Les groupes d'utilisateurs

Avant de vérifier que l’effet de cette modification est immédiat pour l’utilisateur alice, nous allons afficher les groupes
d’utilisateurs utilisés par le système d’exploitation.

Ces informations sont contenues dans le fichier `/etc/group`. 

???+ question "Tester `cat`"

	Saisir `cat group` pour afficher le contenu du fichier directement dans le terminal.  
	Qu'obtenez-vous ?
	
	??? success "Solution"

		![groupes](images/group.PNG){ width=40% }

!!! info "Les groupes"

	* Chaque utilisateur (alice, bob, eve, user, root) possède un groupe à leur nom.
	* Il existe trois groupes : dont deux (eleves et professeurs) possèdent des utilisateurs , et un (nogroup) qui n'en possède pas.

### L'authentification d'alice

Maintenant, vérifions qu’alice ne peut plus s’authentifier dans le terminal.

!!! info "La commande `exit`" 

	La commande `exit` permet de fermer la session courante.


???+ question "Tester"

	Nous allons utiliser deux fois commande `exit` : une fois pour fermer la session de root, une seconde fois pour fermer celle d’alice.  
	Nous revenons ici à l’invite d’authentification.  
	Essayez de vous connecter en tant qu'alice. Qu'obtenez-vous ?
	
	??? success "Solution"

		![login alice](images/alice.PNG){ width=70% }

		Malgré nos tentatives, il n’est pas possible de s’authentifier sous l’utilisateur alice :

!!! warning "Remarque"

	Remarque: vous pouvez modifier tout ce que vous voulez sur cet émulateur. Il suffit de rafraîchir la page pour revenir à la
	configuration d’origine du système d’exploitation.


## V. Droits d'accès aux dossiers et fichiers

👉 Rafraichir la page et se connecter en tant qu'utilisateur alice

???+ question "accès aux dossiers"

	Saisir les commandes qui permettent d'accéder aux fichiers `rep_a` et `rep_b`.

	Que remarquez-vous ?
	
	??? success "Solution"

		![acces](images/acces_rectifie.PNG){ width=50% }

		* alice accède au dossier rep_a.
		* l’accès au dossier rep_b lui est refusé.

???+ question "accès aux fichiers"

	Saisir la commandes qui permet de savoir dans quel dossier vous êtes. Si nécessaire, saisir la commande pour aller dans le dossier `rep_a`
	Saisir la commande `touch fichierC.txt` qui permet de créer le fichier `fichierC.txt` dans le dossier 

	Que remarquez-vous ?
	
	??? success "Solution"

		![touch](images/fichierC.PNG){ width=50% }

		alice accède au dossier `rep_a` mais ne peut pas créer un nouveau fichier (nommé `fichierC.txt`) à l’aide de la commande `touch`.


### Les droits d'accès

👉 Saisir la commande `exit`, puis se connecter avec l'utilisateur eve.

???+ question "accès aux dossiers"

	Aller dans le dossier `rep_a`, puis créer le fichier `fichierC.txt` dans ce dossier.  
	Faire de même dans le dossier `rep_b`

	Que remarquez-vous ?
	
	??? success "Solution"

		![Droits eve](images/droits_eve.PNG){ width=80% }

		🌵 L’utilisateur eve a accès au dossier `rep_a` mais n’a pas le droit de créer un nouveau fichier.  
		😊 Par contre, elle peut accéder au dossier `rep_b` et y créer un nouveau fichier (nommé `fichierC.txt`) à l’aide de la commande `touch`.

### Les groupes

!!! info "La commande `id`"

	La commande id permet de connaitre les groupes auxquels appartiennent les utilisateurs.

???+ question "La commande `id`"

	Saisir `id eve`, puis `id alice`

	Que remarquez-vous ?
	
	??? success "Solution"

		![id](images/id.PNG){ width=80% }

		Nous pouvons déjà déduire :

		* le groupe eleves contient les deux utilisateurs alice et eve.
		* le groupe professeurs contient l’utilisateur eve mais pas l’utilisateur alice.

### Les droits et les utilisateurs

Revenons à ce que nous avons obtenu pour eve : 

![Droits eve](images/droits_eve.PNG){ width=70% }

La lettre `d` est utilisée pour un dossier (directory).  
Pour un fichier, cela commence par `-`

👉 On voit que le dossier rep_a est associé au groupe “eleves” : les utilisateurs alice et eve font partie de ce groupe.

👉 On voit que le dossier rep_b est associé au groupe “professeurs” : ce qui permet de justifier que les droits de alice et eve
sont différents.

!!! abstract "Résumé sur les groupes"

    Il existe 3 types d'utilisateurs pour un fichier ou un répertoire :

	* **u** 😊 le propriétaire du fichier (par défaut c'est la personne qui a créé le fichier), il est symbolisé par la lettre **u**

	* **g** 👩🏾‍🤝‍👩🏻👩🏼‍🤝‍🧑🏼 Un fichier est associé à un **groupe**, tous les utilisateurs appartenant à ce groupe possèdent des droits particuliers sur ce fichier.
	Le groupe est symbolisé par la lettre **g** 

	* **o** 🦔🐬 Tous les autres utilisateurs (ceux qui ne sont pas le propriétaire du fichier et qui n'appartiennent pas au groupe associé au fichier).
	Ces utilisateurs sont symbolisés par la lettre **o** (other).

	* **a** 😊 👩🏾‍🤝‍👩🏻👩🏼‍🤝‍🧑🏼  🦔🐬  (all) correspond à "tout le monde" (permet de modifier "u", "g" et "o" en même temps)

	👉 Il est possible d'utiliser la commande ls avec l'option ls -l afin d'avoir des informations supplémentaires


!!! abstract "Résumé sur les droits"

	* **r** signifie lecture autorisée (read)
	* **w** signifie écriture autorisée (write)
	* **x** signifie exécution autorisée (e**x**ecute)

??? note "Détails sur r, w et x"

	• **Lecture** (r pour read). Pour un fichier ordinaire, cela autorise la lecture du fichier, ce
	qui permet entre autre de le copier. Pour un répertoire, cela permet d’obtenir la liste de
	ses fichiers.

	• **Écriture** (w pour write). Pour un fichier ordinaire, cela permet de le modifier. Pour un
	répertoire, cela permet l’ajout, la suppression, le renommage des fichiers qu’il contient.

	• **Exécution** (x pour execute). Pour un fichier ordinaire cela indique qu’il est possible
	d’exécuter ce fichier (s’il s’agit d’un fichier exécutable). Pour un répertoire, cela autorise
	à se positionner dedans, par exemple avec cd).

!!! info "Créations"

	Lorsque l’utilisateur crée un fichier ou un répertoire, ce dernier lui « appartient » , ainsi qu’à son groupe principal.

!!! abstract "Lectures des droits"

	Pour lire les droits, il faur faire les regroupements par utilisateurs :

	![Droits](images/droits_pleins.PNG){ width=80% }


!!! example "Exemple"

	`-rwxr-xr--`

	![Droits](images/partiels.PNG){ width=80% }

	* Il s'agit d'un fichier (1er caractère à gauche : `-`)
	* Le propriétaire a les droits en lecture (r), écriture (w) et exécution (x)
	* Les utilisateurs du groupe ont les droits en lecture et en exécution
	* Les autres utilisateurs ont les droits en lecture


???+ question "Le répertoire `rep_b`"

	Pourquoi alice ne peut pas créer un fichier dans ce dossier alors qu'eve le peut ?

	??? success "Solution"

		On voit que le dossier rep_b est associé au groupe `professeurs`.
		`drwxrwx---` montre qu' eve qui appartient au groupe `professeurs` a tous les droits sur ce fichier (rwx).
		Par contre alice fait partie des autres utilisateurs (other). Elle n'a aucun droit.


### Changer les droits

!!! abstract "La commande `chmod`"

	La commande `chmod` a cette forme : `chmod [u g o a] [+ - =] [r w x] nom_du_fichier`


???+ question "Le droits"

	Ecrire la commande qui permet d'ajouter le droit en écriture à tous les autres utilisateurs pour rep_a.

	Que se passe-t-il ?

	??? success "Solution"

		`chmod o+w rep_a`

		L'opération n'est pas permise. En effet ce répertoire n'appartient pas à eve.
		
		Seul root peut le modifier.
			

		???+ question

			Essayer de modifier ces droits avec la commande `su`.

			??? success "Solution"

				* Commande `su` puis le mot de passe `admin2022`
				* Puis commande `chmod o+w rep_a`
				* vérifier la modification avec `ls -l`




