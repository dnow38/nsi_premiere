# Chapitre 3.5 - Introduction aux systèmes UNIX

![image](data/BO.png){: .center}

## 1. Le système d'exploitation

<iframe width="560" height="315" src="https://www.youtube.com/embed/SpCP2oaCx8A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

!!! info "Operating System : OS"

    Le système d’exploitation, souvent appelé OS  ( Operating System ) est un ensemble de programmes qui permet d’utiliser les composants physiques d’un ordinateur pour exécuter toutes les applications dont l’utilisateur aura besoin.

    Toute machine est dotée d’un système d’exploitation qui a pour fonction de charger les programmes depuis la mémoire de masse et de lancer leur exécution en leur créant des processus, de gérer l’ensemble des ressources, de traiter les interruptions ainsi que les entrées-sorties et enfin d’assurer la sécurité globale du système.

    *Exemples*

    Windows, Mac OS, Ubuntu Mate  sont des systèmes d’exploitation.

    !!!! danger "Attention"
        Linux n’est pas à  proprement parlé  un OS , c’est en fait le noyau de nombreux OS  que l’on appelle des distributions Linux comme par exemple Gentoo, Debian, Linux Mint, Ubuntu, Fedora, RedHat … 

!!! info "OS en bref"

    Les système d'exploitation font le Lien entre monde applicatif (les applications) et monde matériel (les composants)

??? note "Le monde matériel"

    * la mémoire
    * les processeurs (cerveaux, font des calculs)
    * péripéhriques E/S (imprimantes, écran, clavier, souris, internet...)

??? note "Le monde applicatif"

    * C'est l'ensemble des programmes qui sont en activité à un instant donné.
    * Dans un ordinateur, on a plusieurs applications qui s'exécutent simultanément. chacune exécute une tache.
    * Les applications n'accèdent pas aux éléments de l'ordinateur, elles communiquent seulement avec l'OS.

## 2. Arborescence

??? note pliée "Avant et aujourd'hui"

	À la "préhistoire" des systèmes d'exploitation, ces derniers étaient dépourvus d'interface graphique (système de fenêtres "pilotables" à la souris). Toutes les interactions "système d'exploitation - utilisateur" se faisaient par l'intermédiaire de "lignes de commandes" (suites de caractères, souvent ésotériques, saisies par l'utilisateur).

	Aujourd'hui, même si les interfaces graphiques modernes permettent d'effectuer la plupart des opérations, nous allons dans le cadre du cours de NSI apprendre à utiliser quelques commandes de base.

!!! info "Arborescence"

	Principalement nous allons, grâce à la ligne de commande, travailler sur les fichiers et les répertoires. Dans les systèmes de type "UNIX" (par exemple GNU/Linux ou macOS, BSD, FreeBSD etc...), nous avons un système de fichiers en arborescence :

	![arborescence](data/nsi_prem_base_linux_2.png){: .center width=50%}

	

	Dans le schéma ci-dessus on trouve des répertoires (noms entourés d'un rectangle,exemple : "home") et des fichiers (uniquement des noms "grub.cfg").

	On parle d'arborescence, car ce système de fichier ressemble à un arbre à l'envers. Comme vous pouvez le constater, la base de l'arbre s'appelle la racine de l'arborescence et se représente par un "/"

### Chemin absolu ou chemin relatif ?

!!! info "Chemin absolu ou relatif"

	Pour indiquer la position d'un fichier (ou d'un répertoire) dans l'arborescence, il existe 2 méthodes :

	* indiquer un chemin absolu
	* indiquer un chemin relatif.

!!! info "Chemin absolu"

	Le chemin absolu doit indiquer "le chemin" depuis la racine. Par exemple le chemin absolu du fichier `fiche.ods` sera : `/home/elsa/documents/fiche.ods`

	👉 Remarquez que nous démarrons bien de la racine / (attention les symboles de séparation sont aussi des /)


!!! info "Chemin relatif"

	Il est possible d'indiquer le chemin non pas depuis la racine, mais depuis un répertoire quelconque, nous parlerons alors de **chemin relatif**:

	Le chemin relatif permettant d'accéder au fichier `photo_1.jpg` depuis le répertoire `max` est : `images/photo_vac/photo_1.jpg`

	👉 Remarquez l’absence du / au début du chemin (c'est cela qui nous permettra de distinguer un chemin relatif et un chemin absolu).

	🌵 On peut aussi écrire : `./images/photo_vac/photo_1.jpg`

	**.** désigne l'emplacement courant (là ou on exécute une commande ou un code).


!!! info "Remonter d'un niveau `../`"

	Imaginons maintenant que nous désirions indiquer le chemin relatif pour accéder au fichier `gdbd_3.jpg` depuis le répertoire `photos_vac`. Comment faire ? Il faut "remonter" d'un "niveau" dans l'arborescence pour se retrouver dans le répertoire "images" et ainsi pouvoir repartir vers la bonne "branche". Pour ce faire il faut utiliser 2 points : `../ski/gdbd_3.jpg`

	Il est tout à fait possible de remonter de plusieurs niveaux dans l'arborescence : `../../` depuis le répertoire `photos_vac` permet de remonter dans le répertoire `max`

	
!!! info "Le répertoire `home`"

	Comme déjà évoqué plus haut, les systèmes de type "UNIX" sont des systèmes"multi-utilisateurs": chaque utilisateur possède son propre compte. Chaque utilisateur possède un répertoire à son nom, ces répertoires personnels se situent traditionnellement dans le répertoire `home`.  
	Dans l'arborescence ci-dessus, nous avons 2 utilisateurs : "max" et "elsa". Par défaut, quand un utilisateur ouvre une console, il se trouve dans son répertoire personnel : `/home/elsa` par exemple.

	👉 Ce répertoire `home` de l'utilisateur est noté `~`. Elsa peut, de tout répertoire ou elle se trouve, accéder à son fichier `~/documents/fiche.ods`


Voici l'arborescence d'un disque dur

![arborescence](data/nsi_prem_base_linux_2.png){: .center width=50%}

???+ question

    Donner le chemin absolu du fichiers `cat`

    ??? success "Solution"

         `/bin/cat`
        
???+ question

    Donner le chemin absolu du fichiers `rapport.odt`

    ??? success "Solution"

         `/home/elsa/documents/boulot/rapport.odt`

???+ question

    Supposons que j'ouvre un terminal dans `/home/elsa` et de là je veux accéder au fichier `rapport.odt`, quel sera le chemin relatif pour accéder à ce fichier ?

    ??? success "Solution"

    	`documents/boulot/rapport.odt`


???+ question

    Supposons que j'ouvre un terminal dans `photos_vac` et de là je veux accéder au fichier `gdbd_3.jpg`, quel sera le chemin relatif pour accéder à ce fichier ?

    ??? success "Solution"

    	`../ski/gdbd_3.jpg`

### La commande `cd`

!!! info "la commande cd (change directory)"

	La commande `cd` permet de changer le répertoire courant. Il suffit d'indiquer le chemin (relatif ou absolu) qui permet d'atteindre le nouveau répertoire. Par exemple (en utilisant l'arborescence ci-dessus) :

	* si le répertoire courant est le répertoire `elsa` et que vous voulez vous rendre dans le répertoire `documents`, il faudra saisir la commande :

		* `cd documents` : relatif
		* ou `cd /home/elsa/documents` (ou `cd ~/documents`) : absolu

	* si le répertoire courant est le répertoire `photos_vac` et que vous voulez vous rendre dans le répertoire `ski`, il faudra saisir la commande :

		* `cd ../ski` : relatif
		* ou `cd /home/max/images/ski`: absolu

	* si le répertoire courant est le répertoire `boulot` et que vous voulez vous rendre dans le répertoire `documents`, il faudra saisir la commande :
		* `cd ..` : relatif
		* ou `cd /home/elsa/documents` : absolu

## 3. Introduction aux commandes UNIX à partir du jeu Terminus


🌐 TD à télécharger : Fichier `Terminus.pdf` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/Terminus.pdf)


[Lien pour jouer](http://luffah.xyz/bidules/Terminus/){ .md-button target="_blank" rel="noopener" }


!!! warning "👉 Le symbole **~** au clavier"

    * Il se trouve sur la même touche que  <kbd>2 é ~</kbd>. Il faut appuyer sur la barre espace pour le faire apparaître.
    * En cas de problème : maintenir la touche <kbd>Alt</kbd> enfoncée et saisir `126`

??? tip "Aide pour l'intrigant levier"

    Saisir simplement : `./IntrigantLevier`

??? tip "Pour libérer l'enfant"

	Déplacer l'enfant kidnappé (utiliser l'autocomplétion à cause de l'orthographe du jeu `EnfantKidnapé`) dans la cave des Trolls. Attention à bien indiquer le bon chemin.

??? tip "Pour trouver le mot de passe"

	Si vous voulez gagner du temps, commencer par lire : 
	
	[Les jokers](https://www1.zonewebmaster.eu/serveur-debian-general/les-jokers-utilisation)
	{ .md-button target="_blank" rel="noopener" }

??? tip "Les mots de passe avec Linux"

	Lorsqu'on écrit un mot de passe (à la fin du jeu), on ne voit rien à l'écran. 
	C'est normal, il s'agit d'une sécurité Linux. Ecrire le mot de passe ("dans le vide") 
	puis appuyer sur la touche <kbd>Entrée</kbd>

