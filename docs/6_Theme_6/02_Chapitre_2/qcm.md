---
author: Fabrice Nativel Mireille Coilhac
title: Exercices
---

# QCM

???+ question "Question 1"

    Dans le programme Javascript ci-dessous, quelle est la notation qui délimite le bloc d'instructions exécuté à chaque passage dans la boucle while ?

    ```JavaScript
    var i=0
	while (i<10) {
    alert(i);
    i = i + 1;
	}
	alert("Fin")
	```

    === "Cocher la ou les affirmations correctes"
        
        - [ ] Le fait que les instructions soient encadrées entre { et }
        - [ ] Le fait que les instructions soient indentées de 4 caractères
        - [ ] Le fait que les instructions suivent le mot clé while
        - [ ] Le fait que les instructions suivent la parenthèse fermante )

    === "Solution"
        
        - :white_check_mark: Le fait que les instructions soient encadrées entre { et }
        - :x: ~~Le fait que les instructions soient indentées de 4 caractères~~
        - :x: ~~Le fait que les instructions suivent le mot clé while~~
        - :x: ~~Le fait que les instructions suivent la parenthèse fermante )~~


???+ question "Question 2"

    Quel mot clé permet de commencer la définition d'une fonction en Javascript ?

    === "Cocher la ou les affirmations correctes"
        
        - [ ] `def`
        - [ ] `return`
        - [ ] `var`
        - [ ] `function`

    === "Solution"
        
        - :x: ~~def~~
        - :x: ~~return~~
        - :x: ~~var~~
        - :white_check_mark: `function`

???+ question "Question 3"

    Parmi les balises HTML ci-dessous, quelle est celle qui permet à l'utilisateur de saisir son nom dans un formulaire en respectant la norme HTML ?

    === "Cocher la ou les affirmations correctes"
    
        - [ ] `<input type="text">`
        - [ ] `<input type="name">`
        - [ ] `<select/>`
        - [ ] `<form>`

    === "Solution"
        
        - :white_check_mark: `<input type="text">`
        - :x: ~~`<input type="name">`~~
        - :x: ~~`<select/>`~~
        - :x: ~~`<form>`~~


???+ question "Question 4"

    Parmi les propriétés suivantes d'une balise `<button>` dans une page HTML, laquelle doit être rédigé en Javascript ?

    === "Cocher la ou les affirmations correctes"
        
        - [ ] La propriété `name`
        - [ ] La propriété `id`
        - [ ] La propriété `onclick`
        - [ ] La propriété `type`

    === "Solution"
        
        - :x: La propriété `name`
        - :x: ~~La propriété `id`~~
        - :white_check_mark: La propriété `onclick`
        - :x: La propriété `type`

???+ question "Question 5"

    Quelle est la machine qui va exécuter un programme Javascript inclus dans une page HTML ?

    === "Cocher la ou les affirmations correctes"
        
        - [ ] La machine de l'utilisateur ou le serveur, suivant la confidentialité des données manipulées.
        - [ ] La machine de l'utilisateur ou le serveur, selon leurs disponibilités
        - [ ] Le serveur sur lequel est stocké la page HTML
        - [ ] La machine de l'utilisateur sur laquelle s'exécute le navigateur web

    === "Solution"
        
        - :x: ~~La machine de l'utilisateur ou le serveur, suivant la confidentialité des données manipulées.~~
        - :x: ~~La machine de l'utilisateur ou le serveur, selon leurs disponibilités~~
        - :x: ~~Le serveur sur lequel est stocké la page HTML~~
        - :white_check_mark: La machine de l'utilisateur sur laquelle s'exécute le navigateur web


## Crédits

Auteurs : Fabrice Nativel, Mireille Coilhac

