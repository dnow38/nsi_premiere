# Chapitre 6.2 - Le Javascript
![image](data/BO.png){: .center}


## 1. Présentation
JavaScript a été créé en 1995 par Brendan Eich. Il a été standardisé sous le nom d'ECMAScript en juin 1997 par Ecma International 
dans le standard ECMA-262.  

JavaScript est un langage de programmation principalement employé dans les pages web en complétement de HTML et CSS 
pour les rendre **interactives** (réaction de la page web aux interventions de l'utilisateur).  
Il permet d'écrire des scripts : c'est un langage interprété par le navigateur, coté client.


## 2. Intégrer le JavaScript
Pour être exécuté, le JavaScript doit être intégré à la page HTML. Il existe différentes méthodes pour intégrer le JavaScript, nous allons en étudier une : le code JavaScript est écrit dans un fichier avec l'extension .js (par exemple script.js). Il faut ensuite ajouter une ligne dans le code HTML afin de lier ce dernier au fichier `script.js` :

!! Example "intégrer le JavaScript"
	``` html
	<script src="script.js"></script>
	```

l'attribut `src` de la balise script doit être égal au chemin vers le fichier qui contient le JavaScript.

Voici un exemple de fichier HTML associé avec un fichier script.js :
!!! Example "HTML et  JavaScript"
	``` html
	<!doctype html>
	<html lang="fr">
    	<head>
        	<meta charset="utf-8">
        	<title>Le trio</title>
        	<link rel="stylesheet" href="style.css">
    	</head>
    	<body>
        		...
    	</body>
    	<script src="script.js"></script>
	</html>
	```
Comme vous pouvez le constater ci-dessus la balise script est placée après la balise body fermante (il est aussi possible de placer la balise script dans la partie head, mais ce choix peut entraîner certains problèmes si on ne prend pas certaines précautions, on préférera donc placer la balise script après le body).


## 3. Comparaison des syntaxes entre Python et JavaScript
On retrouve les mêmes structures de base dans tous les langages informatiques, mais la syntaxe varie.   
Comme beaucoup d'autres langages, la syntaxe de JavaScript provient du langage C (avec des spécificités).

### 3.1. Les commentaires en JavaScript
``` js
	y = 2*n;     // Ceci est un commentaire sur le reste de la ligne
	/* 
	Ceci est un commentaire
	sur plusieurs lignes
	*/
```

### 3.2. Les variables
Contrairement à python, les variables utilisées en JavaScript doivent être déclarées.  
``` js
var variable1, variable2 = "exemple"; 	// domaine d'existence limité à la fonction où elle sont déclarées.
let variable3;							// domaine d'existence limité au bloc où elle est déclarée.
const vitesseLumiere = 3e8;				// définit une constante (qu'on ne peut pas modifier par la suite).
variable2 = "exemple2";					// affectation d'une nouvelle valeur à variable2
```
De manière générale, on préfère l'utilisation de `let` ou `const` à l'utilisation de `var` qui date des tous débuts de JavaScript.  
!!! Example "Exemple d'utilisation de vairable"  
	``` html
		<p id="accueil"> </p>
		<script>
			let prenom = "Pierre";
			document.getElementById("accueil").innerHTML = "Bonjour <em>" + prenom + "</em>";
		</script>
	```  
	**Affichage correspondant** :  ![bonjour Pierre](data/bonjourPierreMini.png)

### 3.3. Autres exemples de syntaxe
<table>
<tr><th>python</th><th>JavasScript</th><th> Commentaires</th></tr>

<tr><td>
``` Python
	if blue:
		blue = false
	else:
		blue = true
```
</td><td>
``` js
	if (blue) {
		blue = false;
		}
	else {
		blue = true;
		}
```
</td><td>
<ul>
<li>Un bloc d’instructions est défini entre accolades.</li>
<li>Les conditions sont écrites entre parenthèses.</li>
<li>Toutes les instructions se terminent par un point virgule (pas obligatoire en JS mais obligatoire en C).</li>
<li>L’indentation n’est pas nécessaire pour l’interpréteur mais imposée pour permettre une bonne lecture du programme.</li>
</td></tr>
<tr><td> 
``` python
for n in range(10):
	y = 2*n
```
</td> <td>
``` js
for(let n=0 ; n<10 ; n++) {
	y = 2*n; 
	}
```
</td> <td>
La boucle `for` est constituée de 3 champs :
<ul> <li>1er champ : valeur de la variable de boucle avant d'entrer dans la boucle</li>
<li>2ème champ : condition pour rester dans la boucle</li>
<li>3ème champ : instruction exécutée à la fin de chaque boucle</li></ul>
</td> </tr>

<tr><td> 
``` python
liste = [1,3,5,9,12]
for n in liste:
	y = 2*n
```
</td> <td>
``` js
let tableau = [1,3,5,9,12] 
for (let n of tableau) {
	y = 2*n;
	}
```
</td> <td>
<ul> <li>En python on parle de <code>list</code>, contrairement à tous les autres langages où on parle de tableau (<code>Array</code>)</li>
<li>Utilisation de <code>in</code> en python, de <code>of</code> en JavaScript</li> </ul>
</td> </tr>
<tr><td> 
``` python
if True and not(False or True):
	print("coucou")
```
</td> <td>
``` js
if (true && !(false || true)) {
	console.log("coucou")
	}
```
</td> <td>

<ul> <li><code>true</code> et <code>false</code> sans majuscule en JavaScript</li>
<li>Les opérateurs booléens sont très différents</li></ul>
</td> </tr>
</table>
	
## 4. Le JavaScript pour répondre à des événements

### 4.1. Notion d'événements

Il est possible d'associer des événements aux différentes balises HTML, ces événements sont très souvent des actions réalisées pour l'utilisateur de la page :

- l'événement onclik correspond à un clic de souris (bouton gauche) sur un élément HTML donné
- l'événement onmouseover correspond au survol d'un élément HTML par le curseur de la souris

Il existe beaucoup d'autres événements, dans le cadre de ce cours nous utiliserons uniquement les deux cités ci-dessus.

### 4.2. Quelques exemples de détection d'évènements.

??? Note "Réaction aux évènements de la souris"
	| Evenements | Description | Exemple d'utilisation |
	| :---- | :----- | :----- |
	| `onclick` | Le clic de la souris sur un élément | `:::html <h1 onclick="this.innerHTML = 'Ooops!'">Click on this text!</h1> ` |
	| `ondblclick` | Le double-clic de la souris sur un élément | `:::html <h1 onclick="this.innerHTML = 'Ooops! Ooops!'">Click on this text!</h1> ` |
	| `onmouseover` | Le survol de la souris sur un élément | `:::html <div onmouseover="this.style.backgroundColor = 'lightblue'">`|
	| `onmouseout` | La sortie de la souris d'un élément | `:::html <div onmouseout="this.style.backgroundColor = 'yellow'">`|
	| `onmousedown` | Le maintien du clic appuyé sur un élément | `:::html <div onmousedown="this.style.color = 'white'">` |
	| `onmouseup` | Le relachement du clic de souris | `:::html <div onmouseup="this.style.color = 'blue'">` |


??? Note "Réactions aux évènements sur les `:::html <input type="text"/>`"
	| Evenements | Description | Exemple d'utilisation |
	| :---- | :----- | :----- |
	| `onfocus` | Positionnement du curseur dans le `input` | `:::html <input type="text" onfocus="this.style.border='solid 2px maroon"> ` |
	| `onselect` | Sélection du contenu de la saisie | `:::html <input type="text" onselect="this.style.color = 'red'"> ` |
	| `onblur` | Sortie du champ du `input` | `:::html <input type="text" onblur="this.style.backgroundColor = 'grey'">` |
	| `onkeydown` | Le maintien d'une touche clavier appuyée | `:::html <div onkeydown="this.style.borderStyle = 'dashed'">` |
	| `onkeyup` | Le relachement de la touche clavier | `:::html <div onkeyup="this.style.borderStyle = 'double'">` |
	| `onchange` | Changement d'option sur une liste déroulante | [Voir l'exemple](https://www.w3schools.com/JS/tryit.asp?filename=tryjs_events_dropdown) |

??? Note "Réactions au chargement de pages ou d'images"
	| Evenements | Description | Exemple d'utilisation |
	| :---- | :----- | :----- |
	| `onload` | Quand la page (ou l'image) est chargée | `:::html <body onload="checkCookies()"> ` |
	| `onerror` | lors d'une erreur de chargement d'une image | [Voir l'exemple](https://www.w3schools.com/JS/tryit.asp?filename=tryjs_events_onerror) |
	| `onunload` | Lors de la fermeture de la page | [Voir l'exemple](https://www.w3schools.com/JS/tryit.asp?filename=tryjs_events_onunload) |
	| `onresize` | Changement d'option sur une liste déroulante | [Voir l'exemple](https://www.w3schools.com/JS/tryit.asp?filename=tryjs_events_onresize) |


!!! Example "Exemple d'évements  "
	**Source HTML**
		```html
		<!DOCTYPE html>							
		<html>									
			<head>								
				<meta charset="utf-8" />						
				<title>Les évènements en Javascript</title>	
			</head>								
			<body>	
				<div  id="bloc_page" 
					onmouseover="this.style.backgroundColor = 'yellow'" 
					onmouseout="this.style.backgroundColor = 'lightblue'" 
					onmousedown="this.style.color = 'white'" 
					onmouseup="this.style.color = 'blue'">
					   
					<h1 onclick="this.innerHTML = 'Ooops!'" 
						ondblclick="this.innerHTML = 'Ooops! Ooops!'">Mon super site</h1>
						
					<p>Zone de saisie de type <code> input type="text"</code> : </p>					
					
					<input type="text" 
						onfocus="this.style.border='solid 4px maroon'" 
						onselect="this.style.color = 'red'" 
						onfocusin="this.style.backgroundColor = 'white'"
						onblur="this.style.backgroundColor = 'lightgrey'"  
						onkeydown="this.style.borderStyle = 'dashed'" 
						onkeyup="this.style.borderStyle = 'dotted'">				
				</div>
			</body>								
		</html>
		```
	**Rendu du navigateur**  
		Réaliser les différents évènements de souris et de clavier sur les différents éléments `<div>`, `<h1>` et `<input>`.
		<html>
				<div  id="bloc_page" style="background-color:lightgreen; padding:15px"
					onmouseover="this.style.backgroundColor = 'yellow'" 
					onmouseout="this.style.backgroundColor = 'lightblue'" 
					onmousedown="this.style.color = 'white'" 
					onmouseup="this.style.color = 'blue'">
					<h1 onclick="this.innerHTML = 'Ooops!'" 
						ondblclick="this.innerHTML = 'Ooops! Ooops!'">Mon super site</h1>
					<p>Zone de saisie de type <code> input type="text"</code> : </p>					
					<input type="text" style="border: 2px solid black;"
						onfocus="this.style.border='solid 4px maroon'" 
						onselect="this.style.color = 'red'" onfocusin="this.style.backgroundColor = 'white'"
						onblur="this.style.backgroundColor = 'lightgrey'"
						onkeydown="this.style.borderStyle = 'dashed'"
						onkeyup="this.style.borderStyle = 'dotted'">
				</div>



### 4.3. Réponse à un événement
Un clic de souris vient d'être effectué sur un élément HTML, que va-t-il se passer ?

Dans la plupart des cas rien...si le développeur n'a pas prévu de gérer cet événement, il ne se passera rien.

Comment gérer un événement ?

Il suffit d'ajouter un attribut à la balise HTML concernée, prenons un exemple :

```html
<button onclick="maFonction()">Cliquer ici</button>
```

Nous avons ici une balise button qui possède un attribut onclick. Cet attribut onclick est égal à `maFonction()`. maFonction est une fonction défini dans le fichier JavaScript qui est associé à la page HTML (par exemple le fichier `script.js`). En cas de clic sur le bouton défini ci-dessus, la fonction JavaScript `maFonction` sera exécutée.

### 4.4. Les fonctions en JavaScript

Comment écrire une fonction en JavaScript ?

Il suffit d'utiliser le mot clé `function` suivi du nom de la fonction :
		
```CSS
function Mafonction(){
    .....
}

```
Les limites de la fonction sont définies par l'accolade ouvrante et l'accolade fermante.

### 4.5. Etudions un exemple : Modifier un élément HTML avec le JavaScript

En cas de clic sur le bouton défini ci-dessus, la fonction JavaScript maFonction sera exécutée, mais qu'est-il possible de faire avec le JavaScript ?

Aujourd'hui, presque tout : il est possible de coder des applications extrêmement complexes en JavaScript. Nous allons rester modestes, nous allons uniquement modifier un élément HTML.

Pour modifier un élément HTML il est tout d'abord nécessaire de le sélectionner : c'est le rôle des `querySelector`.

Considérons le code HTML suivant :

```html
<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Le trio</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <h1>Le trio : HTML, CSS et JavaScript</h1>
    <p id="monPara">Voici une page web qui ne fait pas grand chose</p>
    <button onclick="maFonction()">Cliquer ici</button>
</body>
<script src="script.js"></script>
</html>
```

et le code JavaScript suivant (fichier `script.js`) :
```JS
function maFonction() {
let maBalise = document.querySelector("#monPara");
}

```
Le mot clé let permet de définir une variable, dans l'exemple ci-dessus, la variable a pour nom `maBalise` (on aurait évidemment pu choisir un autre nom). Cette variable `maBalise` va nous permettre de manipuler l'élément HTML qui a pour `id monPara` (c'est-à-dire la balise `p` présente dans le code HTML ci-dessus).
dans le cas ci-dessus nous remplacerons le texte présent dans la balise `p` ("Voici une page web qui ne fait pas grand chose") par "coucou".

Autre exemple :

```JS
function maFonction() {
let maBalise = document.querySelector("#monPara");
maBalise.style.color="red";
}
```
dans le cas ci-dessus, le texte du paragraphe sera écrit en rouge.

Il est aussi possible de modifier les classes CSS associées à une balise HTML, prenons un exemple :

soit le code HTML :

```html
<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Le trio</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <h1>Le trio : HTML, CSS et JavaScript</h1>
    <p id="monPara">Voici une page web qui ne fait pas grand-chose</p>
    <button onclick="foncRouge()">Rouge</button>
</body>
<script src="script.js"></script>
</html>

```
soit le code CSS (fichier "style.css") :

```CSS
h1{
	text-align: center;
}
.rou {
    color: red;
}
```
soit le code JavaScript (fichier "script.js") :

```JS
function foncRouge() {
let maBalise = document.querySelector("#monPara");
maBalise.classList.add("rou");
}	
```
Résumons ce qui peut se passer avec la page HTML ci-dessus :

- un utilisateur clique sur le bouton Rouge, comme un événement `onclick` est associé à ce bouton, la fonction JavaScript `foncRouge` est donc exécutée

- la variable maBalise correspond à la balise `p` du code HTML `(let maBalise = document.querySelector("#monPara");)`

- on associe ensuite cette balise `p` à la classe CSS rouge : le texte contenu dans la balise `p` devient donc rouge `(maBalise.classList.add("rou");)`

!!! example "Exercice 1"
	=== "Enoncé"
    	- dans votre répertoire de travail, créez un fichier `index.html`, un fichier `style.css` et un fichier `script.js`

    	- copiez-collez le code suivant dans le fichier `index.html` qui vient d'être créé

		```html
		<!doctype html>
		<html lang="fr">
    	<head>
        	<meta charset="utf-8">
        	<title>Le trio</title>
        	<link rel="stylesheet" href="style.css">
    	</head>
 		<body>
        	<h1>Le trio : HTML, CSS et JavaScript</h1>
        	<p id="monPara">Voici une page web qui ne fait pas grand-chose</p>
        	<button onclick="foncRouge()">Rouge</button>
        	<button onclick="foncVert()">Vert</button>
    	</body>
    	<script src="script.js"></script>
		</html>
		```
		- copiez-collez le code suivant dans le fichier `style.css` qui vient d'être créé

		```CSS
		h1{
    		text-align: center;
		}
		.rouge {
    		color:red;
    		font-size:20px;
		}
		.vert {
    		color:green;
    		font-size:30px;
		}
		```

		- copiez-collez le code suivant dans le fichier `script.js` qui vient d'être créé

		```JS
		function foncRouge() {
    		let balp= document.querySelector("#monPara")
    		balp.classList.remove("vert");
    		balp.classList.add("rouge");
		}
		function foncVert() {
    		let balp= document.querySelector("#monPara")
    		balp.classList.remove("rouge");
    		balp.classList.add("vert");
		}
		```
		Après avoir testé l'exemple ci-dessus en cliquant sur fichier index.html, apportez les modifications nécessaires pour que les boutons rouge et vert permettent de modifier la couleur du titre `h1` et double sa taille (40 px)

	=== "Correction"
		```html
		<!doctype html>
		<html lang="fr">
    	<head>
        	<meta charset="utf-8">
        	<title>Le trio</title>
        	<link rel="stylesheet" href="style.css">
    	</head>
 		<body>
        	<h1 id="montitre">Le trio : HTML, CSS et JavaScript</h1>
        	<p >Voici une page web qui ne fait pas grand-chose</p>
        	<button onclick="foncRouge()">Rouge</button>
        	<button onclick="foncVert()">Vert</button>
    	</body>
    	<script src="script.js"></script>
		</html>
		```
		

		```CSS
		h1{
    		text-align: center;
		}
		.rouge {
    		color:red;
    		font-size:40px;
		}
		.vert {
    		color:green;
    		font-size:40px;
		}
		```


		```JS
		function foncRouge() {
    		let balp= document.querySelector("#montitre")
    		balp.classList.remove("vert");
    		balp.classList.add("rouge");
		}
		function foncVert() {
    		let balp= document.querySelector("#montitre")
    		balp.classList.remove("rouge");
    		balp.classList.add("vert");
		}
		```

!!! example "Exercice 2"

	=== "Enoncé"
		Créez la page web suivante :

		![image](data/c24a_1.png){: .center}

		si l'utilisateur clique sur le bouton Changer, le texte COUCOU ! devient rouge, si on appuie de nouveau sur le bouton Changer, le bouton redevient noir (si on appuie de nouveau sur le bouton, COUCOU ! redevient rouge...)
	=== "Correction"

		```html
		<!doctype html>
		<html lang="fr">
    	<head>
        	<meta charset="utf-8">
        	<title>Le trio</title>
        	<link rel="stylesheet" href="style.css">
    	</head>
    	<body>
        	<h1 id="montitre">COUCOU !</h1>
      
        	<button onclick="foncChang()">Changer</button>

    	</body>
    	<script src="script.js"></script>
		</html>
		```
		

		```CSS
		.rouge {
   			 color:red;

		}
		.noir {
    		color:black;
		}
		```


		```JS
		let indic=0;
		function foncChang() {
    		let balp= document.querySelector("#montitre")
			if (indic==0)
				{
				balp.classList.remove("noir");   
				balp.classList.add("rouge");
				indic=1;
				}
			else
				{
				balp.classList.remove("rouge");   
				balp.classList.add("noir");
				indic=0;
				}
		}
		```



!!! example "Exercice 3"

	=== "Enoncé"
		Soit la page HTML suivante :

		`index.html`

		```html
		<!doctype html>
		<html lang="fr">
    	<head>
        	<meta charset="utf-8">
        	<title>Le trio</title>
        	<link rel="stylesheet" href="style.css">
    	</head>
    	<body>
        	<div></div>
    	</body>
    	<script src="script.js"></script>
		</html>
		```
		et `style.css`

		```CSS
		div {
    		width : 200px;
    		height : 100px;
    		background-color : black;   
		}
		```
	=== "Correction"
		Soit la page HTML suivante :

		`index.html`

		```html
		<!doctype html>
		<html lang="fr">
    	<head>
        	<meta charset="utf-8">
        	<title>Le trio</title>
        	<link rel="stylesheet" href="style.css">
    	</head>
    	<body>
        	<div id="demo" onmouseover = mouseOver() onmouseout = mouseOut()></div>
			<div id="demo1" onmouseover = mouseOver1() onmouseout = mouseOut1()></div>
    	</body>
    	<script src="script.js"></script>
		</html>
		```
		et `style.css`

		```CSS
		div {
    		width : 200px;
    		height : 100px;
    		background-color : black;   
		}
		```
		```JS
			function mouseOver() {
    			let boite= document.querySelector("#demo")
				boite.style.backgroundColor="red";
			}
			function mouseOut() {
    			let boite= document.querySelector("#demo")
				boite.style.backgroundColor="black";
			}
			function mouseOver1() {
    			let boite= document.querySelector("#demo1")
				boite.style.backgroundColor="black";
			}
			function mouseOut1() {
    			let boite= document.querySelector("#demo1")
				boite.style.backgroundColor="red";
			}
		```

## 5. Les formulaires en JavaScript
Un formulaire permet à un client (navigateur) de transmettre des données à un serveur.   
Le formulaire est le bloc qui organise la saisie des données et qui les envoie au serveur lors de sa validation (`submit`).  
Il est intéressant de gérer les formulaires avec JavaScript car il permet de traiter plus facilement 
les données envoyées par le formulaire.



## 6. Formulaire simple en HTML

!!! Example "Exemple de formulaire simple en HTML"
	**Source HTML**
		```html	
			<html>									
				<head>								
					<meta charset="utf-8" />						
					<title>Les formulaires en Javascript</title>	
				</head>								
				<body>	
					<form action="http://lycee-corot-morestel.fr/" target="_blank" method="POST">
						<p>Input de type texte : <input type="text" name="saisie" placeholder="Saisir un texte"></p>
						<input type="submit" value="Validation">
					</form>
				</body>
			</html>
		```
	**Rendu dans le navigateur**	
		<html >	
				<div style="background-color:lightgreen; padding:15px">
					<form action="http://lycee-corot-morestel.fr/" target="_blank" method="POST">
						<p>Input de type texte : <input type="text" name="saisie" placeholder="Saisir un texte"></p>
						<input type="submit" value="Validation">
					</form>
				</div>
**Commentaires : ** 
 
- Le formulaire exécute l'`action` (accès à une nouvelle page) lors de l'appui sur le button de type `submit`.  
- Grâce à la méthode `POST` on peut récupérer la valeur (contenu de l'attribut `value`) des différents 
éléments contenus dans le formulaire à l'aide du nom de l'élément (attribut `name`).

### 6.1. Formulaire simple en JS

!!! Example "Ecriture en JavaScript du même formulaire simple "
	**Source HTML**
		```html	
			<html>									
				<head>								
					<meta charset="utf-8" />						
					<title>Les formulaires en Javascript</title>	
				</head>								
				<body>	
					<form action="http://lycee-corot-morestel.fr/" target="_blank" method="POST">
						<p>Input de type texte : <input type="text" name="saisie" placeholder="Saisir un texte"></p>
						<input type="button" value="Validation" onClick="this.form.submit()">
					</form>
				</body>
			</html>
		```
	**Rendu dans le navigateur**
		<html>
				<div style="background-color:lightgreen; padding:15px">
					<form action="http://lycee-corot-morestel.fr/" target="_blank" method="POST">
							<p>Input de type texte : <input type="text" name="saisie" placeholder="Saisir un texte"></p>
							<input type="button" value="Validation" onClick="this.form.submit()">
					</form>
				</div>

**Commentaires : **  
- Ce formulaire effectue la même chose que le formulaire HTML ci-dessus. Donc peut d'intérêt.  
- Il permet d'introduire la méthode JavaScript spécifique aux formulaires : `submit()`.   
- Le mot clé `this` est une référence à l'objet en cours de manipulation. Il simplifie la syntaxe.
		
### 6.2. Exécution d'une fonction
Copier l'exemple ci-dessous dans un fichier HTML pour le tester.

!!! Example "Validation du formulaire par un clic sur une image "
	**Source HTML**
		```html	
			<html>									
				<head>								
					<meta charset="utf-8" />						
					<title>Les formulaires en Javascript</title>	
				</head>								
				<body>	
					<form id="formulaire" action="http://lycee-corot-morestel.fr/" target="_blank" method="POST">
						Input de type texte : <input id="saisie" type="text" name="saisie" placeholder="Saisir un texte"><br>
						<img src="./data/smiley.jpg" onClick="validation_formulaire()"/>
					</form>
					<script>
						function validation_formulaire() {
							let formElement = document.getElementById('formulaire');
							let elementValue = document.getElementById('saisie').value;
							alert("Valeur saisie : " + elementValue);
							formElement.submit();
						}
					</script>
				</body>
			</html>
		```
	**Rendu dans le navigateur**
		<html>
				<div style="background-color:lightgreen; padding:15px">
					<form id="formulaire" action="http://lycee-corot-morestel.fr/" target="_blank" method="POST"> 
						Input de type texte : <input id="saisie" type="text" name="saisie" placeholder="Saisir un texte"><br>
						<img src="../data/smiley.png" onClick="validation_formulaire()" style="margin:10px;"/>
					</form>
					<script>
						function validation_formulaire() {
							let formElement = document.getElementById('formulaire');
							let elementValue = document.getElementById('saisie').value;
							alert("Valeur saisie : " + elementValue);
							formElement.submit();
						}
					</script>
				</div>

**Commentaires : **  
- L'attribut `onClick` de la balise `img` appelle la fonction `validation_formulaire()`.  
- La variable `formElement` référence le formulaire  : on lui applique la méthode `submit()`.  
- On peut profiter de la fonction pour faire d'autres traitements tel qu'afficher le contenu 
de la zone de saisie dans une boîte de dialogue `alert`.
		




