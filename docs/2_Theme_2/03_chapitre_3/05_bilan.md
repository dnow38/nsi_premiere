---
author: Mireille Coilhac

---


???+ question "Ajouts de couples (clé, valeur)"

    Tester

    {{IDE('scripts/ajout')}} 


!!! abstract "Appartenance d'une clé dans un dictionnaire"

    Soit `mon_dico` un dictionnaire.  

    * `cle in mon_dico` renvoie `True` si la clé `cle` existe dans `mon_dico` et `False` sinon.  
    * `cle not in mon_dico` renvoie `True` si la clé `cle` n'existe pas dans `mon_dico` et `False` sinon. 


!!! abstract "Accéder à une valeur"

    Soit `mon_dico` un dictionnaire.  
    `mon_dico[cle]` renvoie la valeur associée à `cle` si elle est présente dans le dictionnaire, sinon une erreur `KeyError` se produit.
    

!!! abstract "Modifier une valeur"

    Soit `mon_dico` un dictionnaire.  
    Il suffit de faire une nouvelle affectation : `mon_dico[cle] = nouvelle_valeur`


!!! abstract "Supprimer une valeur"

    Soit `mon_dico` un dictionnaire. Soit `valeur` la valeur associée à `cle`.

    * `del mon_dico[cle]` supprime le couple (`cle`, `valeur`) de `mon_dico`.
    * `mon_dico.pop(cle)` supprime le couple (`cle`, `valeur`) de `mon_dico` et renvoie la valeur correspondante.
    

!!! abstract "Ajouter un couple (`cle`, `valeur`)"

    Soit `mon_dico` un dictionnaire. Soit `valeur` la valeur que l'on souhaite associer à `cle`.  

    * Si la clé existe déjà `mon_dico[cle] = valeur` modifie la valeur associée,
    * sinon `mon_dico[cle] = valeur` ajoute la paire (`cle`, `valeur`)
    

!!! abstract "Longueur d'un dictionnaire"

    Soit `mon_dico` un dictionnaire.  
    `len(mon_dico)` renvoie le nombre de couple (`cle`, `valeur`) du dictionnaire.


!!! abstract "Parcourir un dictionnaire"

    Soit `mon_dico` un dictionnaire.  
    Le parcours avec la boucle `for element in mon_dico` permet de pacourir les clés de `mon_dico`


!!! abstract  "Utiliser les méthodes `keys`, `values` et `items`"

    On peut parcourir les vues créées par ces méthodes, de façon analogue à ce que l'on ferait avec d'autres séquences comme des listes :
    
    Soit le dictionnaire `mon_dictionnaire`  
    
    * `mon_dictionnaire.keys()` permet d'accéder à toutes les clés de `mon_dictionnaire`
    * `mon_dictionnaire.values()` permet d'accéder à toutes les valeurs de `mon_dictionnaire`
    * `mon_dictionnaire.items()` permet d'accéder à tous les couples (clé, valeur) de `mon_dictionnaire`
    

!!! abstract  "obtenir des listes de clés, valeurs, paires (clé, valeur)"

    On peut créer les listes de clés, de valeurs ou de couples (clé, valeur) :
    
    Soit le dictionnaire `mon_dictionnaire`  
    
    * `list(mon_dictionnaire.keys())` permet d'obtenir une liste des clés de `mon_dictionnaire`
    * `list(mon_dictionnaire.values())` permet dobtenir une liste des valeurs de `mon_dictionnaire`
    * `list(mon_dictionnaire.items())` permet d'obtenir une liste des tuples (clé, valeur) de `mon_dictionnaire`

