---
author: M. Boehm & C. Poulmaire & P. Remy et Mireille Coilhac 

---


## I. Appartenance

### Gaston a-t-il noté des oies dans son dictionaire ? ou des lapins ?

???+ question "Testez"

    {{IDE('scripts/in_1')}}


??? note "Gaston a-t-il des poules ? Cliquez après avoir réfléchi ..."

    Gaston n'a pas de poule : vous le voyez en lisant le dictionnaire. Vous pouvez tester dans la console de l'exemple ci-dessus : 

    ```pycon
    >>> ferme_gaston["poule"]
    0
    ```

    👉 Pourtant `"poule" in ferme_gaston` est évalué à `True`.

    ??? note "Pourquoi ? "

        👉 l'opérateur `in` renvoie `True` si la clé est présente. Peu importe la valeur associée à cette clé.

    

???+ question "Testez avec une fonction"

    {{IDE('scripts/in_2')}}

### Retenir : 

!!! abstract "Appartenance d'une clé dans un dictionnaire"

    Soit `mon_dico` un dictionnaire.  

    * `cle in mon_dico` est une expression qui renvoie `True` si la clé `cle` existe dans `mon_dico` et `False` sinon.  
    * `cle not in mon_dico` renvoie `True` si la clé `cle` n'existe pas dans `mon_dico` et `False` sinon.  


## II. Comment accéder à une valeur ?

???+ question "Testez"

    {{IDE('scripts/acces_valeur_1')}}

### Retenir : 

!!! abstract "Accéder à une valeur"

    Soit `mon_dico` un dictionnaire.  
    `mon_dico[cle]` renvoie la valeur associée à `cle` si elle est présente dans le dictionnaire, sinon une erreur `KeyError` se produit.


## III. Comment modifier une valeur ?

???+ question "Testez"

    {{IDE('scripts/modif_valeur_1')}}

### Retenir : 

!!! abstract "Modifier une valeur"

    Soit `mon_dico` un dictionnaire.  
    Il suffit de faire une nouvelle affectation : `mon_dico[cle] = nouvelle_valeur`


## IV. Comment supprimer un couple `(clé, valeur)` ?

### En utilisant `del`

???+ question "Testez"

    {{IDE('scripts/suppr_cle_valeur_1')}}

### En utilisant `pop`

???+ question "Testez"

    {{IDE('scripts/suppr_cle_valeur_2')}}


### Retenir : 

!!! abstract "Supprimer une valeur"

    Soit `mon_dico` un dictionnaire. Soit `valeur` la valeur associée à `cle`.

    * `del mon_dico[cle]` supprime le couple (`cle`, `valeur`) de `mon_dico`.
    * `mon_dico.pop(cle)` supprime le couple (`cle`, `valeur`) de `mon_dico` et renvoie la valeur correspondante.
    

## V. Comment ajouter un couple `(clé, valeur)` ?

???+ question "Ajoutez des dragons"

    Vous savez déjà rajouter des éléments dans un dictionnaires, puisque vous l'avez fait pour créer un dictionnaire à 
    partir d'un dictionnaire vide.
    Compléter le script suivant pour ajouter `"dragon": 2`

    {{IDE('scripts/ajout_cle_valeur_1')}}

    ??? success "Solution"

        ```python
        ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 2, "cheval": 4}
        ferme_gaston["dragon"] = 2
        ```

### Retenir : 

!!! abstract "Ajouter un couple (clé, valeur)"

    Soit `mon_dico` un dictionnaire. Soit `valeur` la valeur que l'on souhaite associer à `cle`.  

    * Si la clé existe déjà `mon_dico[cle] = valeur` modifie la valeur associée,
    * sinon `mon_dico[cle] = valeur` ajoute la paire (`cle`, `valeur`)


## VI. Quelle est la longueur d'un dictionnaire ?

???+ question "Testez"

    Essayez de trouver la syntaxe vous-même (pensez aux listes)

    {{IDE('scripts/longueur')}}

    ??? success "Solution"

        ```python
        ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 2, "cheval": 4}
        longueur = len(ferme_gaston)
        ```

### Retenir : 

!!! abstract "Longueur d'un dictionnaire"

    Soit `mon_dico` un dictionnaire.  
    `len(mon_dico)` renvoie le nombre de paires clé, valeur du dictionnaire.


## VII. QCM

???+ question

    On considère le dictionnaire `ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 2, "cheval": 4}`
    On veut accéder au nombre de lapins. Quelle commande faut-il saisir ?

    === "Cocher la ou les affirmations correctes"
        
        - [ ] `ferme_gaston[0]`
        - [ ] `ferme_gaston["lapin"]`
        - [ ] `ferme_gaston("lapin")`
        - [ ] `ferme_gaston[lapin]`

    === "Solution"
        
        - :x: ~~`ferme_gaston[0]`~~ On accède à une valeur grâce à une clé.
        - :white_check_mark: `ferme_gaston["lapin"]`
        - :x: ~~`ferme_gaston("lapin")`~~ Il faut des crochets, non des parenthèses.
        - :x: ~~`ferme_gaston[lapin]`~~ "lapin" est une chaîne de carctères. Ne pas oublier les guillemets.

???+ question

    On considère le dictionnaire `ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 2, "cheval": 4}`
    Gaston a acheté 3 lapins. Il faut modifier le dictionnaire.  
    Quelle commande peut-on saisir ?

    === "Cocher la ou les affirmations correctes"
        
        - [ ] `ferme_gaston[0] = 8`
        - [ ] `("lapin" : 5) = ("lapin" : 8)`
        - [ ] `ferme_gaston("lapin") = 8`
        - [ ] `ferme_gaston["lapin"] = 8`
        - [ ] `ferme_gaston["lapin"] = ferme_gaston["lapin"] + 3`

    === "Solution"
        
        - :x: ~~`ferme_gaston[0] = 8`~~ On accède à une valeur grâce à une clé.
        - :x: ~~`("lapin" : 5) = ("lapin" : 8)`~~
        - :x: ~~`ferme_gaston("lapin") = 8`~~ Il faut des crochets, non des parenthèses.
        - :white_check_mark: `ferme_gaston["lapin"] = 8` juste mais préférer la répose ci-dessous
        - :white_check_mark: `ferme_gaston["lapin"] = ferme_gaston["lapin"] + 3`

???+ question

    On considère le dictionnaire `ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 2, "cheval": 4}`
    On veut ajouter la paire `"dragon : 2"` Quelle commande faut-il saisir ?

    === "Cocher la ou les affirmations correctes"
        
        - [ ] `ferme_gaston["dragon": 2]`
        - [ ] `ferme_gaston["dragon"] = 2`
        - [ ] `2 = ferme_gaston["dragon"]`
        - [ ] `ferme_gaston["dragon", 2]`
        - [ ] `ferme_gaston("dragon", 2)`

    === "Solution"
        
        - :x: ~~`ferme_gaston["dragon": 2]`~~
        - :white_check_mark: `ferme_gaston["dragon"] = 2`
        - :x: ~~`2 = ferme_gaston["dragon"]`~~
        - :x: ~~`ferme_gaston["dragon", 2]`~~
        - :x: ~~`ferme_gaston("dragon", 2)`~~

???+ question

    On considère le dictionnaire `ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 2, "cheval": 4, "dragon": 2}`
    On veut supprimer la paire `"dragon": 2`. Quelle commande faut-il saisir ?

    === "Cocher la ou les affirmations correctes"
        
        - [ ] `del ferme_gaston[4]`
        - [ ] `del ("dragon")`
        - [ ] `del ferme_gaston("dragon")`
        - [ ] `del ferme_gaston("dragon": 2)`
        - [ ] `del ferme_gaston["dragon"]`

    === "Solution"
        
        - :x: ~~`del ferme_gaston[4]`~~ On accède à une valeur grâce à une clé.
        - :x: ~~`del ("dragon")`~~
        - :x: ~~`del ferme_gaston("dragon")`~~
        - :x: ~~`del ferme_gaston("dragon": 2)`~~
        - :white_check_mark: `del ferme_gaston["dragon"]`

???+ question

    On considère le dictionnaire `ferme_gaston = {"lapin": 6, "vache": 7, "cochon": 2, "cheval": 4, "dragon": 2}`
    Que vaut l'expression `len(ferme_gaston)` ?

    === "Cocher la ou les affirmations correctes"
        
        - [ ] 5
        - [ ] 10
        - [ ] Un message d'erreur
        - [ ] Rien
        - [ ] 2

    === "Solution"
        
        - :white_check_mark: 5 .C'est la longueur du dictionnaire. Il y a 5 paires (clés: valeur)
        - :x: ~~10~~
        - :x: ~~Un message d'erreur~~
        - :x: ~~Rien~~
        - :x: ~~2~~

## VIII. Exercices


???+ question "Gaston compte ses animaux"

    On donne la fonction suivante où `animal` est de type `str` et `ferme` de type `dict`

    ```python
    def appartient(animal, ferme):
        return animal in ferme
    ```

    Quel est le type de variable renvoyé par la fonction `appartient` ?

    ??? success "Solution"

        Type booléen. La fonction renvoie `True` ou `False`.

    Compléter la fonction `nombre_animaux`. Cette fonction prend en paramètres `animal` de type `str` et `ferme` de type `dict`.
    Elle renvoie la valeur associée à `animal` si  `animal` est une clé de `ferme`, et 0 sinon.
       
    {{IDE('scripts/appartient_sujet')}}

    ??? success "Solution"

        ```python
        def appartient(animal, ferme):
            return animal in ferme

        def nombre_animaux(animal, ferme):
            if appartient(animal, ferme):
                return ferme[animal]  # sortie anticipée de la fonction

            return 0
        ```

###  ✏️ Des Pokémon : Exercice "papier"

#### L'énoncé

!!! abstract "Exercice à réaliser à l'écrit"

    Un Pokémon est modélisé par :  

    * son nom (chaîne de caractères),
    * ses points de vie ou HP (nombre entier),
    * sa puissance d’attaque (nombre entier),
    * sa défense (nombre entier)
    * sa vitesse (nombre entier),
    * son ou ses types (liste de chaîne de caractères).

    On représente un Pokémon par un dictionnaire.  
    Par exemple :  

    `bulbizarre = {"Nom": "Bulbizarre", "HP": 45, "Attaque": 49,
    "Défense": 49, "Vitesse": 45, "Type": ["Plante", "Poison"]}`

    1) On donne le Pokémon suivant : `mystere = {"Nom": "Salamèche", "HP": 39, "Défense": 43, "Vitesse": 455, "Type": ["Feu"]}`

    (a) Quel est son nom? Préciser l’instruction permettant de le récupérer.  
    (b) Combien a-t-il de types "Type" ? Quelle(s) instruction(s) permet de le(s) récupérer ?  
    (c) La vitesse donnée est inexacte, elle est en fait de 65. Quelle instruction permet de corriger cette erreur ?  
    (d) Il manque la valeur de la puissance d’attaque de ce Pokémon. Elle est de 52. Quelle     instruction faut-il saisir pour ajouter cette information ?  

    2) Donner le dictionnaire qui représente le Pokémon `fragilady` qui a une défense de 75, une attaque de 60, une vitesse de 90, 70 points de vie, de type `Plante`.

    3) Dans les questions qui suivent, on supposera que le Pokémon passé en paramètre existe.

    (a) Ecrire une fonction qui prend un Pokémon en paramètre et renvoie sa valeur d’attaque.  
    (b) Ecrire une fonction qui prend un Pokémon en paramètre et renvoie son nombre de types.  
    (c) Ecrire une fonction qui prend un Pokémon et une valeur entière en paramètre et renvoie `True` si le Pokémon a une valeur de défense supérieure à la valeur passée en paramètre, `False` sinon.  
    (d) Ecrire une fonction qui prend deux dictionnaires représentant des Pokémon en paramètres, et renvoie le nom du gagnant d'un combat entre eux.  
    Le premier paramètre représente un Pokemon attaquant, le second un Pokémon attaqué.  
    Nous choisissons la règle de jeu très simplifiée suivante : le Pokémon attaquant gagne si sa valeur d' "Attaque" est supérieure à la valeur de "Défense" de son adversaire. Si le Pokémon attaqué possède une valeur de "Défense" supérieure à la valeur d' "Attaque" de l'attaquant, il est vainqueur. Dans le cas où ces deux valeurs sont identiques, la fonction doit renvoyer "match nul".
    
    


#### 😀 Voici une correction

<div class="centre">
<iframe 
src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/pokemons_1.ipynb"
width="950" height="1000" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>

<!--- La correction à dévoiler plus tard

#### Correction un peu plus tard ... 😊

⌛ Une correction viendra plus tard ...   
<div class="centre">
<iframe 
src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/a_telecharger/pokemons_1.ipynb"
width="950" height="1000" 
frameborder="0" 
allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
</iframe>
</div>
-->
