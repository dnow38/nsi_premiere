---
author: Mireille Coilhac

---

## I. Les p-uplets nommés

!!! abstract "les p-uplets nommés"

    Il existe une autre structure que les dictionnaires qui utilise des paires clé, valeurs : les p-uplets nommés.  
    Contrairement aux dictionnaires, les p-uplets nommés sont **immuables**. Un de leur intérêt est de permettre d'accéder aux éléments d'un p-uplet grâce à une clé, plutôt qu'avec un indice.

    Prenons l'exemple suivant : 

    ```python
    alice = ("Martin", "Alice", 2003, "alice@mon_mail.fr", "Melun")
    bob = ("Dupond", "Bob", 2004, "bob@mon_mail.fr", "Le Mée")
    gaston = ("Durand", "Gaston", 2000, "gaston@mon_mail.fr", "Savigny")
    ```

    Ces tuples ont tous la même structure. Si l'on désire l'année de naissance d' Alice, il faut se souvenir qu'on y accède avec `alice[2]`. Si l'on désire son adresse mail, il faut se souvenir qu'on y accède avec `alice[3]` etc.

    Les p-uplets nommés permettent de remplacer les indices d'un p-uplet par des clés pour accéder à un élément. Par exemple, dans ce cas-là, l'indice 0 correspond à la clé "nom", l'indice 1 à "prenom", l'indice 2 à "annee_naissance", l'indice 3 à "mail", l'indice 4 à "ville".

    Observons une première implémentation en Python :

    ???+ question "Tester"

        {{IDE('scripts/puplets_nommes')}}

    Observons une seconde implémentation en Python qui permet également de nommer les clés :

    ???+ question "Tester"

        {{IDE('scripts/puplets_nommes_2')}}


## II. Les fichiers CSV

!!! abstract "Les fichiers CSV"

    **Exemple:**

    ```txt
    nom,prenom,nb_voix
    dupond,emile,514
    dupond,chloe,632
    dubois,Jacques,324
    dupons,camille,421
    ```


    Un fichier CSV est un fichier texte. La première ligne est composée des descripteurs (ici `nom`, `prenom`et `nb_voix`). Les lignes suivantes correspondent à des enregistrements : lignes d'un tableau où les virgules (par exemple) correspondent aux séparations entre les colonnes.

    Avec Python, nous pouvons créer une liste de dictionnaires correspondant à un fichier CSV.

    Dans cet exemple, nous pouvons créer la liste suivante : 

    ```python
    elections = [
                {'nom': 'dupond', 'prenom': 'emile', 'nb_voix': '514'}, 
                {'nom': 'dupond', 'prenom': 'chloe', 'nb_voix': '632'}, 
                {'nom': 'dubois', 'prenom': 'Jacques', 'nb_voix': '324'}, 
                {'nom': 'dupons', 'prenom': 'camille', 'nb_voix': '421'}
                ]
    ```

    Nous trouvons beaucoup de fichiers de données CSV sur internet. Pour les exploiter, nous pourrons travailler avec des listes de dictionnaires.  
    Nous étudierons ceci un peu plus tard dans l'année.


## III. Les tables de hash en quelques mots

### Tables de hash : vidéo d'Arnaud LEGOUT 

<iframe width="560" height="315" src="https://www.youtube.com/embed/IhJo8sXLfVw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Dictionnaires Python : vidéo d'Arnaud LEGOUT

<iframe width="560" height="315" src="https://www.youtube.com/embed/VnhBoQAgIVs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## IV. Les dictionnaires sont des objets mutables

!!! warning "Remarque : copies de dictionnaires"

    Nous avons le même comportement avec les dictionnaires qu'avec les listes. Observons cet exemple :

    ```pycon
    >>> d1 = {'a': 1, 'b': 2}
    >>> d2 = d1
    >>> id(d1)
    139633253889600
    >>> id(d2)
    139633253889600
    >>> d1['a'] = 0
    >>> d1
    {'a': 0, 'b': 2}
    >>> d2
    {'a': 0, 'b': 2}
    ```


