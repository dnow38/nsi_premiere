---
author: Mireille Coilhac

---

##  I. Les dictionnaires { :dragon: : 2, :pig: : 5, :ox: : 40}

!!! abstract "Dictionnaire : définition"

    Un dictionnaire est de type `dict` en Python. Il est constitué de paires **non ordonnées clé : valeur**.  
    Chaque **clé** est présente de façon **unique** dans le dictionnaire.

    Un dictionnaire s'écrit avec des **accolades {clé : valeur …}**.

    On accède à une valeur d'un dictionnaire à l'aide d'une clé.

    **Les clés doivent être de types immuables** (ne peuvent pas être modifiées).  
    🌵 On peut donc par exemple choisir comme clé des nombres, des chaînes de caractères, des tuples d'éléments de types immuables,   mais **jamais des listes**.


!!! abstract "Exemples"
    
    ```python
    ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 2, "cheval": 4}
    amis_gaston = {0612457899 : "Gustave", 0712345678 : "Alice", 0998876554 : "Bob"}
    ```


???+ question

    On peut choisir comme clé d'un dictionnaire

    === "Cocher la ou les affirmations correctes"
        
        - [ ] `"nom"`
        - [ ] `["pomme", "Golden"]`
        - [ ] `5`
        - [ ] `("pain", "baguette")`

    === "Solution"
        
        - :white_check_mark: `"nom"` est une chaîne de caractères immuable
        - :x: Une liste **n'est pas immuable**
        - :white_check_mark: Un entier est immuable
        - :white_check_mark: Un tuple de chaines de caractères est immuable


???+ question

    On peut choisir 

    === "Cocher la ou les affirmations correctes"
        
        - [ ] comme clé d'un dictionnaire `"Kouign amann"`.
        - [ ] comme clé d'un dictionnaire `["farine", "beurre", "sucre"]`.
        - [ ] comme valeur d'un dictionnaire `"Kouign amann"`.
        - [ ] comme valeur d'un dictionnaire `["farine", "beurre", "sucre"]`.

    === "Solution"
        
        - :white_check_mark: `"Kouign amann"` est une chaîne de caractères immuable.
        - :x: Une liste **n'est pas immuable**.
        - :white_check_mark: On peut choisir n'importe quel type pour une valeur.
        - :white_check_mark: On peut choisir n'importe quel type pour une valeur.

!!! abstract "Les dictionnaires ... vides ???"

    Nous pouvons créer un dictionaire vide. Pour cela il y a deux possibilités :

    ```python
    dico_vide = {}
    autre_dico_vide = dict()
    ```

    ??? note "Pourquoi créer un dictionnaire vide ?"

        😂 Parce qu'on va pouvoir le remplir ...  
        Nous verrons cela un peu plus bas lorsque nous créerons des dictionnaires.

!!! danger "📖 Le mot dictionnaire"

    🐘 Un dictionnaire est un ensemble non ordonné de couples clé : valeur.  
    Dans la vie courante, nous cherchons le sens des mots dans des dictionnaires. Ces mots peuvent être considérés comme des « clés ». Les valeurs associées à ces clés sont les textes écrits pour chaque mot.
    
    🌵 Dans ces dictionnaires les clés sont rangées par ordre lexicographique pour permettre une recherche relativement rapide. Dans un dictionnaire Python, nous avons vu que l’ordre dans lequel sont écrits les couples « clé: valeur » n’a pas d’importance. C’est un processus décrit dans les compléments (tables de hachage) qui permet une recherche encore plus rapide (indépendante du nombre d’éléments).


## II. Création d'un dictionnaire

### 1. Ecriture directe du dictionnaire dans le code
Nous pouvons directement écrire le dictionnaire `dictionnaire = {une_cle : une_valeur , autre_cle : autre_valeur, …}`

comme par exemple ici :  

```python
figurines = {"licorne": 2, "yeti": 1, "pegase": 1, "dragon": 3}
```

### 2. Compléter un dictionnaire vide.

Une façon très pratique de créer un dictionnaire, est de partir d'un dictionnaire vide. Il suffit ensuite d'insérer des couples **clé : valeur** en utilisant les clés :

???+ question "Tester"

    {{IDE('scripts/panier_1')}}


### 3. Bob fait ses courses

Vous allez aider Bob a constituer le dictionnaire de ses achats. 

Vous devez compléter la fonction `courses` qui prend en paramètres deux listes `produits` et `quantites`.

* `produits` est une liste de chaine de caractères ;
* `quantites` est une liste d’entiers ;
* `produits` et `quantites` sont de même longueur.


Par exemple `courses(["farine", "beurre", "oeufs"], [1, 2, 12])` doit renvoyer `{'farine': 1, 'beurre': 2, 'oeufs': 12}`

    
???+ question

    Compléter le script ci-dessous :

    {{IDE('scripts/bob_1')}}

    ??? success "Solution"

        ```python
        def courses(produits, quantites):
            panier = {}
            for i in range(len(produits)):
                panier[produits[i]] = quantites[i]
            return panier
        ```

??? note "Remarque"

    Il existe d'autres méthodes pour créer des dictionnaires. Nous en évoquerons quelques unes dans la partie "compléments" de ce cours.


??? note "Et dans les langages autres que Python ?"

    Les dictionnaires sont egalement présents dans d’autres langages sous le nom de "mémoires associatives" ou de "tableaux associatifs "
