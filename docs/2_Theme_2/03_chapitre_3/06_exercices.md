---
author: Charles Poulmaire, Mireille Coilhac et l'équipe e-nsi

---



???+ question "Gaston ne se souvient plus ..."

    Gaston veut afficher le nombre de poules qu'il a dans sa ferme.

    <iframe src="https://www.codepuzzle.io/IDLDU7" width="100%" height="600" frameborder="0"></iframe>

    ??? success "Solution"

        Pour vous aider à répondre, lisez "II. Comment accéder à une valeur ?" en suivant ce lien: [Opérations](03_operations.md)
    

???+ question "Gaston achète des animaux ..."

    Gaston achète 3 moutons. Il veut compléter le dictionnaire.

    <iframe src="https://www.codepuzzle.io/ID2MDQ" width="100%" height="600" frameborder="0"></iframe>

    ??? success "Solution"

        Pour vous aider à répondre, lisez "V. Comment ajouter une paire clé/valeur ?" en suivant ce lien: [Opérations](03_operations.md)


???+ question "Gaston vend des animaux"

    `ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 1, "cheval": 4}`

    Gaston a décidé de vendre un animal de chaque espèce. Evidemment, il possède au moins un animal de chaque espèce. Compléter la fonction suivante, qui diminue chaque valeur de 1 dans le dictionnaire "en place".

    👉 Pour mettre au point votre script, vous pouvez passer le `assert` en commentaire en cliquant sur `###` en haut à droite. Cliquer encore une fois reviendra en arrière.
    
    {{IDE('scripts/vend_1')}}

    ??? success "Solution"

        ```python
        def vend(ferme):
            for cle in ferme:
                ferme[cle] = ferme[cle] - 1
        ```

???+ question "Gaston vend encore des animaux"

    `ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 1, "cheval": 4}`

    Gaston a décidé de vendre un animal de chaque espèce. Evidemment, il possède au moins un animal de chaque espèce. 
    
    Compléter la fonction suivante, qui diminue chaque valeur de 1 dans le dictionnaire. On modifiera directement le dictionnaire (on dit que la modification est « en place »).
    
    La solution de l'exercice précedent ne lui convient pas. En effet, un animal qui a un effectif de `0` ne doit pas se trouver dans 
    le dictionnaire. 
    
    {{IDE('scripts/vend_2')}}



    ??? tip "Aide"

        Python interdit de modifier un dictionnaire lors de son parcours...

        👉 Vous pouvez utiliser une liste Python `a_supprimer`


    ??? success "Solution"

        ```python
        def vend_encore(ferme):
            a_supprimer = []
            for cle in ferme:
                ferme[cle] = ferme[cle] - 1
                if ferme[cle] == 0:
                    a_supprimer.append(cle)
            for cle in a_supprimer :
                del ferme[cle]
        ```


???+ question "Gaston fait ses courses"

    Gaston a décidé de préparer sa liste de courses qu'il va commander, avant d'aller la chercher au DRIVE de son supermarché. Quel va être le montant de sa facture ?
    Écrire une fonction `calcul_prix(produits, catalogue)` où :  

    * `produits` est un dictionnaire contenant, comme clés, les produits souhaités par Gaston et comme valeurs associées, la quantité désirée de chacun d’entre eux.
    * `catalogue` est une dictionnaire contenant tous les produits du magasin avec leur prix associé.
    * La fonction renvoie le montant total des achats de Gaston.
    
    Vous pourrez supposer que les arguments passés à la fonction sont du bon type, et que les produits souhaités par Gaston figurent bien dans le catalogue du magasin.

    **Exemple :**  
    L’appel suivant de la fonction :

    ```python
    calcul_prix({"brocoli":2, "mouchoirs":5, "bouteilles d'eau":6},
                {"brocoli":1.50, "bouteilles d'eau":1,  "bière":2,
                "savon":2.50, "mouchoirs":0.80})
    ```

    doit renvoyer : `13.0`

    Compléter ci-dessous : 

       {{IDE('scripts/achats_2')}}

    ??? success "Solution"

        ```python
        def calcul_prix(produits, catalogue):
            prix = 0
            for cle in produits: # On aurait aussi pu écrire : for cle in produits.keys()
                prix = prix + produits[cle]*catalogue[cle]
            return prix

        ```

    ??? note "Remarque : les flottants"

        Le résultat qui devrait s'afficher pour `calcul_prix(produits, catalogue)`  est 63,8. Or il s'affiche 63.79999999999999.  

        C'est un problème lié à l'encodage des nombres flottants. Nous étudierons ceci pendant l'année.  

        L'instruction `assert calcul_prix(produits, catalogue) == 63.8` provoquerait un message   d'erreur d'assertion. Il ne faut jamais comparer des nombres flottants entre eux **ainsi**.

???+ question "Gaston fait des histogrammes"

    Gaston s'entraîne au jeu de scrabble. Quelles sont les lettres nécessaires pour écrire le mot "brontosaurus" ? Il faut une lettre "b", deux lettres "r", deux lettres "o" etc.  
    Pour aider Gaston, compléter la fonction qui prend en paramètre une chaine de caracères et renvoie le dictionnaires dont les clés sont chaque caractère, et la valeur associée le nombre de fois qu'il apparaît dans le mot.

    {{IDE('scripts/histogramme_sujet')}}

    ??? tip "Aide"

        Vous pourrez définir un dictionnaire vide pour commencer.

    ??? success "Solution"

        ```python
        def histogramme(caracteres):
            histo = {}
            for caractere in caracteres:
                if caractere in histo:
                    histo[caractere] += 1
                else:
                    histo[caractere] = 1
            return histo
        ```

???+ question "Top 3"

    Un jury doit attribuer le prix du « Codeur de l’année ».   
    Afin de récompenser les trois candidats ayant obtenu la meilleure note, nous vous demandons d’écrire une fonction `top_3_candidats` qui reçoit un dictionnaire contenant comme clés les noms des candidats et comme valeurs la note que chacun a obtenue.  
    Cette fonction doit renvoyer la liste contenant les noms des trois meilleurs candidats, par ordre décroissant de leurs notes.  
    Vous pourrez supposer que les candidats ont des notes différentes, et qu’ils sont plus que trois.  
  

    **Exemple :** 
    L’appel suivant de la fonction :  
    ```python
    top_3_candidats({'Candidat 7': 2, 'Candidat 2': 38, 'Candidat 6': 85, 'Candidat 1': 8, 'Candidat 3': 17, 
    'Candidat 5': 83,'Candidat 4': 33})
    ``` 

    doit renvoyer :   
    `['Candidat 6', 'Candidat 5', 'Candidat 2']`

    Compléter ci-dessous : 

    {{IDE('scripts/Top3Candidats_sujet')}}

    ??? tip "Astuce 1"

        Vous pouvez partir d'une liste vide

    ??? tip "Astuce 2"

        Vous pouvez chercher la note maximale, supprimer la paire correspondante du dictionnaire de départ, ajouter le nom correspondant dans la liste cherchée. Il suffit de répéter ce processus 3 fois.

    ??? tip "Astuce 3"

        La fonction `pop` peut vous aider.

    
    ??? success "Solution"
    
        ```python
        def top_3_candidats(notes):
            """
            Renvoie une liste contenant les 3 meilleurs candidats
            Prend en entrée un dictionnaire de noms/notes
            """

            top_liste = []  #Initialise une liste vide pour creer la liste des 3 meilleurs
            for i in range(3):  # On cherche les 3 premiers
                note_max = -1  # Initialisation pour entrer dans la boucle
                             # de recherche de la plus grande note
                for (nom, note) in notes.items(): #parcourt du dictionnaire
                    if note > note_max:  # recherche de note maximale
                        note_max = note
                        premier = nom     # nom associe
                notes.pop(premier)   # supprime cet item du dictionnaire
                                # on cherche l'éleve de note maximale parmi
                                # ceux qui restent
                top_liste.append(premier) # Ajoute ce candidat à la liste des 3 vainqueurs
            return top_liste
        ```



???+ question "Le bon enclos"
   
    [Le bon enclos](https://codex.forge.apps.education.fr/exercices/bon_enclos/){ .md-button target="_blank" rel="noopener" }


???+ question "Anniversaires"
   
    [Anniversaires](https://codex.forge.apps.education.fr/exercices/anniversaires/){ .md-button target="_blank" rel="noopener" }


???+ question "Valeurs extrêmes"
   
    [Valeurs extrêmes](https://codex.forge.apps.education.fr/exercices/dict_extremes/){ .md-button target="_blank" rel="noopener" }


???+ question "Dictionnaire d'occurences"
   
    [Dictionnaire d'occurences](https://codex.forge.apps.education.fr/exercices/dico_occurrences/){ .md-button target="_blank" rel="noopener" }


???+ question "Top likes"
   
    [Top likes](https://codex.forge.apps.education.fr/exercices/top_like/){ .md-button target="_blank" rel="noopener" }


???+ question "Dictionnaire des antécédents"
   
    [Dictionnaire des antécédents](https://codex.forge.apps.education.fr/exercices/antecedents/){ .md-button target="_blank" rel="noopener" }


???+ question "Dictionnaire de likes"
   
    [Dictionnaire de likes](https://codex.forge.apps.education.fr/exercices/dictionnaire_aime/){ .md-button target="_blank" rel="noopener" }
    

!!! abstract  "Les Pokémon"

    <div class="centre">
    <iframe 
    src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/pokemon_sujet.ipynb"
    width="950" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

    ⌛ Une correction viendra plus tard ...


<!--- La correction à dévoiler plus tard

😀 Voici une correction

    <div class="centre">
    <iframe 
    src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/pokemon_correction.ipynb)"
    width="950" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>
-->


!!! abstract  "🏰 🐉  Mini-projet  🐉 🏰"


    <div class="centre">
    <iframe 
    src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/Donjon_sujet.ipynb"
    width="950" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

    ⌛ Une correction du mini-projet viendra plus tard ...



<!--- La correction à dévoiler plus tard

😀 Voici une correction
   
    <div class="centre">
    <iframe 
    src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/Donjon_corr.ipynb"
    width="950" height="1000" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>
                
-->


???+ question "La ferme de gaston avec GUI"

    On nomme **GUI** une Interface Graphique Utilisateur.
    Jean-Louis Thirot en a préparé une pour aider Gaston à gérer sa ferme.
    Le code se trouve dans le fichier `gestion_ferme.py` 

    🌐 Vous devez télécharger ce fichier : `gestion_ferme.py` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/gestion_ferme.py). 

    😀 Vous n'avez pas besoin de regarder le code de ce fichier, toutes les fonctions seront importées dans le fichier que vous devez compléter, et qui permet à Gaston de bien gérer sa ferme.

    🌐 Vous devez télécharger ce second fichier : `gestion_stock_2023.py`: ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/gestion_stock_2023.py).

    👉 Vos deux fichiers doivent être placés dans le même dossier. 
    
    1. Avec votre éditeur Python habituel, ouvrez les deux fichiers.
    2. Exécutez le fichier `gestion_ferme.py`. Vous verrez apparaître l'interface graphique. Vous pouvez la tester, mais elle ne fonctionne pas encore ...
    3. Compléter les fonctions du fichier `gestion_stock_2023.py`. Ne pas oublier d'enregistrer.
    Tester en exécutatnt `gestion_ferme.py`, et corriger votre code jusqu'à ce qu'il fonctionne comme Gaston le voudrait.
        
    

    ??? success "Solution"

        ⏳ La correction viendra bientôt ... 



<!--- La correction à télécharger plus tard A SORTIR DE L'ADMONITION

🌐 Fichier `gestion_stock_corrige.py` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/gestion_stock_corrige.py)

-->

        
