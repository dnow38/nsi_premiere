def appartient(animal, ferme):
    return animal in ferme

def nombre_animaux(animal, ferme):
    if appartient(animal, ferme):
        ...
    ...


ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 1, "cheval": 4}

# Tests
assert appartient("oie", ferme_gaston) == False, "oie n'est pas une clé de ce dictionnaire"
assert nombre_animaux("lapin", ferme_gaston) == 5, "Il y a 5 lapins"
assert nombre_animaux("oie", ferme_gaston) == 0, "Nombre d'oies : 0"
