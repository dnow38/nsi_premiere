---
author: Mireille Coilhac

---




![image](data/BO.png){: .center}


## I. La ferme de Gaston - version 1

Gaston possède des lapins, des vaches, des cochons et des chevaux. Il désire créer un "document" (en python 😊) lui permettant de gérer le nombre fluctuant d'animaux. Par exemple, il doit noter qu'il possède 5 lapins, 7 vaches, 2 cochons et 4 chevaux.

Comment peut-il faire ? Quelle structure python que vous connaissez va-t-il pouvoir utiliser ?

??? success "Solution"
    * On peut imaginer utiliser deux listes :  
    ```python
    liste_animaux = ["lapin", "vache", "cochon", "cheval"]
    liste_effectifs = [5, 7, 2, 4]
    ```
    * On peut imaginer utiliser des listes de listes comme par exemple : 
    ```python
    ferme_gaston_1 = [["lapin", 5], ["vache", 7], ["cochon", 2], ["cheval", 4]]
    ferme_gaston_2 = [["lapin", "vache", "cochon", "cheval"], [5, 7, 2, 4]]
    ```
    * On peut aussi imaginer utiliser des listes de tuples comme par exemple :
    ```python
    ferme_gaston_3 = [("lapin", 5), ("vache", 7), ("cochon", 2), ("cheval", 4)]
    ```
    * Vous avez peut-être eu d'autres idées encore ... 😊  
    🖐️ Dans ce cas-là, appelez votre professeur pour lui montrer. Il voudra peut-être le partager avec toute la classe ...


??? note "Aidez Gaston"
    Finalement, Gaston a choisi la liste de listes
    ```python
    ferme_gaston_1 = [["lapin", 5], ["vache", 7], ["cochon", 2], ["cheval", 4]]
    ```
    Mais il est distrait, il n'a pas la liste sous les yeux, et ne sait plus dans quel ordre il a mis les animaux dans la liste.  
    Aidez le à écrire les instructions qui permettent de retrouver qu'il a 7 vaches. Pour cela, compléter la fonction suivante :

    {{IDE('scripts/fct_nbre_animaux')}}

    ??? success "Solution"
        ```python
        def nb_animaux(ferme, animal):
            """ renvoie le nombre d'animaux `animal` présents dans la liste `ferme`
            Par exemple nb_animaux([["lapin", 5], ["vache", 7]], "vache") renvoie 7
            """
            for liste in ferme:
                if liste[0] == animal:
                    return liste[1]
        ```

## II. La ferme de Gaston - version 2

Gustave vient d'expliquer à Gaston, son ami, qu’il peut utiliser une structure appelée "dictionnaire". L'efficacité d'une recherche, ou d'un accès dans un dictionnaire est bien supérieure à celle qu'on obtiendrait avec une liste de liste. C'est un des intérêts majeurs de cette nouvelle structure.

Voilà comment on construit le dictionnaire `ferme_gaston` :  
`ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 2, "cheval": 4}`

Cela permet d'associer à chaque animal, par exemple à "vache", le nombre correspondant.
Dans cet  exemple "vache" s'appelle une **clé** et 7 est la **valeur** qui lui est associée.

!!! abstract "Dictionnaire : premiers éléments"

    Un dictionnaire est constitué de paires **clé : valeur**.

    Il s'écrit avec des **accolades {clé : valeur …}**

???+ question

    Donner une autre clé du dictionnaire `ferme_gaston`, et la valeur qui lui est associée.

    ??? success "Solution"
        * Pour la clé "lapin", la valeur associée est 5
        * Pour la clé "cochon", la valeur associée est 2
        * Pour la clé "cheval", la valeur associée est 4


???+ question "Aider Gaston à retrouver la mémoire"

    Tester le script ci-dessous, et ajouter vos propres essais :
 
    {{IDE('scripts/ferme_1')}}

## III. Gaston achète des lapins

Gaston vient d'acheter 6 lapins. Il décide donc de modifier son dictionnaire.  
Tester ci-dessous :

{{IDE('scripts/ferme_2')}}

???+ question

    Que s'est-il passé ?

    ??? success "Solution"
        Dans un dictionnaire, il ne peut pas y avoir deux clés identiques. Python n'en a gardé qu'une ...

!!! abstract "Dictionnaire : précisons"

    Un dictionnaire est constitué de paires **clé : valeur**.  
    Chaque **clé** est présente de façon **unique** dans le dictionnaire.

    Un dictionnaire s'écrit avec des **accolades {clé : valeur …}**.

## IV. Gaston met à jour son dictionnaire

Gaston a bien compris qu'il ne pouvait pas y avoir deux clés identiques dans son dictionnaire.  
Après avoir acheté ses lapins, il va donc tout simplement modifier son dictionnaire, avec l'aide de Gustave.

Tester ci-dessous

{{IDE('scripts/ferme_3')}}

Il aurait pu tout simplement rajouter 6 lapins. Tester ci-dessous

{{IDE('scripts/ferme_5')}}


## V. Gaston utilise un dictionnaire de secours

Gaston est parti vérifier qu'il ne manquait pas d'animaux dans sa ferme, mais il a oublié son dictionnaire chez lui.

Gustave est toujours là, et lui fournit un dictionnaire qu'il a reconstitué de mémoire : `ferme_gaston_secours`

Les dictionnaires suivants sont-ils identiques ? 

* `ferme_gaston = {"lapin": 11, "vache": 7, "cochon": 2, "cheval": 4}` et
* `ferme_gaston_secours = {"cochon": 2, "lapin": 11, "cheval": 4, "vache": 7}`

Tester ci-dessous

{{IDE('scripts/ferme_4')}}

!!! abstract "Dictionnaire : précisions supplémentaires"

    Un dictionnaire est un ensemble **non ordonné de couples (clé, valeur)**.  
    Chaque **clé** est présente de façon **unique** dans le dictionnaire.

    Un dictionnaire s'écrit avec des **accolades {clé : valeur …}**.

## VI. Gaston part au marché

Gaston veut acheter des pommes et des poires au marché. Il est très méthodique, et très fier d'avoir appris à utiliser les dictionnaires Python. 
Il décide donc de constituer un dictionnaire dans lequel il va noter, au fur et à mesures qu'il les voit sur les étalages, les différentes variétés de pommes et
 de poires, avec leur prix au kilo.


???+ question

    Tester ci-dessous

    {{IDE('scripts/marche_1')}}

    ??? success "Que s'est-il passé ?"

        Les clés doivent être de type immuable (non "mutable" en anglais). On ne peut pas choisir des listes comme clés.

???+ "Aidez Gaston"

    Créer un dictionnaire valide, avec les renseignements notés par Gaston.

    {{IDE('scripts/marche_2')}}

    ??? success "Solution"

        Il y a plusieurs possibilités : 
        
        * 
        ```python
        fruits_2 = {("pomme", "Pink"): 2.1,  
                    ("poire", "Williams"): 2.5,  
                    ("pomme", "Golden"): 3.0,  
                    ("poire", "Conférence"): 2.8,
                    }
        ```
        * 
        ```python
        fruits_2 = {"pomme Pink": 2.1, 
                    "poire Williams": 2.5, 
                    "pomme Golden": 3.0, 
                    "poire Conférence": 2.8,
                    }
        ```
        *
        ```python
        fruits_2 = {"Pink": ["pomme", 2.1],
                    "Williams": ["poire", 2.5],
                    "Golden": ["pomme", 3.0],
                    "Conférence": ["poire", 2.8],
                    }
        ```
        * Vous avez peut-être eu d'autres idées encore ... 😊  
        🖐️ Dans ce cas-là, appelez votre professeur pour lui montrer. Il voudra peut-être le partager avec toute la classe ...


!!! abstract "Dictionnaire : encore plus de précisions"

    Un dictionnaire est un ensemble **non ordonné de couples (clé, valeur) notés `clé : valeur`**.  
    Chaque **clé** est présente de façon **unique** dans le dictionnaire.

    Un dictionnaire s'écrit avec des **accolades {clé : valeur …}**.

    **Les clés doivent être de types immuables** (ne peuvent pas être modifiées).  
    🌵 On peut donc choisir comme clé des nombres, des chaînes de caractères, des tuples d'éléments de types immuables,  mais **jamais des listes ni des dictionnaires**.

    😊 En revanche, une valeur peut être de n'importe quel type : nombre, liste, chaine de caractères, tuple, ou même dictionnaire.
