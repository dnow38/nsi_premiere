# Exercices sur les Listes

##  Exercice 1: QCM

???+ question "Parcours sur les indices ou sur les valeurs ?"

    On considère un `#!py tableau` **non vide** contenant des valeurs quelconques. Indiquer dans chaque cas le bon type de parcours à effectuer.

    * On souhaite déterminer **l'indice de la valeur maximale**.

        === "Cocher la bonne réponse"
            
            - [ ] Parcours sur les indices
            - [ ] Parcours sur les valeurs

        === "Solution"
            
            - :white_check_mark: Parcours sur les indices
            - :x: Parcours sur les valeurs

    * On souhaite calculer **la somme des valeurs**.

        === "Cocher la bonne réponse"
            
            - [ ] Parcours sur les indices
            - [ ] Parcours sur les valeurs

        === "Solution"
            
            - :x: Parcours sur les indices
            - :white_check_mark: Parcours sur les valeurs

    * On souhaite **créer un nouveau tableau ne contenant que les valeurs de la première moitié de `#!py tableau`**.

        === "Cocher la bonne réponse"
            
            - [ ] Parcours sur les indices
            - [ ] Parcours sur les valeurs

        === "Solution"
            
            - :white_check_mark: Parcours sur les indices
            - :x: Parcours sur les valeurs

    * On souhaite **déterminer les deux *extrema* (minimum et maximum)**.

        === "Cocher la bonne réponse"
            
            - [ ] Parcours sur les indices
            - [ ] Parcours sur les valeurs

        === "Solution"
            
            - :x: Parcours sur les indices
            - :white_check_mark: Parcours sur les valeurs

    * On souhaite **élever au carré toutes les valeurs du `#!py tableau`** en écrivant les nouvelles valeurs dans le même `#!py tableau`. Par exemple `#!py [2, 3, 4]` deviendrait `#!py [4, 9, 16]`.

        === "Cocher la bonne réponse"
            
            - [ ] Parcours sur les indices
            - [ ] Parcours sur les valeurs

        === "Solution"
            
            - :white_check_mark: Parcours sur les indices
            - :white_check_mark: Parcours sur les valeurs avec une liste en compréhension : `#!py tableau = [x * x for x in tableau]`

## Exercice 2 : Les notes d'Alice - Saison 1

![bulletin](data/notes_Alice.jpg){ width=20%; : .center }

**1.**	Alice veut créer la liste des notes qu’elle a obtenues ce trimestre en NSI.  
Ces notes ne sont pas forcément des nombres entiers.  
Ecrire une fonction qui demande de saisir les notes une par une, et qui renvoie `liste_notes`, la liste des notes qu’elle a obtenues.  

??? tip "Astuce 1"

    Vous pouvez prendre en paramètre de la fonction le nombre de notes à saisir.

??? tip "Astuce 2"

    vous pouvez initialiser `liste_notes = []`, puis ajouter dans la liste les notes une par une avec une boucle.

???+ question "Compléter le code"

    {{IDE('scripts/alice_creation_liste')}}

??? success "Solution"

    ```python
    def creation_liste(n):
        """
        n est un entier donnant la taille de la liste à saisir
        La fonction renvoie la liste saisie
        """
        liste_notes = []
        for i in range(n):
            note = float(input("saisir votre note : "))
            liste_notes.append(note)
        return liste_notes
    ```

**2.**	Créer une fonction `moyenne` qui prend en paramètre une liste `lst` et renvoie la moyenne des éléments de cette liste.  
  

???+ question "Compléter le code"

    Compléter le script pour qu’il affiche la moyenne des notes saisies par Alice.

    {{IDE('scripts/alice_moyenne_liste')}}

??? success "Solution"

    ```python
    def moyenne(lst):
        """
        Cette fonction renvoie la moyenne de la liste lst
        """
        somme = 0  #initialisation de la somme des notes
        for note in lst:  # calcul de la somme des notes avec une boucle
            somme = somme + note
        moyenne = somme/len(lst)  # len(liste) est le nombre de notes
        return moyenne

    n = int(input("nombre de notes à saisir : "))
    notes = creation_liste(n)
    print(notes)  
    moyenne_notes = moyenne(notes)
    print(moyenne_notes)
    ```

   
## Exercices du site "CodEx"

!!! warning "Remarque"

    Ne faire que les exercices dont les liens sont donnés ici, car certains se trouvant sur le menu de gauche nécessitent des connaissances que nous n'avons pas encore vues.


???+ question "Exercice 3 : Recherche d'indice - non guidé"

    Il s'agit de déterminer l'indice de la plus petite valeur dans un tableau non-vide.

    [Indice du minimum d'un tableau](https://codex.forge.apps.education.fr/exercices/ind_min/){ .md-button target="_blank" rel="noopener" }

???+ question "Exercice 4 : Recherche de valeur - non guidé"

    La recherche de la valeur maximale dans un tableau. Classique.
    
    [Maximum](https://codex.forge.apps.education.fr/exercices/maximum_nombres/){ .md-button target="_blank" rel="noopener" }

???+ question "Exercice 5 : Lecture dans un tableau - non guidé"

    On donne les altitudes des différentes étapes d'une course en montagne. On demande quel est le dénivelé positif total.

    [Dénivelé positif](https://codex.forge.apps.education.fr/exercices/denivele_positif/){ .md-button target="_blank" rel="noopener" }


???+ question "Exercice 6 : Comparaison d'éléments consécutifs - non guidé"

    Le tableau fourni est-il trié ?

    [Est trié ?](https://codex.forge.apps.education.fr/exercices/est_trie/){ .md-button target="_blank" rel="noopener" }


## Exercice 7 : Modification d'un tableau


Cet exercice est à réaliser pour introduire la notion "Affectations et listes" de la section suivante.

On se donne un tableau, une valeur cible et une valeur de remplacement et il faut parcourir le tableau et remplacer la cible par la nouvelle valeur.


Écrire la fonction `remplacer` prenant en argument :

* une liste d'entiers `valeurs`
* un entier `valeur_cible`
* un entier `nouvelle_valeur`

Cette fonction doit renvoyer une **nouvelle** liste contenant les mêmes valeurs que `valeurs`, dans le même ordre, sauf `valeur_cible` qui a été remplacée par `nouvelle_valeur`. 

⚠️ **La liste passée en paramètre ne doit pas être modifiée**.

???+ example "Exemples"

    ```pycon
    >>> valeurs = [3, 8, 7]
    >>> remplacer(valeurs, 3, 0)
    [0, 8, 7]
    >>> valeurs
    [3, 8, 7]
    ```

    ```pycon
    >>> valeurs = [3, 8, 3, 5]
    >>> remplacer(valeurs, 3, 0)
    [0, 8, 0, 5]
    >>> valeurs
    [3, 8, 3, 5]
    ```

    
???+ question "Compléter"

    {{ IDE('scripts/remplacer') }}




## Exercice 8 : Les notes d'Alice - Saison 2 

???+ question "Les notes d'Alice - Saison 2"

    <div class="centre">
    <iframe 
    src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/notes_Alice_tuples_sujet.ipynb"
    width="900" height="900" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

    ⏳ La correction viendra bientôt ..

<!--- La correction à télécharger plus tard A SORTIR DE L'ADMONITION

😀 La correction est arrivée ...
    <div class="centre">
    <iframe 
    src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/notes_Alice_tuples_correction.ipynb"
    width="900" height="900" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>
 -->

## Exercice 9 : Soleil couchant
    
???+ question "Recherche de maxima relatifs - non guidé"

    Combien de bâtiments sont éclairés par le soleil couchant. Le sujet est original mais l'algorithme très classique.

    [Soleil couchant](https://codex.forge.apps.education.fr/exercices/soleil_couchant/){ .md-button target="_blank" rel="noopener" }
    

## Exercice 10 : Recherche d'indices

_Auteur : Sébastien HOARAU_


Ecrire une fonction qui renvoie la liste croissante des indices d'un certain élément dans un tableau.

Il est recommandé d'utiliser une liste en compréhension.

Écrire une fonction `indices` qui prend en paramètres un entier `element` et un tableau `entiers` d'entiers. Cette fonction renvoie la liste croissante des indices de `element` dans le tableau `entiers`.

Cette liste sera donc vide `[]` si `element` n'apparait pas dans `entiers`.

> On n'utilisera ni la méthode `index`, ni la méthode `max`.

???+ example "Exemples"

    ```pycon
    >>> indices(3, [3, 2, 1, 3, 2, 1])
    [0, 3]
    >>> indices(4, [1, 2, 3])
    []
    >>> indices(10, [2, 10, 3, 10, 4, 10, 5])
    [1, 3, 5]
    ```

???+ question "Compléter ci-dessous"

    {{ IDE('scripts/recherche_indices', SANS="index, max") }}


## Exercice 11

???+ question "Exercice 11"
    === "Énoncé"
        ![image](data/sanglier.jpg){: .center width=60%}
        Résolvez le **Pydéfi** proposé à [cette adresse](https://pydefis.callicode.fr/defis/Herculito04Sanglier/txt)

        Vous pouvez vous créer un compte pour valider vos résultats, ce site (géré par l'Académie de Poitiers) est **remarquable**. 
    
    === "Correction"
       
        ```python linenums='1'
        lst = [0, 50, 40, 100, 70, 90, 0]

        total = 0
        for i in range(len(lst)-1):
            if lst[i] > lst[i+1]:
                nb_pierres = (lst[i]-lst[i+1])//10 + 1
                total += nb_pierres

        print(total)
        ```

        (à faire avec les valeurs de test) 
      
        
## Exercice 12

???+ question "Exercice 12"

    === "Énoncé"
        On donne la liste ```jours``` suivante :
        ```python
        jours = ["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"]
        ```  

        On rappelle que la fonction ```len``` permet d'obtenir le nombre de caractères d'une chaine de caractères :

        ```python
        >>> len("test")
        4
        ```

        **Q1.** Créer **en compréhension** une liste ```lst1``` contenant uniquement les jours comportant 5 lettres.
        
        ??? success "Correction" 
            ```python
            lst1 = [day for day in jours if len(day) == 5]
            ```            
        

        **Q2.** Créer **en compréhension** une liste ```lst2``` contenant uniquement les jours comportant la lettre ```a``` dans leur nom.
        
        ??? success "Correction" 
            ```python
            lst2 = [day for day in jours if 'a' in day]
            ```        
        

        **Q3a.** Créer une fonction ```compte_e``` qui prend en paramètre une chaine de caractères et qui renvoie le nombre de ```e``` que contient cette chaine de caractères.
       
        ??? success "Correction" 
            ```python linenums='1'
            def compte_e(mot):
                compteur = 0
                for lettre in mot:
                    if lettre == 'e':
                        compteur += 1
                return compteur
            ```    
       

        **Q3b.**  Créer **en compréhension** une liste ```lst4``` contenant uniquement les jours comportant deux fois la lettre ```e``` dans leur nom.

        
        ??? success "Correction" 
            ```python
            lst4 = [day for day in jours if compte_e(day) == 2]
            ```    
        







## Exercice 13

???+ question "Exercice 13"
    === "Énoncé"
        On donne le tableau ```m``` suivant :
        ```python linenums='1'
        m = [[17, 71, 75, 89, 45, 10, 54, 26, 59, 47, 57, 64, 44], \
            [67, 25, 47, 49, 28, 40, 10, 17, 77, 35, 87, 15, 68], \
            [66, 89, 28, 43, 16, 14, 12, 21, 68, 22, 14, 18, 59], \
            [60, 35, 30, 23, 22, 37, 49, 89, 82, 80, 85, 28, 17], \
            [61, 42, 39, 46, 29, 38, 85, 72, 44, 60, 47, 35, 52], \
            [44, 28, 24, 40, 71, 71, 46, 25, 78, 54, 66, 84, 52], \
            [29, 71, 7, 38, 71, 60, 71, 60, 16, 82, 35, 39, 23], \
            [18, 61, 38, 7, 8, 32, 67, 43, 23, 28, 29, 16, 30], \
            [45, 30, 74, 9, 84, 78, 11, 80, 42, 64, 9, 39, 26], \
            [78, 57, 54, 66, 57, 63, 10, 42, 61, 19, 26, 25, 53], \
            [38, 87, 10, 64, 75, 26, 14, 68, 19, 33, 75, 50, 18], \
            [52, 81, 24, 67, 37, 78, 17, 19, 61, 82, 57, 24, 54]]

        ```
        Afficher successivement chaque ligne du tableau en respectant les règles suivantes :

        - si le nombre est divisible par 7, afficher ```*```, sinon afficher une espace ``` ```
        - sur une même ligne, on affichera tous les symboles côte à côte, en rajoutant le paramètre ```end = ''``` à la fonction ```print```. (*exemple :* ```print('*', end = '')``` )
        - on ira à la ligne à la fin de chaque ligne, par l'instruction ```print()```     
    === "Correction"
        
        ```python linenums='1'
        m = [[17, 71, 75, 89, 45, 10, 54, 26, 59, 47, 57, 64, 44], \\
            [67, 25, 47, 49, 28, 40, 10, 17, 77, 35, 87, 15, 68], \\
            [66, 89, 28, 43, 16, 14, 12, 21, 68, 22, 14, 18, 59], \\
            [60, 35, 30, 23, 22, 37, 49, 89, 82, 80, 85, 28, 17], \\
            [61, 42, 39, 46, 29, 38, 85, 72, 44, 60, 47, 35, 52], \\
            [44, 28, 24, 40, 71, 71, 46, 25, 78, 54, 66, 84, 52], \\
            [29, 71, 7, 38, 71, 60, 71, 60, 16, 82, 35, 39, 23], \\
            [18, 61, 38, 7, 8, 32, 67, 43, 23, 28, 29, 16, 30], \\
            [45, 30, 74, 9, 84, 78, 11, 80, 42, 64, 9, 39, 26], \\
            [78, 57, 54, 66, 57, 63, 10, 42, 61, 19, 26, 25, 53], \\
            [38, 87, 10, 64, 75, 26, 14, 68, 19, 33, 75, 50, 18], \\
            [52, 81, 24, 67, 37, 78, 17, 19, 61, 82, 57, 24, 54]]

        for ligne in m:
            for elt in ligne:
                if elt % 7 == 0:
                    print('*', end = '')
                else:
                    print(' ', end = '')
            print('')
        ```
        
## Exercice 14

???+ question "Exercice 14"
    === "Énoncé"
        Pydéfi **Insaisissable matrice** (version originale à [cette adresse](https://pydefis.callicode.fr/defis/AlgoMat/txt){. target="_blank"})

        On considère la matrice suivante :

        ```python
        -------------------------------
        | 17 | 3  | 4  | 14 | 5  | 17 |
        -------------------------------
        | 8  | 16 | 3  | 17 | 14 | 12 |
        -------------------------------
        | 13 | 5  | 15 | 4  | 16 | 3  |
        -------------------------------
        | 14 | 7  | 3  | 16 | 3  | 2  |
        -------------------------------
        | 6  | 1  | 16 | 10 | 5  | 13 |
        -------------------------------
        | 11 | 1  | 9  | 11 | 18 | 8  |
        -------------------------------
        ``` 
        
        ```python
        M = [[17, 3, 4, 14, 5, 17], [8, 16, 3, 17, 14, 12], [13, 5, 15, 4, 16, 3], [14, 7, 3, 16, 3, 2], [6, 1, 16, 10, 5, 13], [11, 1, 9, 11, 18, 8]]
        ```

        Cette matrice va évoluer au cours du temps, et le contenu `k` d'une case est transformé, à chaque étape en `(9*k + 3) % 19`.

        Rappelons que  `a % b` donne le reste de la division entière de `a` par `b`.

        À chaque étape de calcul, tous les nombres de la matrice sont simultanément modifiés. L'entrée du problème est le nombre d'étapes à appliquer (ici : **39**). Vous devez répondre en donnant la somme des valeurs contenues dans la matrice après application de toutes les étapes.



    === "Correction"
        
        ```python linenums='1'
        M = [[17, 3, 4, 14, 5, 17], [8, 16, 3, 17, 14, 12], [13, 5, 15, 4, 16, 3], [14, 7, 3, 16, 3, 2], [6, 1, 16, 10, 5, 13], [11, 1, 9, 11, 18, 8]]

        def f(k):
            return (9*k + 3) % 19

        def tour():
            for i in range(6):
                for j in range(6):
                    M[i][j] = f(M[i][j])

        for _ in range(39):
            tour()

        somme = 0
        for i in range(6):
            for j in range(6):
                somme += M[i][j]

        print(somme)

        ```

      
## Exercice 15

???+ question "Exercice 15"
    === "Énoncé"
        **D'après Advent Of Code 2021, day02**

        ![image](data/aoc.png){: .center}
        

        Un sous-marin peut se déplacer horizontalement (toujours vers la droite) grâce à l'instruction ```forward``` suivie d'un nombre.

        Il peut aussi monter ou descendre, grâce aux instructions ```up``` ou ```down```, elles aussi suivies d'un nombre.

        Un grand nombre d'instructions successives sont données. Le but de l'exercice est de trouver le produit final de l'abscisse du sous-marin et de sa profondeur.

        Exemple :

        ```
        forward 5
        down 5
        forward 8
        up 3
        down 8
        forward 2
        ``` 

        Après ces déplacements, le sous-marin se trouve à l'abscisse 15 et à la profondeur 10. La réponse à l'énigme serait donc 150.

        - [énoncé orginal](https://adventofcode.com/2021/day/2){. target="_blank"}

       
        - Téléchargez le fichier [input.txt](data/input.txt). Votre fichier ```.py``` de travail doit se situer dans le même répertoire que le fichier ```input.txt```.

        :arrow_right: **Parsing des données**

        *Parser* des données consiste à les récupérer et les rendre exploitables. C'est quelque chose de souvent pénible (dans les énigmes de code ET dans la vraie vie). Pourtant de la qualité du parsing (et surtout de la structure de stockage choisie) va dépendre la difficulté (ou non) de l'exploitation des données.

        Proposition de parsing :

        ```python linenums='1'
        data_raw = open('input.txt').read().splitlines()
        lst_raw = [d.split(' ') for d in data_raw]
        lst = [[l[0], int(l[1])] for l in lst_raw]

        ```

        Exécutez ce code et observez ce que contient la liste ```lst```.

        :arrow_right: **Résolution de l'énigme**
        
        À la fin de toutes ses manœuvres, quel est le produit de l'abscisse du sous-marin et de sa profondeur ?
     


    === "Correction"
        
        ```python
        data_raw = open('input.txt').read().splitlines()
        lst_raw = [d.split(' ') for d in data_raw]
        lst = [[l[0], int(l[1])] for l in lst_raw]

        x = 0
        y = 0

        for couple in lst:
            direction = couple[0]
            valeur = couple[1]
            if direction == 'forward':
                x += valeur
            if direction == 'down':
                y += valeur
            if direction == 'up':
                y -= valeur

        print(x*y)
        ```

        La réponse est donc 1746616.
        