# Thème 2 : Représentation des données

1. [Listes](../01_chapitre_1/cours/)
2. [Tuples](../02_chapitre_2/cours/)
3. [Dictionnaires](../03_chapitre_3/01_quesako)
4. [Bases](../04_chapitre_4/cours/)
5. [Booléens](../05_chapitre_5/cours/)
6. [Codage des caractères](../06_chapitre_6/cours/)
7. [Codage des entiers](../07_chapitre_7/cours/)
8. [Codage des non-entiers](../08_chapitre_8/cours/)