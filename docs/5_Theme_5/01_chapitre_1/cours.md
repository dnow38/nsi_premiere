title: Chapitre 5.1 - Manipulation de fichiers

# Chapitre 5.1 - Manipulation de fichiers
![image](data/BO.png){: .center}

![image](data/meme.png){: .center}



## 1. Lire un fichier

Le [Projet Gutenberg](https://www.gutenberg.org/) est une bibliothèque en ligne regroupant les versions numériques d'ouvrages libres de droits. On y a récupéré le fichier des [Fables de la Fontaine](../fables.txt) au format `txt`.

### 1.1 La fonction `#!py open`

Ouvrir un fichier avec Python est simple. On utilise la fonction `open` présente dans la [bibliothèque native](https://docs.python.org/fr/3/library/functions.html#open).

Cette fonction prend plusieurs paramètres. Retenons les trois suivants (déroulez pour avoir des détails):

??? info "`#!py file` : le nom du fichier"
    
    Le nom du fichier (avec son extension donc, au format `#!py str`).
    
    Attention, par « nom » on désigne **l'adresse** du fichier. Cette adresse peut-être :
    
    * *absolue*, depuis la racine du disque, `C:/home/nico/Documents/donnees_en_table/fables.txt` par exemple ;
    
    * ou *relative*, depuis le fichier Python qui ouvre le fichier, `fables.txt` par exemple si les deux fichiers sont dans le même dossier.

    On conseille **très fortement** d'utiliser des adresses relatives.

??? info "`#!py mode` : le mode d'ouverture"
    
    Le mode d'ouverture du fichier (au format `#!py str`).
    
    On peut retenir les modes suivants :

    | Caractère  | Signification                                                  |
    | :--------- | :------------------------------------------------------------- |
    | `#!py 'r'` | ouvre en lecture, en mode texte                                |
    | `#!py 'w'` | ouvre en écriture, en effaçant le contenu du fichier           |
    | `#!py 'a'` | ouvre en écriture, en ajoutant les données à la fin du fichier |
    | `#!py 'b'` | mode binaire                                                   |

    On notera que le mode `#!py 'w'` efface directement le contenu du fichier, il n'y a pas de message d'avertissement !

??? info "`#!py encoding` : l'encodage utilisé"
    
    Le type d'encodage (au format `#!py str`).
    
    L'encodage d'un fichier correspond à la façon dont le programme qui l'ouvre doit interpréter les données qu'il contient.
    
    Un fichier est stocké en machine sous forme d'une succession de bits. Dans la table ASCII initiale, 7 bits représentent un caractère. Dans la table ASCII étendue, il faut 8 bits pour représenter un caractère. L'encodage `#!py 'utf-8'` est plus subtil : les caractères « courants » (l'alphabet latin par exemple) sont codés sur 8 bits, les caractères moins « courants » sur 16 voire 24 bits.

    Changer l'encodage lors de l'ouverture d'un fichier ne modifie pas les données contenues dans le fichier mais **la façon de les lire**.
    
    Par exemple les bits `#!py 010100110110100101101101011100000110110001100101001000000111010001100101011110000111010001100101` lus :
    
    * avec l'encodage `#!py 'utf-8'` donnent `#!py 'Simple texte'`,
    
    * avec l'encodage `#!py 'utf-16 LE'` donnent `#!py '楓灭敬琠硥整'` !

    L'encodage le plus classique pour nous sera `#!py 'utf-8'`.


La fonction `#!py open` peut être utilisée de deux façons :

* utilisation classique, on ouvre **et** on ferme le fichier :

    ```python
    fichier = open(file="fichier.txt", mode="r", encoding="utf-8")
    # Traitement du fichier
    fichier.close()
    ```

    Techniquement, on devrait même utiliser un `#!py try ... except` au cas où le fichier est inaccessible :

    ```python
    try:
        fichier = open(file="fichier.txt", mode="r", encoding="utf-8")
        # Traitement du fichier
        fichier.close()
    except IOError:  # Le fichier est inaccessible
        print("Le fichier est inaccessible")
    ```

* utilisation avec `with`, il est inutile de fermer le fichier et de gérer les erreurs, c'est automatique :

    ```python
    with open(file="fichier.txt", mode="r", encoding="utf-8") as fichier:
        # Traitement du fichier
    ```

On utilisera la seconde méthode.

### 1.2 Lire le fichier, des lignes, une ligne !

Une fois le fichier ouvert, on peut réaliser différentes actions. Si l'objet renvoyé par `#!py open` s'appelle `#!py fichier`, on peut :

* `#!py fichier.read()` : lit la totalité du fichier. Renvoie une unique chaîne de caractères.
* `#!py fichier.readlines()` : lit la totalité du fichier **ligne par ligne**. Renvoie la liste contenant les différentes lignes.
* `#!py fichier.readline()` : lit la prochaine ligne du fichier **ligne par ligne**. Renvoie une chaîne de caractères.

??? note "Écrire ?"

    Si le fichier est ouvert en mode *écriture*, on peut écrire en faisant `#!py fichier.write("texte à écrire")`.

    Attention toutefois à ne pas écraser le contenu du fichier !

Les deux scripts ci-dessous sont donc équivalents :

```python
with open(file="fichier.txt", mode="r", encoding="utf-8") as fichier:
    contenu = fichier.readlines()
```

```python
contenu = []
with open(file="fichier.txt", mode="r", encoding="utf-8") as fichier:
    for ligne in fichier:
        contenu.append(ligne)
```

La seconde permet toutefois de traiter précisément chaque ligne lue. On l'utilisera préférentiellement.

### 1.3 Des cigales et des fourmis

Nous sommes donc équipés pour ouvrir et lire nos fichiers. 

???+ question "Lire les fables de la Fontaine"

    Le fichier contenant les fables de la Fontaine est nommé `#!py fables.txt`, il est situé dans le même dossier que le fichier Python manipulé par l'éditeur. Il est encodé en `#!py utf-8`.

    Compléter le script afin d'ouvrir ce fichier et charger ses différentes lignes dans une liste.

    {{ IDE('pythons/ouverture/exo', MAX=5) }}

On obtient la liste suivante :

```python
fables = [
    'FABLES DE LA FONTAINE\n',
    '\n',
    'I\n',
    '\n',
    'LA CIGALE ET LA FOURMI.\n',
    '\n',
    '\n',
    'La cigale, ayant chanté\n',
    "Tout l'été,\n",
    ...
]
```

Le `#!py '\n'` que l'on observe à plusieurs reprises est le caractère de retour à la ligne. On peut le supprimer en faisant `#!py ligne.strip()`. En effet, la méthode `#!py strip` supprime les caractères « blancs » au début ou à la fin d'une chaîne de caractères.

La lecture du fichier devient alors :

```python
fables = []
with open(file="fables.txt", mode="r", encoding="utf-8") as fichier:
    for ligne in fichier:
        ligne_propre = ligne.strip()  # suppression des \n
        fables.append(ligne_propre)
```

On obtient :

```python
fables = [
    'FABLES DE LA FONTAINE',
    '',
    'I',
    '',
    'LA CIGALE ET LA FOURMI.',
    '',
    '',
    'La cigale, ayant chanté',
    "Tout l'été,",
    ...
]
```
??? question "Relire les fables de la Fontaine"

    On a supprimé les caractères de retour à la ligne mais il reste désormais des lignes vides dans la liste `#!py fables`.

    Compléter le script afin d'ouvrir ce fichier, charger ses différentes lignes **non vides** dans une liste.

    {{ IDE('pythons/ouverture_propre/exo', MAX=5) }}

### 1.4 Comptons les moutons

Nous avons donc récupéré l'ensembles des fables dans une liste contenant toutes les lignes. Nous pouvons désormais faire des requêtes sur cette liste.

Combien de vers contiennent le mot `#!py "mouton"` ? Pour le savoir on fait :

```python
moutons = [vers for vers in fables if "mouton" in vers.lower()]  # tous les vers contenant "mouton"
print(len(moutons))  # le nombre de vers
```

!!! tip "Astuce"

    On passe le vers en minuscule avec `#!py vers.lower()` afin de trouver en une seule passe les chaînes `#!py "mouton"`, `#!py "Mouton"`, `#!py "MOUTON"`, *etc*

??? question "Requêtes dans les fables"

    Compléter le script ci-dessous afin d'effectuer les requêtes demandées.
    
    Toutes les recherches de chaînes de caractères seront insensibles à la casse (utiliser `#!py vers.lower()` comme ci-dessus).

    {{ IDE('pythons/requetes_fables/exo', MAX=10)}}


Les fichiers CSV (pour Comma Separated Values) sont des fichiers-texte (ils ne contiennent aucune mise en forme) utilisés pour stocker des données, séparées par des virgules (ou des points-virgules, ou des espaces...). Il n'y a pas de norme officielle du CSV.  



## 2. Les fichiers `csv` et `json`

### 2.1 Découverte

Le site [data.gouv](https://www.data.gouv.fr/fr/) propose de nombreux jeux de données en libre accès.

Les fichiers correspondants sont souvent proposés aux formats 

* `csv` pour _**C**omma **S**eparated **V**alues_,
* `json` pour _**J**ava**S**cript **O**bject **N**otation_.

Ces deux formats de fichiers permettent de **présenter des données textuelles**. Voici par exemple les mêmes informations présentées dans chacun des formats :

* au format  `csv` (le fichier s'appelle `amis.csv`):

    ```title="📑 Données CSV"
    nom,âge,ville,passion
    Jean,26,Paris,VTT
    Marion,28,Lyon,badminton
    ```

* au format `json` (le fichier s'appelle `amis.json`):

    ```json
	{ "amis": [
        {"nom": "Jean","âge": 26,"ville": "Paris","passion": "VTT"},
        {"nom": "Marion","âge": 28,"ville": "Lyon","passion": "badminton"},
              ]
    }
    ```

Nous travaillerons désormais avec les fichiers `csv`. L'exemple précédent permet de remarquer plusieurs choses :

* un fichier `csv` contient des **données textuelles**,

* les données sont organisées en lignes,

* la première ligne regroupe le nom des **descripteurs** (il y en a quatre ici : `#!py nom`, `#!py âge`, `#!py ville` et `#!py passion`),

* les autres lignes contiennent des **enregistrements** (il y en a deux ici : `#!py Jean,26,Paris,VTT` et `#!py Marion,28,Lyon,badminton`),

* au sein de chaque ligne, les valeurs sont délimitées par un **séparateur** (ici le caractère `#!py ","`),

* les données peuvent être de types différents. Ici le `#!py nom`, la `#!py ville` et la `#!py passion` sont des chaînes de caractères, l'`#!py âge` un entier.

!!! danger "Attention"

    La réalité n'est pas aussi simple :

    * il arrive que **la première ligne ne contienne pas les entêtes**. Ils peuvent être listés dans un fichier annexe ou... perdus !
    
    * on trouve parfois **une seconde ligne contenant les types des données** (entier, texte...).
    
    * le séparateur **n'est pas toujours une virgule**. Il est courant que l'on trouve des `#!py ";"` dans les fichiers français car la virgule est utilisée comme séparateur décimal.

??? question "Premiers contacts"

    On considère les deux fichiers `csv` ci-dessous (on n'en donne que les trois première lignes) :

    * `petanque.csv` ([Localisation des terrains de pétanque dans l'agglomération de Tours](https://www.data.gouv.fr/fr/datasets/terrain-de-petanque-tours-metropole/)) :

    ```title="📑 Données CSV"
    geo_point_2d;nb_equipement;commune;cp
    (47.3392380011,0.7162219998);1;Chambray-lès-Tours;37170
    (47.3300100011,0.6120900019);5;Ballan-Miré;37510
    ```
    
    * `bac.csv` ([Résultats au baccalauréat par académie](https://www.data.gouv.fr/fr/datasets/le-baccalaureat-par-academie/)) :

    ```title="📑 Données CSV"
    session,academie,sexe,diplome_specialite,nombre_d_inscrits,nombre_d_admis_totaux
    INT,TEXT,TEXT,TEXT,INT,INT
    2021,AIX-MARSEILLE,FILLES,BAC PRO AG 21302 GEST MILIEUX NATURELS FAUNE,16,13
    ```

    Cochez la ou les bonnes réponses.

    === "Propositions"
        
        - [ ] Le séparateur du fichier `petanque.csv` est la virgule
        - [ ] Le fichier `petanque.csv` compte quatre descripteurs
        - [ ] Le séparateur du fichier `bac.csv` est la virgule
        - [ ] `INT` est un descripteur du fichier `bac.csv`

    === "Solution"
        
        - :x: Le séparateur du fichier `petanque.csv` est le point-virgule
        - :white_check_mark: Le fichier `petanque.csv` compte bien quatre descripteurs
        - :white_check_mark: Le séparateur du fichier `bac.csv` est bien la virgule
        - :x: `INT` est un type de données

??? question "Problème !"

    On propose ci-dessous un extrait d'un fichier `csv`. Identifiez les trois problèmes présents :

    ```title="📑 Données CSV"
    nom,prenom,identifiant;mdp,derniere_connexion
    Clark,Sarah,sclark,k012345,20230105,
    Mapple,Marc,marc.mapple,20221231
    ```

    ??? success "Solution"

        1. Le séparateur n'est pas constant : il y a un point-virgule dans la première ligne
        2. Il y a une virgule en trop en fin de deuxième ligne
        3. Il manque un champs sur la troisième ligne

        On pourrait aussi noter le **gros** problème qui consiste à stocker les mots de passe des utilisateurs en clair dans un fichier !

??? note "Autres formats..."

    Les fichiers `csv` et `json` ne sont pas les seuls formats permettant de conserver des données. 
    
    On peut aussi retenir le format `xml` pour _e**X**tensible **M**arkup **L**anguage_ qui utilise des balises au même titre que le `html` :


    ```xml title="📑 Données XML"
    <?xml version="1.0" encoding="UTF-8"?>
    <amis>
        <personne>
            <nom>Jean</nom>
            <âge>26</âge>
            <ville>Paris</ville>
            <passion>VTT</passion>
        </personne>
        <personne>
            <nom>Marion</nom>
            <âge>28</âge>
            <ville>Lyon</ville>
            <passion>badminton</passion>
        </personne>
    </amis>
    ```

### 2.2 Création

On l'a dit, les fichiers `csv` et `json` sont des fichiers « texte » classique. Il est donc possible de les créer, d'ouvrir ou de modifier avec un simple éditeur de texte.



2.2.1 Ouverture/modification d'un fichier CSV par des logiciels classiques


- Télécharger le fichier [exemple.csv](../data/exemple.csv)
- Ouvrir avec le Bloc-Notes ce fichier.
- Rajouter une ligne avec une personne supplémentaire, sauvegarder le fichier.
- Ouvrir le fichier avec LibreOffice.


2.2.2 Création avec python


??? question "« *Créer* » un fichier `csv`"

    On souhaite « *créer* » un fichier `csv` recensant les jours fériés en France durant l'année 2023.
    
    Afin de rester dans l'interface proposée par ce site nous travaillerons dans un éditeur Python.

    !!! note "Pour de vrai !"

        Au lieu de travailler dans l'éditeur Python proposé ci-dessous, vous pouvez utiliser votre propre éditeur et réellement créer le fichier `csv` en l'enregistrant avec l'extension `.csv`. Vous pourrez ensuite le lire comme vu plus haut. Remarquez qu'un "copié/collé" de cet écran peut vous faire gagner du temps. Il faudra juste le transformer un peu ...

    Ces jours sont présentés dans le tableau ci-dessous :

    |           motif |     jour | numero |     mois |
    | --------------: | -------: | -----: | -------: |
    |    Jour de l'an | dimanche |      1 |  janvier |
    | Lundi de Pâques |    lundi |     10 |    avril |
    | Fête du travail |    lundi |      1 |      mai |
    |   Victoire 1945 |    lundi |      8 |      mai |
    |       Ascension |    jeudi |     18 |      mai |
    |  Fête Nationale | vendredi |     14 |  juillet |
    |      Assomption |    mardi |     15 |     août |
    |       Toussaint | mercredi |      1 | novembre |
    |  Armistice 1918 |   samedi |     11 | novembre |
    |            Noël |    lundi |     25 | décembre |

    Compléter la chaîne de caractère `contenu` ci-dessous en saisissant le **contenu** du fichier `csv` :

    * on saisira les descripteurs sur la première ligne,
    * on utilisera la virgule comme séparateur.

    Vous pouvez saisir les descripteurs dans l'ordre que vous souhaitez, il faut par contre faire en sorte de saisir les valeurs dans le même ordre !

    {{ IDE('pythons/creation_csv/exo') }}


## 3. Exploitation d'un fichier CSV en Python
L'utilisation d'un tableur peut être délicate lorsque le fichier CSV comporte un très grand nombre de lignes. 
Python permet de lire et d'extraire des informations d'un fichier CSV même très volumineux, grâce à des modules dédiés, comme le bien-nommé `csv` (utilisé ici) ou bien `pandas` (qui sera vu plus tard).


### 3.1 Première méthode. Création d'une liste
Le script suivant :
```python linenums='1'
import csv                          
f = open('exemple.csv', "r", encoding = 'utf-8') # le "r" signifie "read", le fichier est ouvert en lecture seule
donnees = csv.reader(f)  # donnees est un objet (spécifique au module csv) qui contient des lignes

for ligne in donnees:               
    print(ligne)
    
f.close()    # toujours fermer le fichier !
```

donne :

```python
['Prénom', 'Nom', 'Email', 'SMS']
['John', 'Smith', 'john@example.com', '33123456789']
['Harry', 'Pierce', 'harry@example.com', '33111222222']
['Howard', 'Paige', 'howard@example.com', '33777888898']
```




**Problèmes**

1. Les données ne sont pas structurées : la première ligne est la ligne des «descripteurs» (ou des «champs»), alors que les lignes suivantes sont les valeurs de ces descripteurs.
2. La variable `donnees` n'est pas exploitable en l'état. Ce n'est pas une structure connue.


### 3.2 Améliorations. Créaction d'un dictionanire
Au lieu d'utiliser la fonction `csv.reader()`, utilisons `csv.DictReader()`. Comme son nom l'indique, elle renverra une variable contenant des dictionnaires.

Le script suivant :
```python linenums='1'
import csv
f = open('exemple.csv', "r", encoding = 'utf-8')
donnees = csv.DictReader(f)

for ligne in donnees:
    print(dict(ligne))
    
f.close()
```

donne
```python
{'Prénom': 'John', 'Nom': 'Smith', 'Email': 'john@example.com', 'SMS': '33123456789'}
{'Prénom': 'Harry', 'Nom': 'Pierce', 'Email': 'harry@example.com', 'SMS': '33111222222'}
{'Prénom': 'Howard', 'Nom': 'Paige', 'Email': 'howard@example.com', 'SMS': '33777888898'}
```



C'est mieux ! Les données sont maintenant des dictionnaires. Mais nous avons juste énuméré 3 dictionnaires. Comment ré-accéder au premier d'entre eux, celui de John Smith ? Essayons :


```python
>>> donnees[0]

    ---------------------------------------------------------------------------

    TypeError                                 Traceback (most recent call last)

    <ipython-input-3-9914ab00321e> in <module>
    ----> 1 donnees[0]
    

    TypeError: 'DictReader' object does not support indexing
```

### 3.3 Une liste de dictionnaires

Nous allons donc créer une liste de dictionnaires.


Le script suivant :
```python linenums='1'
import csv
f = open('exemple.csv', "r", encoding = 'utf-8')
donnees = csv.DictReader(f)
amis = []
for ligne in donnees:
    amis.append(dict(ligne))
    
f.close()
```

permet de faire ceci :

```python
>>> amis

    [{'Prénom': 'John',
      'Nom': 'Smith',
      'Email': 'john@example.com',
      'SMS': '33123456789'},
     {'Prénom': 'Harry',
      'Nom': 'Pierce',
      'Email': 'harry@example.com',
      'SMS': '33111222222'},
     {'Prénom': 'Howard',
      'Nom': 'Paige',
      'Email': 'howard@example.com',
      'SMS': '33777888898'}]

>>> print(amis[0]['Email'])
    john@example.com

>>> print(amis[2]['Nom'])
  Paige
```
### 3.4 Import d'un fichier `csv` dans une liste de listes

On considère le fichier [temperatures_2020.csv](../temperatures_2020.csv). Ce fichier regroupe les températures minimales, maximales et moyennes dans différentes régions françaises pour certains jours de l'année 2020. Il est dans le dossier de travail et est encodé en `utf-8`.

Les premières lignes du fichier sont données ci-dessous :

```title="📑 Données CSV"
mois,jour,région,tmin,tmax,tmoy
août,13,Pays de la Loire,19.25,25.35,22.3
août,13,Occitanie,17.51,26.55,22.03
```

??? question "Repérer les bonnes informations"

    Observez l'extrait proposé et répondez aux questions suivantes :

    1. Quel est le séparateur utilisé ?
    2. Combien y-a-t-il de descripteurs ?
    3. Quels sont les types des descripteurs ? (entier, nombre décimal, chaîne de caractères...)

    ??? success "Solution"

          1. Le séparateur est la virgule
          2. Il y a six descripteurs
          3. `#!py mois` et `#!py région` sont des chaînes de caractères, `#!py jour` est un entier, les trois autres sont des flottants.


Un fichier `csv` est un fichier contenant des données textuelles : son importation avec Python peut se faire facilement avec `#!py open`.

On propose dans un premier temps d'importer les données dans une liste de listes. Voici un code fonctionnel :

!!! warning inline end "Important"

    Prenez le temps de lire les commentaires !
    
    Cliquez sur les +

```{ .python .annotate }
temperatures = []
with open(file="temperatures_2020.csv", mode="r", encoding="utf-8") as fichier:
    fichier.readline()  # (1)
    for ligne in fichier:  # (2)
        ligne_propre = ligne.strip()  # (3)
        valeurs = ligne_propre.split(",")  # (4)
        temperatures.append(valeurs)
```

1. :warning: Les descripteurs ne sont pas utilisés : on lit la première ligne sans l'ajouter au résultat :warning:
2. :eyeglasses: Parcours et lecture de toutes les lignes restantes :eyeglasses:
3. :broom: on ôte le `#!py \n` en fin de ligne :broom:
4. :scissors: on découpe la ligne à chaque caractère `#!py ","` en une liste de valeurs :scissors:

On obtient le résultat suivant :

```python
temperatures = [
    ["août", "13", "Pays de la Loire", "19.25", "25.35", "22.3"],
    ["août", "13", "Occitanie", "17.51", "26.55", "22.03"],
    ["août", "14", "Pays de la Loire", "17.7", "25.7", "21.7"],
    ...
]
```

Les données ont bien été importées mais elles sont mal typées : les températures sont par exemple stockées sous forme des chaînes de caractères. Sous cette forme la somme `#!py "19.25" + "26.55"` renvoie `#!py "19.2526.55"` !

Il reste donc à **typer les données**. Par défaut celles-ci sont toutes au format `#!py str`. Seuls les nombres entiers et les décimaux sont à typer.

On rappelle les indices ci-dessous :

| Descripteur | `#!py mois` | `#!py jour` | `#!py région` | `#!py tmin`  | `#!py tmax`  | `#!py tmoy`  |
| :---------: | :---------: | :---------: | :-----------: | :----------: | :----------: | :----------: |
|   Indice    |  `#!py 0`   |  `#!py 1`   |   `#!py 2`    |   `#!py 3`   |   `#!py 4`   |   `#!py 5`   |
| Type Python | `#!py str`  | `#!py int`  |  `#!py str`   | `#!py float` | `#!py float` | `#!py float` |

Il est possible de typer toutes les valeurs à l'indice `#!py 1` en faisant : 

```python
for entree in temperatures:
    entree[1] = int(entree[1])  # int convertit une chaîne de caractère en un entier
```

???+ question "Import et typage complets"

    Compléter le script ci-dessous permettant d'importer et de typer convenablement les données du fichier `temperatures_2020.csv`.

    On rappelle que `#!py float` permet de convertir une chaîne de caractère en un flottant.

    {{ IDE('pythons/temperatures_import/exo', MAX=10) }}



## 4. Une application : les joueurs de rugby du TOP14

Le fichier [`top14.csv `](../data/top14.csv)  contient tous les joueurs du Top14 de rugby, saison 2019-2020, avec leur date de naissance, leur poste, et leurs mensurations. 

_Ce fichier a été généré par Rémi Deniaud, de l'académie de Bordeaux._

!!! example "Exercice 1"
    Stocker dans  une variable `joueurs`  les renseignements de tous les joueurs présents dans ce fichier csv.

    ??? success "Correction" 
        ```python linenums='1'
        import csv
        f = open('data/top14.csv', 'r', encoding = 'utf-8')
        donnees = csv.DictReader(f)
        joueurs = []
        for ligne in donnees:
            joueurs.append(dict(ligne))
            
        f.close()
        ```        




### 4.1 Première analyse

!!! example "Exercice 2"
    
    Combien de joueurs sont présents dans ce fichier ?

  
    ??? success "Correction" 
        ```python
        >>> len(joueurs)
        595
        ```        
  


!!! example "Exercice 3"
    Quel est le nom du joueur n°486 ?
  
    ??? success "Correction" 
        ```python
        >>> joueurs[486]['Nom']
        'Wenceslas LAURET'
        ```        
  

### 4.2 Extraction de données particulières

!!! example "Exercice 4"
    En 2019, où jouait Baptiste SERIN ?  

    
    ??? success "Correction" 
        La méthode la plus naturelle est de parcourir toute la liste jusqu'à trouver le bon joueur, puis d'afficher son équipe.
        ```python
        >>> for joueur in joueurs :
                if joueur['Nom'] == 'Baptiste SERIN' :
                    print(joueur['Equipe'])
        ```

        Une méthode plus efficace est d'utiliser une liste par compréhension incluant un test. 

        ```python
        >>> clubSerin = [joueur['Equipe'] for joueur in joueurs if joueur['Nom'] == 'Baptiste SERIN']
        >>> clubSerin
        ```





!!! example "Exercice 5"
    Qui sont les joueurs de plus de 140 kg ?

    *Attention à bien convertir en entier la chaine de caractère renvoyée par la clé ```Poids```, à l'aide de la fonction ```int()```.*

   
    ??? success "Correction" 
        ```python
        >>> lourds = [(joueur['Nom'], joueur['Poids']) for joueur in joueurs if int(joueur['Poids']) > 140]
        >>> lourds
        ```        
   



## 5. Exploitation graphique
Nous allons utiliser le module Matplotlib pour illustrer les données de notre fichier csv.

Pour tracer un nuage de points (par l'instruction ```plt.plot```), Matplotlib requiert :

- une liste ```X``` contenant toutes les abscisses des points à tracer.
- une liste ```Y``` contenant toutes les ordonnées des points à tracer.

### 5.1 Exemple 


```python linenums='1'
import matplotlib.pyplot as plt
X = [0, 1, 3, 6]
Y = [12, 10, 7, 15]
plt.plot(X, Y, 'ro') 
plt.show()
```
Dans l'instruction ```plt.plot(X, Y, 'ro') ``` :

- ```X``` sont les abscisses,
- ```Y``` sont les ordonnées,
- ```'ro'``` signifie :
    - qu'on veut des points (c'est le ```'o'```, plus de choix [ici](https://matplotlib.org/stable/api/markers_api.html){. target="_blank"}).
    - qu'on veut qu'ils soient rouges (c'est le ```'r'``` plus de choix [ici](https://matplotlib.org/stable/gallery/color/named_colors.html#tableau-palette){. target="_blank"}).



![png](data/01_Manipulation_csv_34_0.png){: .center}


### 5.2 Application

!!! example "Exercice 6"
    Afficher sur un graphique tous les joueurs de rugby du Top14, en mettant le poids en abscisse et la taille en ordonnée.

  
    ??? success "Correction" 
        ```python linenums='1'
        X = [int(joueur['Poids']) for joueur in joueurs]
        Y = [int(joueur['Taille']) for joueur in joueurs]
        plt.plot(X, Y, 'ro') 
        plt.show()
        ```


        ![png](data/01_Manipulation_csv_37_0.png){: .center}        
   

!!! example "Exercice 7"
    Faire apparaître ensuite les joueurs évoluant au poste de Centre en bleu, et les 2ème lignes en vert.

 
    ??? success "Correction" 
        ```python linenums='1'
        #tous les joueurs
        X = [int(joueur['Poids']) for joueur in joueurs]
        Y = [int(joueur['Taille']) for joueur in joueurs]
        plt.plot(X, Y, 'ro') 

        #on recolorie les Centres en bleu
        X = [int(joueur['Poids']) for joueur in joueurs if joueur['Poste'] == 'Centre']
        Y = [int(joueur['Taille']) for joueur in joueurs if joueur['Poste'] == 'Centre']
        plt.plot(X, Y, 'bo')

        #on recolorie les 2ème ligne en vert
        X = [int(joueur['Poids']) for joueur in joueurs if joueur['Poste'] == '2ème ligne']
        Y = [int(joueur['Taille']) for joueur in joueurs if joueur['Poste'] == '2ème ligne']
        plt.plot(X, Y, 'go')


        plt.show()
        ```


        ![png](data/01_Manipulation_csv_38_0.png){: .center}        
   









