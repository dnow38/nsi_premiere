# Thème 5 :  Traitement de données

1. [Manipulation de fichiers csv](../01_chapitre_1/cours/)
2. [Les Requêtes](../02_chapitre_2/cours/)
3. [Trier des données](../03_chapitre_3/cours/)
4. [Fusions](../04_chapitre_4/cours/)
5. [Utilisation du module Pandas](../05_chapitre_5/cours/)