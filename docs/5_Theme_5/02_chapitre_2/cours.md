title: Chapitre 5.2 - Les Requêtes
# Chapitre 5.2 - Les Requêtes
## 1. Requêtes simples

### 1.1 🦝 Les prénoms donnés en France

On s'intéresse au fichier [nat2021.csv](../nat2021.csv) regroupant des informations sur les prénoms donnés en France de 1900 à 2021. Ce fichier provient du [site de l'INSEE](https://www.insee.fr/fr/statistiques/2540004?sommaire=4767262#documentation).

!!! warning "Fichier volumineux"

    Le fichier initial comporte 648 616 lignes !
    
    À vrai dire, il est si long que le moteur permettant de faire fonctionner ce site mettrait beaucoup trop de temps à le lire en entier...

    On a donc limité le fichier original aux seules années postérieures à l'an 2000 (inclus).
    
    Même allégé, on déconseille vivement d'afficher la totalité du tableau avec Python...

Les premières lignes de ce fichier sont :

```title="📑 Données CSV"
sexe;prenom;annee;nombre
G;PRENOMS_RARES;2000;12583
G;PRENOMS_RARES;2001;13285
```

!!! note "Remarques"

    `PRENOMS_RARES` correspond à des prénoms donnés moins de 20 fois entre 1900 et 1945 ou entre 1946 et 2021. Tous ces prénoms sont regroupés pour chaque année. On doit donc comprendre qu'en 2000, 12 583 garçons ont reçu un « prénom rare ».

    On remarquera de plus que **tous les prénoms sont saisis en majuscule** (avec potentiellement des accents comme dans `LÉONIE`).

???+ question "Repérer les bonnes informations"

    Observez l'extrait proposé et répondez aux questions suivantes :

    1. Quel est le séparateur utilisé ?
    2. Combien y-a-t-il de descripteurs ?
    3. Quels sont les types des descripteurs ? (entier, nombre décimal, chaîne de caractères...)

    ??? success "Solution"

          1. Le séparateur est le point virgule
          2. Il y a quatre descripteurs
          3. `#!py sexe` et `#!py prenom` sont des chaînes de caractères, `#!py annee` et `#!py nombre` des entiers.

**A. Import(s)**

On laisse ici le choix d'importer ce fichier sous forme d'une liste de listes ou d'une liste de dictionnaires. Vous pouvez aussi vous entraîner en réalisant les deux types d'imports !

Quel que soit le choix fait, la liste regroupant l'ensemble des entrées sera nommée `prenoms` et les données seront **typées**.

On rappelle que la structure du fichier est la suivante :

| Descripteur | `#!py sexe` | `#!py prenom` | `#!py annee` | `#!py nombre` |
| :---------: | :---------: | :-----------: | :----------: | :-----------: |
|   Indice    |  `#!py 0`   |   `#!py 1`    |   `#!py 2`   |   `#!py 3`    |
| Type Python | `#!py str`  |  `#!py str`   |  `#!py int`  |  `#!py int`   |

??? question "Import dans une liste de listes"

    Compléter le script ci-dessous afin d'importer les données dans une liste de listes.

    Les données seront typées.

    !!! warning "Il y a beaucoup de données : le script met un peu de temps à s'exécuter"

    {{ IDE('pythons/prenoms_import_liste/exo', MAX=10) }}

??? question "Import dans une liste de dictionnaires"

    Compléter le script ci-dessous afin d'importer les données dans une liste de dictionnaires.

    Les données seront typées.

    !!! warning "Il y a beaucoup de données : le script met un peu de temps à s'exécuter"

    {{ IDE('pythons/prenoms_import_dico/exo', MAX=10) }}

**B. Requêtes**

Dans ce qui suit, les imports ont été effectués et la liste `prenoms` est chargée en mémoire. On peut désormais l'interroger afin d'en extraire des résultats.

On propose ci-dessous deux versions du même exercice selon le type de listes choisis : liste de listes ou de dictionnaires.

??? question "Requêtes dans une liste de listes"

    La première entrée de la liste est :

    ```pycon
    >>> prenoms[0]
    ['G', 'PRENOMS_RARES', 2000, 12583]
    ```
    
    On peut sélectionner les entrées correspondant au prénom `#!py ADELINE` en faisant :

    ```python
    adeline = [entree for entree in prenoms if entree[1] == "ADELINE"]
    ```

    Vous remarquerez que les prénoms sont saisis en majuscule dans le fichier `csv`.

    On peut de la même façon sélectionner les entrées correspondant au prénom `#!py "ANTOINE` en `#!py 2007` :

    ```python
    antoine_2007 = [entree for entree in prenoms if entree[1] == "ANTOINE" and entree[2] == 2007]
    ```

    Compléter le script ci-dessous afin d'effectuer les requêtes demandées. `prenoms` a déjà été chargé  et correctement typé sous forme d'une liste de listes.

    !!! warning "Il y a beaucoup de données : le script met un peu de temps à s'exécuter"
    
    {{ IDE('pythons/prenoms_requetes_liste/exo', MAX=10) }}

??? question "Requêtes dans une liste de dictionnaires"

    La première entrée de la liste est :

    ```pycon
    >>> prenoms[0]
    {'sexe': 'G', 'prenom': 'PRENOMS_RARES', 'annee': 2000, 'nombre': 12583}
    ```

    On peut sélectionner les entrées correspondant au prénom `#!py ADELINE` en faisant :

    ```python
    adeline = [entree for entree in prenoms if entree["prenom"] == "ADELINE"]
    ```

    Vous remarquerez que les prénoms sont saisis en majuscule dans le fichier `csv`.

    On peut de la même façon sélectionner les entrées correspondant au prénom `#!py "ANTOINE` en `#!py 2007` :

    ```python
    antoine_2007 = [entree for entree in prenoms if entree["prenom"] == "ANTOINE" and entree["annee"] == 2007]
    ```
    
    Compléter le script ci-dessous afin d'effectuer les requêtes demandées. `prenoms` a déjà été chargé  et correctement typé sous forme d'une liste de dictionnaires.

    !!! warning "Il y a beaucoup de données : le script met un peu de temps à s'exécuter"
    
    {{ IDE('pythons/prenoms_requetes_dico/exo', MAX=10) }}

### 1.2 🦝 Populations des communes françaises (Métropole)

On s'intéresse au fichier [pop_hist.csv](../pop_hist.csv) regroupant des informations sur la population des communes de France Métropolitaine à travers l'histoire.

Ce fichier a été adapté à partir de [cette source](https://www.insee.fr/fr/statistiques/3698339). Les adaptations faites sont :

* suppression des données de population pour certaines années,

* suppression des communes pour lesquelles les données sont incomplètes (populations anciennes manquantes). Cette suppression explique que les communes listées soient toutes en métropole (hors Corse).

!!! warning "Fichier volumineux"

    Le fichier comporte 34 496 lignes !
    
    On déconseille vivement d'afficher la totalité du tableau avec Python...

Les premières lignes de ce fichier sont :

```title="📑 Données CSV"
departement;nom;pop_2019;pop_2009;pop_1999;pop_1990;pop_1982;pop_1968;pop_1954;pop_1936;pop_1926;pop_1906;pop_1896;pop_1886;pop_1876
Ain;L'Abergement-Clémenciat;779;787;728;579;477;347;424;506;543;629;572;622;604
```

Les descripteurs sont explicités ci-dessous :

|        Descripteur |    Indice | Type Python |                   Contenu |
| -----------------: | --------: | ----------: | ------------------------: |
| `#!py departement` |  `#!py 0` |  `#!py str` | département de la commune |
|         `#!py nom` |  `#!py 1` |  `#!py str` |         nom de la commune |
|    `#!py pop_2019` |  `#!py 2` |  `#!py int` |        population en 2019 |
|    `#!py pop_2009` |  `#!py 3` |  `#!py int` |        population en 2009 |
|    `#!py pop_1999` |  `#!py 4` |  `#!py int` |        population en 1999 |
|    `#!py pop_1990` |  `#!py 5` |  `#!py int` |        population en 1990 |
|    `#!py pop_1982` |  `#!py 6` |  `#!py int` |        population en 1982 |
|    `#!py pop_1968` |  `#!py 7` |  `#!py int` |        population en 1968 |
|    `#!py pop_1954` |  `#!py 8` |  `#!py int` |        population en 1954 |
|    `#!py pop_1936` |  `#!py 9` |  `#!py int` |        population en 1936 |
|    `#!py pop_1926` | `#!py 10` |  `#!py int` |        population en 1926 |
|    `#!py pop_1906` | `#!py 11` |  `#!py int` |        population en 1906 |
|    `#!py pop_1896` | `#!py 12` |  `#!py int` |        population en 1896 |
|    `#!py pop_1886` | `#!py 13` |  `#!py int` |        population en 1886 |
|    `#!py pop_1876` | `#!py 14` |  `#!py int` |        population en 1876 |

On propose ci-dessous deux versions du même exercice selon le type de listes choisis : liste de listes ou de dictionnaires.

??? question "Requêtes dans une liste de listes"

    Compléter le script ci-dessous afin d'effectuer les requêtes demandées. 
    
    Il faudra tout d'abord importer et typer les données ici sous la forme d'une liste de listes.
    
    La liste reprenant l'ensemble des informations sera `communes`.

    !!! warning "Il y a beaucoup de données : le script met un peu de temps à s'exécuter"
    
    {{ IDE('pythons/population_liste/exo', MAX=10) }}

??? question "Requêtes dans une liste de dictionnaires"

    Compléter le script ci-dessous afin d'effectuer les requêtes demandées. 
    
    Il faudra tout d'abord importer et typer les données ici sous la forme d'une liste de dictionnaires.
    
    La liste reprenant l'ensemble des informations sera `communes`.

    !!! warning "Il y a beaucoup de données : le script met un peu de temps à s'exécuter"
    
    {{ IDE('pythons/population_dico/exo', MAX=10) }}

## 2. Requêtes élaborées


### 2.1 🐻 Meilleurs films selon IMDB

À ce stade nous savons :

* ouvrir un fichier `csv` à l'aide de Python,
* lire son contenu dans une liste (de listes ou de dictionnaires),
* typer les données,
* effectuer des requêtes sur ces données à l'aide de listes en compréhension.

**A. Présentation des données et import**

Nous allons pousser notre étude un peu plus loin lors de l'étude du fichier `films.csv`[^1]. Ce [fichier](../films.csv) reprend les 517 films les mieux notés sur le site [IMDB](https://www.imdb.com/).

[^1]: [source](https://www.kaggle.com/datasets/faisaljanjua0555/top-500-movies-of-all-time)

Les descripteurs proposés sont :


|    Descripteur |   Indice |  Type Python |                         Contenu |
| -------------: | -------: | -----------: | ------------------------------: |
| `#!py Ranking` | `#!py 0` |   `#!py int` | rang du film dans le classement |
|    `#!py Name` | `#!py 1` |   `#!py str` |                     nom du film |
|    `#!py Year` | `#!py 2` |   `#!py int` |             année de production |
| `#!py Minutes` | `#!py 3` |   `#!py int` |              durée (en minutes) |
|  `#!py Genres` | `#!py 4` |   `#!py str` | genres associés (voir remarque) |
|  `#!py Rating` | `#!py 5` | `#!py float` |            note moyenne(sur 10) |
|   `#!py Votes` | `#!py 6` |   `#!py int` |                 nombre de votes |
|   `#!py Gross` | `#!py 7` | `#!py float` |         revenus générés (en M$) |

!!! note "Les genres"

    Chaque film est associé à au moins un genre, certains à trois genres...

    Dans le cas où plusieurs genres sont cités, ceux-ci sont séparés par des virgules.


Voici les premières lignes du fichier :

```title="📑 Données CSV"
Ranking;Name;Year;Minutes;Genres;Rating;Votes;Gross
1;Citizen Kane;1941;119;Drama, Mystery;8.3;442770;1.59
2;The Godfather;1972;175;Crime, Drama;9.2;1849989;134.97
3;The Wizard of Oz;1939;102;Adventure, Family, Fantasy;8.1;400883;2.08
```

On importe les données sous la forme d'une liste de dictionnaires. Le code est le suivant :

```python
import csv

films = []
with open("films.csv", "r", encoding="utf-8") as fichier:
    lecteur = csv.DictReader(fichier, delimiter=";")
    for entree in lecteur:
        entree["Ranking"] = int(entree["Ranking"])
        entree["Year"] = int(entree["Year"])
        entree["Minutes"] = int(entree["Minutes"])
        entree["Rating"] = float(entree["Rating"])
        entree["Votes"] = int(entree["Votes"])
        entree["Gross"] = float(entree["Gross"])
        films.append(entree)
```

!!! tip "Import réalisé !"

    Dans toute la suite du TP, la liste `films` telle qu'importée ci-dessus sera accessible dans chaque éditeur.

**B. Votes par genre**

Le fichier compte 25 films dont l'un des `#!py Genres` est `#!py Family` et 26 avec le genre `#!py Horror`. On précise qu'**aucun film ne possède ces deux genres**.

Parmi ces deux genres, quel est celui qui a reçu le plus de votes au total (en additionnant les votes reçus par chaque film) ?

??? question "Comparaison de genres"

    Compléter le script ci-dessous afin de compter : 
    
    * le nombre total de votes qu'ont reçus les films dont l'un des genres est `#!py Family` (utiliser la variable `#!py total_family`),
    
    * le nombre total de votes qu'ont reçus les films dont l'un des genres est `#!py Horror` (utiliser la variable `#!py total_horror`).

    {{ IDE('pythons/family_horror/exo') }}
    

**C. Projection(s)**

Comment faire pour n'obtenir que les noms des films cités dans le fichier ?

On peut procéder ainsi :

```pycon
>>> [entree["Name"] for entree in films]
['Citizen Kane', 'The Godfather', 'The Wizard of Oz', 'The Shawshank Redemption', 'Pulp Fiction', ...]
```

Afin de généraliser le procédé, on souhaite écrire une fonction `#!py projection` qui :

* prend en argument la liste de dictionnaires à manipuler ainsi que le nom d'un descripteur,

* renvoie la liste ne contenant que les valeurs associées à ce descripteur.

??? question "La fonction `projection`"

    Compléter le code ci-dessous en saisissant la fonction `projection` telle que décrite.

    Attention, les résultats doivent être **dans le même ordre que dans la liste initiale**.

    {{ IDE('pythons/film_projection/exo') }}

??? question "La fonction `projection_multiple`"

    On souhaite désormais récupérer les données associées à plusieurs descripteurs : la fonction `#!py projection_multiple` prend en argument la liste des données et un tuple contenant les descripteurs à conserver.

    Par exemple `projection_multiple(films, ("Name", "Year", "Rating"))` renverra la liste des tuples formés du nom, de l'année de production et de la note de de chaque film.

    ```pycon
    >>> projection_multiple(films, ("Name", "Year", "Rating"))
    [('Citizen Kane', 1941, 8.3), ('The Godfather', 1972, 9.2), ('The Wizard of Oz', 1939, 8.1), ...]
    ```

    ??? tip "Aide"

        Nous avons vu comment créer des listes en compréhension. Il est aussi possible de créer des tuples en compréhension :

        ```pycon
        >>> tuple(2 * k for k in range(3))
        (0, 2, 4)
        >>> [tuple(k * lettre for k in range(3)) for lettre in "ab"]
        [('', 'a', 'aa'), ('', 'b', 'bb')]
        ```

    Compléter le code ci-dessous en saisissant la fonction `projection_multiple` telle que décrite.

    {{ IDE('pythons/film_projection_multiple/exo') }}

**D. Sommes et moyennes**

Quel est le montant total rapporté par l'ensemble de ces films ? Et le montant moyen ? Pour le savoir il faut dans un premier temps additionner les revenus générés.

Écrivons une fonction pour cela.

??? question "La fonction `somme`"

    Compléter le code ci-dessous en saisissant la fonction `somme` qui :

    * prend en argument la liste de dictionnaires à manipuler ainsi que le nom d'un descripteur,

    * renvoie la somme de toutes les valeurs associées à ce descripteur.

    !!! note "Remarque"

        Vous observerez que l'on ne teste pas la stricte égalité des résultats car les valeurs manipulées sont des flottants et qu'il y a des erreurs d'arrondis.

    {{ IDE('pythons/film_somme/exo') }}

Il est désormais possible de calculer la moyenne des valeurs d'un descripteurs. Là encore, écrivons une fonction pour cela.

??? question "La fonction `moyenne`"

    Compléter le code ci-dessous en saisissant la fonction `moyenne` qui :

    * prend en argument la liste de dictionnaires à manipuler ainsi que le nom d'un descripteur,

    * renvoie la moyenne de toutes les valeurs associées à ce descripteur.

    !!! tip "Remarque"

        Une version de la fonction `somme` précédente est fournie. Vous pouvez l'utiliser dans votre code.

    {{ IDE('pythons/film_moyenne/exo') }}

La fonction `moyenne` est donc valide. Pourtant l'appel `moyenne(films, "Gross")` renvoie un résultat faux !

En effet, pour certains films le revenu total est inconnu. Néanmoins, afin de proposer un fichier `csv` valide, sans valeurs manquantes, la valeur `#!py -1.0` a été ajoutée dans ce cas. C'est le cas par exemple pour le 19ème film du classement (« *Les 400 coups* » de François Truffaut) :

```pycon
>>> projection_multiple(films, ("Name", "Gross"))[18]
('The 400 Blows', -1.0)
```

Pouvez-vous calculer la moyenne des revenus générés par les films pour lesquels les revenus sont connus ?

??? question "Revenu moyen des films"

    Calculer le revenu moyen des films proposés. Il ne faut pas tenir compte des films pour lesquels cette valeur est inconnue (`#!py -1.0` dans le fichier).

    Votre variable contenant la bonne valeur doit s'appeler `moyenne_valide` afin de pouvoir passer les tests avec succès.

    Les fonctions `somme` et `moyenne` sont déjà chargées.

    {{ IDE('pythons/film_revenus/exo') }}

**E. La meilleure année ?**

Quelle année a vu se produire le plus de films présents dans ce classement ? Nous allons compter le nombre de films produits chaque année.

Une approche naïve consisterait à effectuer de nombreuses requêtes, une par année : « *quels sont les films produits en 1900 ?* », « *quels sont les films produits en 1901 ?* », *etc*.

Cette approche présente plusieurs inconvénients :

* quelles sont les années à chercher ? On a fait l'hypothèse que les films ont été produits à partir de 1900 mais c'est vraisemblablement incorrect...

* Python doit lire l'ensemble des données à **chaque** requête ! Donc 100 lectures des 517 films si l'on teste tout le XX-ème siècle...

Une approche plus efficace consiste à utiliser un dictionnaire dont les clés sont les années et les valeurs le nombre de films produits cette année-ci. On peut procéder ainsi :

* initialement le dictionnaire est vide,
* on parcourt l'ensemble des films. Pour chacun :
  * si son année de production n'est pas présente dans le dictionnaire, on l'ajoute (en tant que clé) avec la valeur `#!py 1` (c'est la première fois que l'on rencontre cette valeur)
  * si l'année est déjà dans le dictionnaire on incrémente simplement la valeur associée.

??? question "Films par années"

    Compléter le script ci-dessous permettant de compléter le dictionnaire `annee_films` associant à chaque année présente dans la liste des films le nombre de films produits cette année-ci.
    
    {{ IDE('pythons/film_annee/exo') }}

Il reste à déterminer l'année durant laquelle le plus de films ont été produits.

??? question "La meilleure année"

    On a chargé en mémoire le dictionnaire `annee_films` associant à chaque année le nombre de films produits cette année-ci.

    Déterminer l'année (nommée `annee_maxi`) durant laquelle le plus de films ont été produits.

    !!! tip "Pas d'égalité !"

        Une année sort du lot, il n'y a pas d'*ex-aequo*.

    {{ IDE('pythons/film_annee_max/exo') }}


**F. Le meilleur genre ?**

Quel genre de film est cité le plus de fois ? Facile, il suffit de compter les apparitions de chaque genre comme l'on vient de compter les années.

**Mais** les genres sont mal présentés : chaque film est associé à un, deux ou trois genres, si besoin séparés par des virgules, avec des espaces derrière les virgules...

```pycon
>>> films[3]["Genres"]
'Drama'
>>> films[4]["Genres"]
'Crime, Drama'
>>> films[5]["Genres"]
'Drama, Romance, War'
```
Pouvez-vous déterminer le genre le plus présent ?

??? question "Le meilleur genre"

    Déterminer le genre le plus représenté dans les films.

    La variable contenant ce genre sera nommée `meilleur_genre`

    !!! tip "Astuce"

        On rappelle que la méthode `#!py str.split` découpe une chaîne de caractères à chaque apparition de l'argument et renvoie la liste des chaînes obtenues.

        Par exemple `#!py "31/5/2007".split("/")` renvoie `#!py ['31', '5', '2007']`.
    
    {{ IDE('pythons/film_genre/exo') }}