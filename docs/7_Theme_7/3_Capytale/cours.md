title:  7.3 Utilisation du service Capytale

![image](data/logo.svg){: .center}

Capytale est accessible via ENT, il faut donc avoir ses identifiants Educonnect.


## Activité-test : 
1. Connectez-vous à l'ENT  grâce à vos identifiants Educonnect.
2. Cliquez sur [https://camille-corot.ent.auvergnerhonealpes.fr/](https://camille-corot.ent.auvergnerhonealpes.fr/){. target="_blank"}
3. Dans le menu de gauche, choisir Ressources numériques puis Médiacentre.
4. Et enfin sélectionner Capytale.