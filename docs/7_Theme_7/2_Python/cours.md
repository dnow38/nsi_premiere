title:  7.2 Python en ligne


!!! info "Console"
    Vous pouvez accéder à une console Python en ligne

    ![image](data/snip_42.png){: .center width=50%}
        
    [Accéder à la console](https://lycee-corot-morestel.fr/console/){ .md-button target="_blank" rel="noopener" }

!!! info "Notebook"
    Vous pouvez accéder une interface Notebook en ligne

    ![image](data/snip_36.png){: .center width=50%}
        
    [Accéder à l'interface](https://lycee-corot-morestel.fr/notebook/){ .md-button target="_blank" rel="noopener" }