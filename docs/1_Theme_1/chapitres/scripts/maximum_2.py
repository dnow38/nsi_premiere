# --- PYODIDE:code --- #

def maxi_2(n1, n2):
    ...


# --- PYODIDE:corr --- #

def maxi_2(n1, n2):
    if n1 < n2 :
        return n2
    else :
        return n1


# --- PYODIDE:tests --- #

assert maxi_2(3, 5) == 5
assert maxi_2(7, 7) == 7
assert maxi_2(8, 5) == 8


# --- PYODIDE:secrets --- #

assert maxi_2(30, 5) == 30
assert maxi_2(3, 50) == 50