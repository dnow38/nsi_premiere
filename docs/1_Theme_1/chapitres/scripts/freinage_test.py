# Tests
assert reaction(36) == 10.0
assert freinage(50) == 12.5
assert arret(180) == 212.0

# Autres tests

assert reaction(72) == 20.0
assert freinage(100) == 50
assert arret(90) == 65.5
