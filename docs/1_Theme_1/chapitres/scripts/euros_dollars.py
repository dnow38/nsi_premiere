def euros_vers_dollars(montant):
    resultat_dollars = montant * 1.19 # en supposant qu'un euro vaut 1,19 dollars
    return resultat_dollars

montant_euros = float(input("Saisir le montant en euros : "))
# Nous allons affecter à la variable montant_dollars
# la valeur renvoyée par la fonction euro_vers_dollar
montant_dollars = euros_vers_dollars(montant_euros)
# Affichage du résultat :
print(montant_dollars)
