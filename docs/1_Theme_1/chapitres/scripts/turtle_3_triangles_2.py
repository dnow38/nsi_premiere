# --- PYODIDE:env --- #
from js import document
if "restart" in globals():
    restart()

# --- PYODIDE:code --- #
from turtle import *
def triangle(n): # n est le nombre de pixels
    for i in range(3):
        forward(n)
        left(120)

n = int(input("nombre de pixels : "))
...
# --- PYODIDE:post --- #
done()
document.getElementById("cible_4").innerHTML = svg()