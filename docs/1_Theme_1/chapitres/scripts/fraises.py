# avant de commencer, Bob a mangé 0 fraises
nb_total = 0

# on commence les cueillettes :
for cueillette in range(1, 11) :
    # on ajoute ce nombre fraisesdans l'accumulateur :
    nb_total = nb_total + cueillette
    print("Bob mange ", cueillette," fraises. En tout cela fait maintenant", nb_total," fraises") # affichage à chaque tour de boucle

# on affiche le résultat final en sortie de boucle
print("En tout Bob a mangé ", nb_total, "fraises.")
