# --- PYODIDE:code --- #
VOYELLES = 'aeiouyAEIOUY'

def nb_voyelles(mot):
    ...


# --- PYODIDE:corr --- #

VOYELLES = 'aeiouyAEIOUY'

def nb_voyelles(mot):
    nb = 0
    for lettre in mot:
        if lettre in VOYELLES:
            nb = nb + 1
    return nb


# --- PYODIDE:tests --- #

assert nb_voyelles("Bonjour") == 3
assert nb_voyelles("") == 0


# --- PYODIDE:secrets --- #

assert nb_voyelles("alors") == 2
assert nb_voyelles("ada") == 2
assert nb_voyelles("PYTHON") == 2
assert nb_voyelles("e") == 1
assert nb_voyelles("z") == 0

