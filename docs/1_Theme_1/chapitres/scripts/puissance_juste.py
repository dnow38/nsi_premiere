def fonction_puissance_juste(x, n):
    """
    renvoie x à la puissance n
    """
    resultat = 1 # C'est ce qu'il fallait modifier
    for i in range(n):
        resultat = resultat * x
    return resultat


assert fonction_puissance_juste(2, 3) == 8, "le résultat de 2^3 est 1"
assert fonction_puissance_juste(2, 0) == 1, "le résultat de 2^0 est 1"
assert fonction_puissance_juste(3, 0) == 1, "le résultat de 3^0 est 1"
