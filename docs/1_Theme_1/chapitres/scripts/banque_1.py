# initialisation 
somme_totale = ...

# itération sur les 12 mois de l'année :
for i in range(...):
    # calculez la somme à ajouter
    somme_a_ajouter = ...
    print("Mois numéro", i + 1, " Bob épargne ", somme_a_ajouter)
    
    # on ajoute la somme dans l'accumulateur :
    somme_totale = ...
    
# affichage du résultat :
print("Après 12 mois, Bob dispose de ", somme_totale," €")
