---
author: Mireille Coilhac
title: Modules Python
---

## I. Les importations

Il existe de nombreux modules pour Python : random, math, cmath, numpy, matplotlib, turtle, sympy, tkinter, PIL, pygame, pyknon, statistics… 

Il y a deux possibilités pour importer :

???+ question "Possibilité 1"

    Tester:

    {{IDE('scripts/import_math')}}


!!! info "Syntaxe `import module`"

    💗 Si on utilise la syntaxe `import module`, pour utiliser  `fonction` de `module`, il faut écrire `module.fonction`


???+ question "Possibilité 2"

    Tester:

    {{IDE('scripts/from_math')}}


???+ note dépliée

    Pour économiser, nous aurions pu simplement appeler la fonction `sqrt`

!!! info "Syntaxe `from module import fonction`"

    💗 Si on utilise la syntaxe `from module import fonction`, pour utiliser  `fonction` de `module`, il faut simplement écrire `fonction`

## II. Contenu d'un module et aides

???+ question "La fonction `dir`"

	* Recopier dans l'éditeur `import math`, puis exécuter.
	* Recopier dans la console `dir(math)`, puis appuyer sur la touche  <kbd>⏎</kbd>

	{{ IDE() }}

???+ question "Autre exemple"	

	* Recopier dans l'éditeur `import statistics`, puis exécuter.
	* Recopier dans la console `dir(statistics)`, puis appuyer sur la touche  <kbd>⏎</kbd>

	{{ IDE() }}

???+ question "La fonction `help`"	

	Nous voulons savoir à quoi sert la fonction `mean`.  
	Attention nous avons fait l'import avec l'instruction `import statistics`.  

	Recopier dans la console `help(statistics.mean)`, puis appuyer sur la touche  <kbd>⏎</kbd>

	{{ IDE() }}

???+ question "Autre possibilité pour la fonction `help`"	

	* Recopier dans l'éditeur `from statistics import mean`, puis exécuter.
	* Recopier dans la console `help(mean)`, puis appuyer sur la touche  <kbd>⏎</kbd>

	{{ IDE() }}
	
???+ note dépliée "`help`"

	![help](images/help.jpeg){ width=50%; : .center }

	😊 A vous ...

	{{ terminal() }}




## III.  Exemple d'utilisation de trois modules : `statistics`, `random`, `numpy`

???+ question "Les modules `statistics`, `random`, `numpy`"	

	Tester :

    {{IDE('scripts/stats')}}

???+ question "Question"

	Si vous exécutez plusieurs fois le code ci-dessous, obtenez-vous toujours les même résultats? Pourquoi? 

	??? success "Solution"

		La liste est constituée de façon aléatoire, on peut donc obtenir des résultats différents.

## IV. Exemple d'utilisation de la bibliothèque `math`

???+ question "La biliothèque `math`"

	La bibliothèque math est utile pour faire des calculs .   
	Ecrire le code pour trouver les différentes fonctions de cette bibliothèque

	{{ IDE() }}

??? success "Solution"

	* Recopier dans l'éditeur `import math`, puis exécuter.
	* Recopier dans la console `dir(math)`, puis appuyer sur la touche  <kbd>⏎</kbd>

???+ question "La fonction `pow`"

	Ecrire le code qui permet de savoir à quoi sert la fonction `pow`

	{{ IDE() }}

??? success "Solution"

	* Recopier dans l'éditeur `import math`, puis exécuter.
	* Recopier dans la console `help(math.pow)`, puis appuyer sur la touche  <kbd>⏎</kbd>

	La fonction `pow` permet d'élever un nombre à une certaine puissance. Tester : 

	{{ IDE() }}

## V. Le module numpy

???+ question "Le module `numpy`"

	Tester :

    {{IDE('scripts/numpy')}}


## VI. Le module `random`

???+ question "Le module `random`"

	Trouver toutes les fonctions de cette bibliothèque.    
	Chercher l’aide fournie sur : `choice`, `sample`, `shuffle.`   
	Testez ces fonctions.

	{{ IDE() }}

??? success "Solution"

	* Recopier dans l'éditeur `import random`, puis exécuter.
	* Recopier dans la console `dir(random)`, puis appuyer sur la touche  <kbd>⏎</kbd>

	* Recopier dans la console `help(random.choice)`, puis appuyer sur la touche  <kbd>⏎</kbd>.  
	Faire de même pour `sample` et `shuffle.` 

## VII. Des modules graphiques


Après avoir téléchargé le fichier, vous pourrez le lire à partir de [Basthon](https://notebook.basthon.fr/){ .md-button target="_blank" rel="noopener" }

Nous allons voir quelques petits exemples dans le TD suivant :

🌐 TD à télécharger : Fichier `modules_graphiques.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/modules_graphiques.ipynb)






