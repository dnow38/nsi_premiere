def costume(rang, couleurs):
    ...

# Tests
assert costume(2, ["bleu", "blanc", "rouge"]) == "blanc"
assert costume(8, ["rose", "vert", "orange", "bleu"]) == "bleu"


