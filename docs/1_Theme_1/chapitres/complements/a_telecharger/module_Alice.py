# Module d'Alice

def fonction_1(x):
    """Renvoie le carre de x"""
    return x**2

def fonction_2(n, k):
    """Renvoie la liste des k premiers multiples de n
    Par exemple fonction_2(5, 4) renvoie [5, 10, 15, 20]
    """

    liste_multiples = k * [0]
    for i in range(k):
        liste_multiples[i] = (i + 1)*n
    return liste_multiples
