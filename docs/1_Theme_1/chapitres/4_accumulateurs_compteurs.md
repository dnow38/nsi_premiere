---
author: Mireille Coilhac
title: Chapitre 1.4 - Accumulateurs et compteurs
---

!!! abstract "Résumé"

    Vous avez appris à : 
    
    * faire des boucles pour répéter des instructions
    * utiliser le compteur de la boucle 

    👉 Nous allons voir dans ce cours deux notions fondamentales :

    * compter (avec un compteur) quelques chose, dans une boucle.
    * additionner (avec un accumulateur) des éléments successivement dans une boucle.

## I. Les accumulateurs

???+ question "Bob découvre un beau fraisier"

    Bob découvre un beau fraisier....

    * Il cueille une fraise et la mange. 
    * Y prenant goût, il y retourne et cette fois prend 2 fraises. 
    * N'y tenant plus, il y retourne et prend cette fois 3 fraises. 

    Et ainsi de suite, à chaque fois, il ne peut pas s'en empêcher, il y retourne. Et à chaque fois il prend une fraise de plus que la fois précédente !

    Combien de fraise Bob a-t-il mangé au bout de 10 cueillettes ?

    Pour les compter, nous allons ajouter une variable. Cette technique est d'usage très fréquent, et la variable que nous allons ajouter 
    est nommée un **accumulateur**, c'est une variable qui sert à ajouter, à chaque tour de la boucle, une certaine quantité.

    Tester ci-dessous : 

    {{IDE('scripts/fraises')}}


!!! info "Accumulateur"

    Il y a 3 éléments pour bien utiliser notre variable accumlateur :
    
    * **I**nitialisation : Avant la boucle, on initialise la variable
    * **I**térations : on défini soigneusement les itérations
    * **I**nstructions : Dans la boucle, on modifie la valeur de l'accumulateur
    
    Pour vous aider à mémoriser cela, nous déciderons de nommer ces 3 étapes sous le terme de **la règle des 3I** 

???+ question "La moyenne"

    Le professeur de NSI veut créer un outil pour calculer les moyennes des élèves.

    Compléter le code ci-dessous, qui demande de saisir chaque note (il y a 4 notes sur la période), calcule la somme des notes, puis affiche la moyenne, c'est à dire la somme des notes divisée par 4.

    {{IDE('scripts/saisie_moyenne')}}

    ??? success "Solution"

        ```python title=''
        somme = 0

        # saisie des notes et calcul de la somme au fur et à mesure
        for i in range(4):
            note = int(input("Entrez la note numéro " + str(i + 1) + " :"))
            somme = somme + note
        moyenne = somme / 4                            

        # affichage de la moyenne:
        print("La moyenne de l'élève est : ", moyenne)
        ```

## II. Les compteurs

???+ question "Le prof fou"

    Le professeur décide, arbitrairement, que seules les notes paires comptent dans la moyenne.   

    **1.** Il commence par écrire le code : (vous l'exécuterez en entrant les notes 12 12 12 et 13). Qu'en pensez-vous ?

    {{IDE('scripts/prof_fou_1')}}    

    ??? success "Solution"

        La moyenne est fausse.

        Dans le calcul final de la moyenne, on divise par le nombre de notes. Mais comme on n'a compté que les notes paires, il faudrait diviser par **le nombres de notes paires**    
        
        Mais pour cela, bien entendu, il va falloir .... **compter** le nombre de notes paires. Et dès qu'on parle de **compter** on pense à un **compteur**

    **2.** Il faut donc introduire une variable, nommée par exemple `compteur`, qui va nous servir à compter combien il y a de notes paires.  
    A vous de jouer !

    {{IDE('scripts/prof_fou_2')}}   

    ??? success "Solution"

        ```python title=''
        nb_de_notes = 4

        somme = 0  # Initiaisation de l'accumulateur
        compteur = 0  # initialisation du compteur
            
        for i in range(nb_de_notes):
            note = int(input("Entrez la note numéro " + str(i + 1) + ":"))
                
            # on ajoute cette note dans le total seulement si elle est paire :
            if note % 2 == 0 :
                somme = somme + note
                compteur = compteur + 1  # on compte, c'est-à-dire on ajoute 1

        # calcul de la moyenne :
        moyenne = somme/compteur

        print("La moyenne est : ", moyenne)
        ```

!!! info "Compteur"

    Les compteurs sont des variables qui servent à compter quelque chose.
        
    Comme les **accumulateurs**, les compteurs sont utilisés en respectant **la règle des 3I** :   
    
    * **I**nitialisation : Avant la boucle, on initialise la variable `compteur` à sa valeur initiale
    * **I**térations : on définit soigneusement les itérations
    * **I**ncrémentation : quand c'est nécessaire, on incrémente le compteur (on lui ajoute 1)


## III. Exercices

???+ question "Exercice 1 : for i in range(...) avec un seul paramètre"

    Bob a 243 € sur son compte en banque.

    Il décide d'épargner durant une année. 

    * En janvier il dépose 10 € sur son compte
    * En février il dépose 20 € sur son compte
    * En mars il dépose 30 € sur son compte

    Et ainsi de suite, chaque mois il dépose la même somme que le mois précédent, augmentée de 10€.

    ??? tip "Astuce : les trois I"

        * Initialisation : Quelle est la somme de départ ?
        * Itération : combien de fois je dois itérer ?
        * Instructions : Dans la boucle, combien ajouter à notre variable accumulateur ?

    Compléter ci-dessous. Il doit s'afficher $1023$ €

    {{ IDE('scripts/banque_1') }}

    ??? success "Solution"

        ```python title=''
        # initialisation 
        somme_totale = 243

        # itération sur les 12 mois de l'année :
        for i in range(12) :
            # calculez la somme à ajouter
            somme_a_ajouter = (i + 1)*10
            print("Mois numéro", i + 1, " Bob épargne ", somme_a_ajouter)
            
            # on ajoute la somme dans l'accumulateur :
            somme_totale = somme_totale + somme_a_ajouter

            
        # affichage du résultat :
        print("Après 12 mois, Bob dispose de ", somme_totale, " €")   
        ```        

???+ question "Exercice 2 : for i in range(..., ...) avec deux paramètres"

    ??? note "Même situation que celle de l'exercice 1"

        Bob a 243 € sur son compte en banque.

        Il décide d'épargner durant une année. 

        * En janvier il dépose 10 € sur son compte
        * En février il dépose 20 € sur son compte
        * En mars il dépose 30 € sur son compte

        Et ainsi de suite, chaque mois il dépose la même somme que le mois précédent, augmentée de 10€.

    ??? tip "Astuce : les trois I"

        * Initialisation : Quelle est la somme de départ ?
        * Itération : il faut itérer en commençant à ... et s'arrêter à ... (donc avant ...) 
        * Instructions : Dans la boucle, combien ajouter à notre variable accumulateur ?

    Compléter ci-dessous, en utilisant `for i in range(..., ...)` avec deux paramètres. Il doit s'afficher $1023$ €

    {{ IDE('scripts/banque_2') }}

    ??? success "Solution"

        ```python title=''
        # initialisation 
        somme_totale = 243

        # itération sur les 12 mois de l'année : du mois 1 au mois 12 inclus, donc 13 exclus.
        for i in range(1, 13) :
            # calculez la somme à ajouter
            somme_a_ajouter = i * 10
            print("Mois numéro", i, " Bob épargne ", somme_a_ajouter)
            
            # on ajoute la somme dans l'accumulateur :
            somme_totale = somme_totale + somme_a_ajouter

            
        # affichage du résultat :
        print("Après 12 mois, Bob dispose de ", somme_totale, " €")   
        ```   

???+ question "Exercice 3 : for i in range(..., ..., ...) avec trois paramètres"

    ??? note "Même situation que celle de l'exercice 1"

        Bob a 243 € sur son compte en banque.

        Il décide d'épargner durant une année. 

        * En janvier il dépose 10 € sur son compte
        * En février il dépose 20 € sur son compte
        * En mars il dépose 30 € sur son compte

        Et ainsi de suite, chaque mois il dépose la même somme que le mois précédent, augmentée de 10€.

    ??? tip "Astuce : les trois I"

        * Initialisation : Quelle est la somme de départ ?
        * Itération : il faut itérer en commençant à ... et s'arrêter à ... (donc avant ...) avec un incrément de ...
        * Instructions : Dans la boucle, combien ajouter à notre variable accumulateur ?
        
        ??? tip "Aide : quelle est la dernière somme à ajouter ?"

            * Au départ, il y a la somme initiale, et $0$ à ajouter
            * Au bout de 1 mois, il faut ajouter $1 \times 10=20$
            * Au bout de 2 mois, il faut ajouter $2 \times 10=20$
            * Ainsi de suite
            * Au bout de 12 mois, il faut ajouter $12 \times 10=120$

    Compléter ci-dessous, en utilisant `for i in range(..., ..., ...)` avec trois paramètres. Il doit s'afficher $1023$ €

    {{ IDE('scripts/banque_3') }}

    ??? success "Solution"

        ```python title=''
        # initialisation 
        somme_totale = 243
        mois = 0

        # itération sur les 12 mois de l'année :
        for somme in range(10, 121, 10):  # somme prend successivement les valeurs 10, 20, 30, ..., 120 (donc 121 exclu)
            mois = mois + 1
            # calculez la somme à ajouter
            somme_a_ajouter = somme
            print("Mois numéro", mois, " Bob épargne ", somme_a_ajouter)
            
            # on ajoute la somme dans l'accumulateur :
            somme_totale = somme_totale + somme_a_ajouter
            
        # affichage du résultat :
        print("Après 12 mois, Bob dispose de ", somme_totale," €")
        ```  

???+ question "Exercice 4 - un classique de maths - la somme des entiers de 1 à n compris"

    Compléter le code ci-dessous pour qu'il s'affiche le résultat de $1+2+...+n$

    {{ IDE('scripts/somme_n') }}

    ??? success "Solution"

        ```python title=''
        n = int(input("Saisir un entier n"))

        # Calcul de 1 + 2 + ... + n
        total = 0
        for i in range(n + 1):
            total = total + i

        print("total = ", total)
        ```

???+ question "Exercice 5 - un classique de maths - le produit des entiers de 1 à n compris"

    Compléter le code ci-dessous pour qu'il s'affiche le résultat de $1 \times 2 \times ... \times n$

    {{ IDE('scripts/factorielle_n') }}

    ??? success "Solution"

        ```python title=''
        n = int(input("Saisir un entier n"))

        # Calcul de 1 * 2 * ... * n
        total = 1
        for i in range(1, n + 1):
            total = total * i

        print("total = ", total)
        ```

    !!! info "Factorielle n"

        Le nombre $1 \times 2 \times ... \times n$ est égal à  $n \times (n-1) \times ... \times 1$  
        Ce nombre se note $n!$   
        On le lit "factorielle n"

    





