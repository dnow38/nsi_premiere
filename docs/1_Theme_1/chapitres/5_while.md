---
author: Jean-Louis Thirot et Mireille Coilhac
title: Chapitre 1.5 - Boucle While (Tant que)
---

## I. La syntaxe par l'exemple

???+ question "Syntaxe du while"

    !!! warning inline end "Important"

        Prenez le temps de lire les commentaires !
    
        Cliquez sur les +
    

	```python
	nombre = 3
	while nombre < 20:  # (1)
    	nombre = 2*nombre
    	print(nombre)  
    ```

    1. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

    2. :warning: i prend les valeurs entières de l'intervalle [0; 4]
    
    Tester :
    
    {{IDE('scripts/exemple_1')}}

???+ question "Avec un peu d'aléatoire"

    !!! warning inline end "Important"

        Prenez le temps de lire les commentaires !
    
        Cliquez sur les +
    

	```python
	# Jeu de dé
	from random import randint  # (1)
	lancer_de = randint(1, 6)  # (2)

	while lancer_de != 6:
    	print(lancer_de, "perdu!")
    	lancer_de = randint(1, 6)
	print(lancer_de, "gagné!")
    ```

    1. Cette ligne est nécessaire pour pouvoir utiliser `randint(1, 6)` qui renvoie un entier aléatoire entre 1 et 6 compris.

    2. On simule un lancer de dé.
   
    Tester plusieurs fois de suite :
    
    {{IDE('scripts/jeu_de_6')}}


## II. Découverte de la boucle while

!!! info "Syntaxe générale"

	```python title=''
	while condition:
	    instructions  # (1)
	```

	1. bloc d’instructions qui sera exécuté **tant que** `condition` est `True`.

???+ question "le mot de passe"

    Le code ci-dessous demande d'entrer le mot de passe.

	Tant que vous n'entrez pas le bon, il vous repose la question.

	Le mot de passe ici est : 123456.

    Compléter le script ci-dessous :

    {{ IDE('scripts/mdp') }}


	??? success "Solution"

		On exécute la boucle **tant que** `mot_de_passe != secret`

		C'est à dire tant que **`mot_de_passe != secret`** s'évalue à **`True`**

		```python title=''
		# Initialisation du mot de passe secret :
		secret = 123456

		# On demande une première fois le mot de passe qui est de type int :
		mot_de_passe = int(input("Tapez le mot de passe :" ))
		# Tant que ce n'est pas le bon, on redemande le mot de passe :
		while mot_de_passe != secret:
			mot_de_passe = int(input("Tapez le mot de passe :" ))
		print("Vous pouvez entrer !") # Si on est sorti de la boucle, c'est que le mot de passe a été trouvé.
		``` 

???+ question "Saisie valide"

    Ecrire un code qui demande de saisir un float entre 1 et 100.

	* **Tant que** le nombre saisi n'est pas entre 1 et 100, une nouvelle saisie est demandée.
	* Si la saisie est correcte, il doit s'afficher : "On peut continuer !"

    {{ IDE() }}

	??? success "Solution 1"

		```python title=''
		# On demande une première fois :
		saisie = int(input("Saisir un nombre entre 1 et 100 : "))

		# Tant que ce n'est pas bon, on redemande :
		while saisie < 1 or saisie > 100:
			saisie = int(input("Saisir un nombre entre 1 et 100 : "))
		print("On peut continuer !")
		```

	??? success "Solution 2"

		Utilisation de l'opérateur not : 

		```python title=''
		# On demande une première fois :
		saisie = int(input("Saisir un nombre entre 1 et 100 : "))

		# Tant que ce n'est pas bon, on redemande :
		while not(saisie >= 1 and saisie <= 100):
			saisie = int(input("Saisir un nombre entre 1 et 100 : "))
		print("On peut continuer !")
		```

!!! info "Exécution de la boucle while"

    Dans les deux cas ci-dessus, on a initialisé la variable (celle qui est utilisée pour la condition) avant la boucle. Il se peut que la condition soit fausse dès la première fois (on a entré le bon mot de passe du premier coup, on a bien entré un entier entre 1 et 100 du premier coup...) et dans ce cas, la boucle while n'est pas exécutée, pas même une première fois. 

???+ question "⚠️ Danger"

	🌵 Il est fréquent de faire une boucle `while` et d'oublier de vérifier si celle-ci se termine. 

	On ecrit  tant que ... et la condition d'arrêt ne se réalise jamais.

	😢 Dans un tel cas, votre programme ne s'arrête donc pas. 

	⌛ Lorsque cela se produit, vous devez fermer le site.

	😴 Essayez !

	Le code ci-dessous est un exemple de boucle qui ne se finit jamais. Cela va donc provoquer un gel du navigateur, mais comme ça, vous saurez quoi faire quand cela vous arrivera pour de vrai (et croyez moi, cela vous arrivera !)

	* exécuter le code
	* Constater que le navigateur est "planté"puis quitter le site pour y revenir.

	Pourquoi cette boucle est-elle infinie ?

    {{ IDE('scripts/infini') }}

	

	??? success "Solution"

		* `n = 0` donc `n < 10` est évalué à `True`.  
		On rentre donc dans la boucle `while`, et il s'affiche `0`  
		* `n` n'a pas été modifié, donc de nouveau `n < 10` est évalué à `True`.  
		On rentre donc dans la boucle `while`, et il s'affiche `0`. 
		* `n` n'a pas été modifié, donc de nouveau `n < 10` est évalué à `True`.  
		On rentre donc dans la boucle `while`, et il s'affiche `0`.  
		* etc.

## III. Les drapeaux (flags en anglais)

???+ question "Terminer la partie"

	Un drapeau (flag en anglais) est une variable qui sert à marquer une situation. Par exemple, dans le code ci-dessous, on utilise un drapeau nommé **`continuer`** qui vaut **`True`** tant que la partie n'est pas terminé, et **`False`** quand elle est terminée.

	Il y a 2 façons de terminer la partie :

	* quand on a gagné
	* si on abandonne

	Dans ces deux cas, on affectera la valeur **`False`** à la variable **`continuer`** pour sortir de la boucle `while`.

	La variable **`continuer`** est un drapeau (ou flag)

	Tester ci-dessous

    {{ IDE('scripts/flag') }}


## IV. Exercices

???+ question "Exercice 1"

	Une grande partie du travail de l'administration de l'université, en plus de gérer les enseignants, les étudiants, les cours… est de veiller au bon fonctionnement de l'université et en particulier à ce que les comptes soient bien tenus. En particulier il faut, une fois par an, faire un bilan annuel des dépenses.

	Toutes les dépenses de l'année ont été enregistrées et classées dans une multitude de dossiers et il faut maintenant calculer la somme de toutes ces dépenses. Mais personne ne sait exactement combien de dépenses différentes ont été effectuées durant l'année écoulée !

	Votre programme devra lire une suite d'entiers positifs et afficher leur somme. On ne sait pas combien il y aura d'entiers, donc le code va demander d'entrer des valeur jusqu'à ce que l'on entre la valeur -1 (qui n'est pas une dépense, juste un marqueur de fin). Il doit ensuite afficher la dépense totale.

	Voici un exemple d'exécution : 

	```pycon
	Entrez la somme dépensée : 200
	Entrez la somme dépensée : 100
	Entrez la somme dépensée : 40
	Entrez la somme dépensée : -1
	dépense totale : 340
	```

	À vous : 

    {{ IDE() }}

	??? success "Solution"

		```python
		# Nous utiliserons un accumulateur
		somme = 0

		# Initialisation la condition du while  :
		# La condition d'arrêt est : on a entré une dépense égale à -1
		# Il faut donc initialiser la variable dépense à autre chose que -1

		depense = 0

		# Commençons à saisir les dépenses :
		while depense != -1 :
			depense = int(input("Entrez la somme dépensée : "))
			# On ajoute la somme saisie
			if depense != -1 :
				somme = somme + depense

		print("dépense totale :", somme)
		```

???+ question "Exercice 2 : intérêts bancaires"

	Alice a déposé ses économies à sa banque sur un compte rémunéré.  

	Écrire un programme qui :

	* demande de saisir le capital initial déposé
	* demande le taux `taux` en %
	* puis calcule et affiche le nombre d'années au bout duquel son capital aura au moins doublé. 

	Indication  :  chaque année le capital est multiplié par $(1+\dfrac{t}{100})$

    {{ IDE('scripts/doublement') }}

	??? success "Solution"

		```python
		capital_initial = int(input("Capital initial : "))
		taux = int(input("taux en %. Par exemple saisir 2 pour un taux de 2% : "))
		nb_annees = 0 # Pour compter les années
		capital = capital_initial

		while capital < 2 * capital_initial:
			capital = capital*(1 + taux/100)
			nb_annees = nb_annees + 1
			print(nb_annees, "-", capital)
			
		print("au bout de ", nb_annees, "années le capital vaut : ", capital)
		```


