---
author: Jean-Louis Thirot et Mireille Coilhac
title: Chapitre 1.1 - Variables
---


## I. Les affectations

!!! example "Exemple d'affectation"

    Dans la console saisir :

    ```pycon
    >>> a = 1
    ```

    {{ terminal() }}

!!! info "Affectation"

	Vous venez de faire deux choses en même temps :

	* Vous avez défini une variable que vous avez nommé `a`
	* Vous avez affecté la valeur 1 à `a`

!!! example "Exemple d'affichage"

    Dans la console saisir :

    ```pycon
    >>> a = 1
    >>> print(a + 1)
    ```
    {{ terminal() }}

    Python affiche la valeur 2 mais la variable `a`n'a pas été modifiée. Vérifiez le en ajoutant dans la console précédente :

    ```pycon
    >>> a
    ```
    Après exécution de cette ligne, on constate que `a` n'a pas été modifié.


???+ question "Ajoutons 1 à notre variable"

    Dans la console saisir :

    ```pycon
    >>> a = 1
    >>> a = a + 1
    ```

    {{ terminal() }}

    Il ne s'affiche rien, c'est normal, nous avons juste fait une affectation.

    Ajouter dans la console précédente :

    ```pycon
    >>> a 
    ```

    La variable `a` a bien été modifiée.

!!! example "Des chaînes de caractères"

    Dans la console saisir :

    ```pycon
    >>> a = "Hello "
	>>> b = "World"
	>>> print(a + b)
    ```

    {{ terminal() }}

!!! info "Concaténation"

	Il s'affiche "Hello World". 

	👉 Vous avez créé 2 variables, `a` et `b`

	👉 Vous avez en même temps affecté des valeurs à ces deux variables, puis vous avez affiché `a + b`qui n'est pas une addition, car `a` et `b`ne sont pas des nombres.

	🐘 On dit qu'on a réalisé une **concaténation**.


## II. Dérouler un code

Plus haut, nous avons exposé comment affecter une valeur une variable, et abordé la notion du type. Un code comprend généralement plusieurs affectations, de plusieurs variables, et effectue des opérations avec ces variables. Il est exécuté ligne par ligne, mais pas forcément dans l'ordre ou elles sont écrites.

Toutefois, pour le moment, nous n'utiliserons pas encore de boucle ou d'instructions conditionnelles, que nous verrons ensuite. Les lignes sont donc interprétées et exécutées dans l'ordre ou elles sont écrites.

Pour lire un code, ou pour mettre au point un code, il est déterminant de savoir le dérouler à la main, sur une feuille.

Il n'existe pas de format standard pour écrire le déroulé d'un code, mais nous allons adopter une de ces deux présentations que nous utiliserons toute l'année. ci-dessous un petit exemple :

###✍️ A noter : Méthode 1

![nom image](images/deroule.png){ width=50% }

A gauche nous indiquons le numéro de ligne, à droite les affectations.

Le numéro de ligne est peu utile dans cet exemple très simple, puisqu'elles sont exécutées dans l'ordre ou elles sont écrites, mais ne perdez pas de vue que ce n'est pas toujours le cas.

###✍️ A noter : Méthode 2

![nom image](images/modele_tab_trace.jpg){ width=50% }

Le tableau indique l'état de toutes les variables après exécution de chaque ligne.

## III. Exercices

Pour résoudre ces exercices, il est demandé de dérouler le code sur papier.

???+ question "Exercice 1"

	```python linenums='1'
	x = 5
	x = x + x
	x = x - 5
	```
	Que contient la variable `x` à la fin du code ?


    ??? success "Solution"

        Déroulé du code ligne par ligne
		
		L1 : x = 5  
        L2 : x = 10  
        L3 : x = 5  

        A la fin on a `x = 5`

???+ question "Exercice 2"

	```python linenums='1'
	nombre = 3
	print(nombre + 1)
	print(nombre)
	nombre = nombre + 4
	print(nombre)
	```
	Que s'affiche-t-il ?


    ??? success "Solution"

 		Il s'affiche : 

 		4  
 		3  
 		7

???+ question "Exercice 3"

	```python linenums='1'

	premier = 4
	deuxieme = 6
	troisieme = premier * deuxieme
	deuxieme = troisieme - premier
	premier = premier + deuxieme + troisieme
	troisieme = deuxieme * premier
	```

	Donner la valeur finale de toutes les variables à la fin du programme

    ??? success "Solution"

    	* Première présentation possible :

    	L1 : premier = 4  
    	L2 : deuxieme = 6  
    	L3 : troisieme = 24  
    	L4 : deuxieme = 20  
		L5 : premier = 48  
    	L6 : troisieme = 960  

    	A la fin on a :  

    	premier = 48  
    	deuxieme = 20  
    	troisieme = 960  

    	* Présentation à privilégier :

    	|ligne|premier|deuxieme|troisieme|
		| :---    | :----  | :---   |:---   |
		| L1  | 4 |  | |
		| L2 | 4 | 6 | |
		| L3 | 4 | 6 |24 |
		| L4 | 4 | 20 |24 |
		| L5 | 48 | 20 |24 |
		| L6 | 48 | 20 |960 |

???+ question "Exercice 4 - sur papier : l'algorithme d'échange"

	[Algorithme d'échange : sujet](a_telecharger/algorithme_echange.pdf){ .md-button target="_blank" rel="noopener" }

	??? success "Solution"

		[Algorithme d'échange : correction](a_telecharger/cor_algorithme_echange.pdf){ .md-button target="_blank" rel="noopener" }

	
<!---  ⏳ La correction viendra bientôt ... -->

<!--- La correction à télécharger plus tard A SORTIR DE L'ADMONITION
🌐 Fichier `cor_algorithme_echange.pdf` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/a_telecharger/cor_algorithme_echange.pdf)
-->


## IV. A retenir

!!! abstract "Variable"

    Une **variable** est une zone de la mémoire de l'ordinateur dans laquelle une valeur est stockée. Aux yeux du programmeur, cette variable est définie par un nom, alors que pour l'ordinateur, il s'agit en fait d'une adresse, c'est-à-dire d'une zone particulière de la mémoire.


	😊 En Python on ne "déclare" pas les variables. Une variable est crée en lui affectant une valeur.


!!! abstract "print"

	La fonction `print()` permet d'afficher dans la console.

## V. Découverte des types

!!! info "Les types"

	C'est une notion fondamentale en Python (comme dans tout les langages).

	Dans les exemples qui précèdent, nous avons utilisé des nombres entiers `a = 1` puis des chaînes de caractères `a = "Hello"`

	Ce ne sont pas les mêmes objets.

!!! info "Les objets"

	Qu'est-ce qu'un objet en python ?

	Python est un langage orienté objet. Une variable est un mot (ou une simple lettre) qui réfère à une valeur. par exemple, si on entre a = 1, la variable `a` réfère à la valeur 1.

	La valeur est ici un entier et le type de la variable `a` est un entier. 

!!! info "Connaître un type"

	On peut connaitre le type d'une variable avec la fonction `type()`. Essayez en saisissant ceci dans la console :

	```pycon
	>>> a = 1
	>>> type(a)
	```

	{{ terminal() }}

	Vous obtenez : `<class 'int'>`

	La fonction `type` nous permet de savoir que le type de la variable est `int` ce qui est l'abréviation de integer qui signifie entier en anglais.

???+ question "Exercice 1"

	Créer une variable nommée `mot` à laquelle vous affecterez la valeur "Bonjour" et faites afficher, dans la console, le type de cette variable.

	{{ terminal() }}

    ??? success "Solution"

    	```pycon
		>>> mot = "Bonjour"
		>>> type(mot)
		```

???+ question "Exercice 2"

	Créer une variable nommée `nombre` à laquelle vous affecterez la valeur `5.5` et faites afficher, dans la console, le type de cette variable.

	{{ terminal() }}

    ??? success "Solution"

    	```pycon
		>>> nombre = 5.5
		>>> type(nombre)
		```

		Le type `float` est traduit en français par "flottant".


!!! info "Les types d'objets et les méthodes attachées"

	Le type d'un objet est important, car chaque objet dispose de méthodes. Les méthodes sont des fonctions spéciales, utilisable spécifiquement avec un type d'objet. Par exemple, les objet de type chaîne de caractères (type `str`) disposent d'une méthode `upper()` qui permet de convertir l'objet en majuscule.

	Recopier dans la console :

	```pycon
	>>> a = "Hello "
	>>> type(a)
	>>> a.upper()
	>>> a	
	```
	{{ terminal() }}

!!! info "False"

	Recopier dans la console :

	```pycon
	>>> proposition_1 = 1>3
	>>> type(proposition_1)
	>>> proposition_1
	```
	{{ terminal() }}

!!! info "True"

	Recopier dans la console :

	```pycon
	>>> proposition_2 = 1<3
	>>> type(proposition_2)
	>>> proposition_2
	```
	{{ terminal() }}

!!! abstract "Résumé"

    Les valeurs des variables sont stockées quelque part dans un emplacement non spécifié dans la mémoire de l’ordinateur sous forme de zéros et de uns.

	Notre programme n’a pas besoin de connaître l’emplacement exact où une variable est stockée; il peut simplement s’y référer par son nom.

	Ce que le programme doit savoir, c’est le type de données stockées dans la variable. Stocker un entier simple n’est pas la même chose que stocker une lettre ou un grand nombre à virgule flottante.

	Même s’ils sont tous représentés par des zéros et des uns, ils ne sont pas interprétés de la même manière et, dans de nombreux cas, ils n’occupent pas la même quantité de mémoire.


!!! info "Les types de données fondamentaux"

	Ce sont des types de base implémentés directement par le langage qui représentent les unités de stockage de base prises en charge de manière native par la plupart des systèmes. Ils peuvent principalement être classés en:
	<table>
	<tr>
  	<td style="width:100px">Type</td>
  	<td  style="width:100px">représente</td>
  	<td style="width:100px">exemples</td>
  	<td style="width:400px">remarque</td>
	</tr>
	<tr>
  	<td>str</td>
  	<td>chaîne de caractères</td>
  	<td>'A’  &nbsp;&nbsp;‘$’ &nbsp;&nbsp;'bonjour'</td>
  	<td>chaque caractère est codé sur 1 à 4 octet.</td>
	</tr>
	<tr>
  	<td>int</td>
  	<td>Entiers numériques</td>
  	<td>0&nbsp;&nbsp; -1&nbsp;&nbsp; 102</td>
  	<td>Ils existent en différentes tailles et peuvent être signés ou non signés, selon qu’ils supportent des valeurs négatives ou non.</td>
	</tr>
	<tr>
  	<td>float</td>
  	<td>Nombres à virgule flottante</td>
  	<td>3.14 ou 0.01</td>
  	<td>Dans de nombreux langages, on peut choisir le niveaux de précision, en fonction du type utilisé pour les trois types à virgule flottante.</td>
	</tr>
	<tr>
  	<td>bool</td>
  	<td>Booléen</td>
  	<td>True False</td>
  	<td>Ne peut représenter qu’un des deux états, vrai ou faux.</td>
	</tr>
	</table>

???+ question "Exécuter le code"

	{{IDE('scripts/python_print_1')}}


???+ question "Exécuter le code"

	Ci-dessus, tout s'est bien passé.
	🌵 Mais regardons maintenant : 

	{{IDE('scripts/python_print_2')}}

!!! abstract "À comprendre"

    👉 Le message d'erreur parle de **type(s)**  

	En effet les variables `prenom_1`, `prenom_2`, `age_2` sont en quelque sorte des "mots".  
	On le voit car ils sont entre guillemets. La variable `age_1` est un nombre.


!!! abstract "Différents types"

	📝 Il existe différents **types** de valeurs, et on doit toujours veiller à ne pas mélanger des choux avec des carottes.

	On sait facilement calculer 1 + 3 ou même 1 + 3.5 mais on ne sait que faire de 1 + "Albert"

	En effet 1 est un **nombre entier**, et "Albert" est une **chaine de caractère**.


???+ question "`Albert` et `"Albert"`"

	⚠️ Bien faire la différence entre `Albert` et `"Albert"`. 
	
	Exécuter ci-dessous

	{{IDE('scripts/albert')}}


??? success "Que s'est-il passé ?"

	Nous obtenons une erreur une erreur **`NameError`** qui nous indique que la **variable** `Albert` n'existe pas (pas définie).

    
	🐘 **Retenez bien** : 
    
	* Albert désigne une variable.
	* "Albert" désigne la chaine de caractère "Albert".

!!! info "L'opérateur `==`"

    `==` est utilisé pour déterminer si deux objets python ont la même valeur

	⚠️ Il ne faut pas confondre `=` qui sert à faire une affectation avec `==`

	Exécuter ci-dessous

	{{IDE('scripts/egalite')}}



???+ question "Égaux ou identiques ?"

	* Les nombres 1 et 1.0 sont égaux car ils ont la même valeur
	* 🌵 Les nombres 1 et 1.0 ne sont pas identiques car ils sont de types différents

	{{IDE('scripts/identiques')}}




{{multi_qcm(
    [
"""
Que vaut la variable `somme` après exécution du code ci-dessous ?

```python title=''
a = 1
b = 7
somme = 'a + b'
```

""", 
        ["8", "le code génère une erreur de type ValueError", "le code génère une erreur de type TypeError", " `'a + b'`" ], [4]
    ],
    [
"""
L'instruction ci-dessous est :

```python title=''
mot = 'arbre'
```
""", 
        ["une affectation", "une expression", "un calcul", "cette instruction ne signifie rien"], [1]
    ],
    [
"""
Le code ci-dessous :

```python title=''
a = 1
b = 2
a + b = 3
print(a + b)
```
""", 
        ["n'affiche rien", "génère une erreur", "affiche 3", "affiche `True`"], [2]
    ],
    [
"""
L'instruction ci-dessous  :

```python title=''
a = '5'
```
""", 
        ["n'a pas de sens", "est une affectation", "est une expression", "est une égalité"], [2]
    ],
	[
"""
L'instruction ci-dessous  :

```python title=''
a = 2
```
""", 
        ["n'a pas de sens", "est une affectation", "est une expression", "est une égalité"], [2]
    ],
	[
"""
Si on exécute le code ci-dessous, il s'affiche  :

```python title=''
a = '2'
b = a
print(a * b) 
```
""", 
        ["SyntaxError", "TypeError", "4", " `2*2`"], [2]
    ],
multi = False,
qcm_title = "QCM à refaire tant que nécessaire ...",
DEBUG = False,
shuffle = True,
)}}


## VI. Les variables : entrées et sorties au clavier

Un programme sert en général à quelque chose : il effectue une tâche précise, répétitive, et peut recommencer avec des données d'entrée différentes. Même un programme très simple peut interagir avec l'utilisateur. Par exemple :

???+ question "input()"

	Tester le script ci-dessous

    {{IDE('scripts/accueil')}}

???+ question "Tester le script ci-dessous"

    {{IDE('scripts/carre_de_16')}}

    Ce code va afficher le carré de `x` et ici `x = 16`.  
    Mais si on veux le carré d'un autre nombre il faut modifier le programme lui même, et ce n'est pas satisfaisant. 

???+ question "Tester le script ci-dessous"

	Nous allons utiliser `input` comme nous l'avons vu.

	{{IDE('scripts/input_str')}}

??? success "Que s'est-il passé ?"

	Nous obtenons un message d'erreur : 

	```python
	TypeError: unsupported operand type(s) for ** or pow(): 'str' and 'int'
	```

	Nous avons parler des type plus haut, et dans le code ci-dessous nous avons une erreur de type : TypeError

	Il est important de commencer à apprendre à lire les messages d'erreurs. L'interpreteur Python vous donne 2 informations :

	Le fichier en cause et le numéro de la ligne ou l'erreur s'est produite suivit de contenu de cette ligne.

	Une description de l'erreur composée de trois parties (séparées par **:**) :  
	1. TypeError : il s'agit donc d'une erreur de type  
	2. unsupported operand type(s) for `**` or pow() : les types des opérandes de la fonction puissance (pow() ou `**`) sont incorrects.  
	3. 'str' et 'int' : vous avez utilisé la fonction puissance avec `x` de type `str` et `2` de type `int`

	Conclusion: `x**2` n'a pas pu être évalué, car `x` est de type `str` alors qu'il aurait dû être de type `int`


!!! info "`x = input()` permet de lire une valeur au clavier, mais la valeur lue sera toujours de type `str` (chaîne de caractère)"

!!! info "Saisir des entiers : version 1"

	Evidement il est possible de saisir aussi des valeurs numériques! Mais il faut pour cela, convertir la valeur saisie dans le type souhaité. Python convertira si c'est possible :

	* si vous entrez "bonjour" et demandez de convertir en un nombre, vous aurez une erreur. 
	* mais si vous entrez 12, il lit "12" et vous pouvez convertir "12" en 12.

	Tester :

	{{IDE('scripts/input_int')}}

!!! info "La ligne `x = int(x)` convertit la chaine de caractère `x` en `int`"

	
 	
???+ question "Saisir des entiers et afficher : version 2"
	
	Tester :

	{{IDE('scripts/input_int_v2')}}

!!! info "On peut condenser en une seule ligne `x = int(input())`"

???+ question "Saisir des entiers et afficher : version 3"
	
	Tester :

	{{IDE('scripts/input_int_v3')}}

???+ question "Saisir des entiers et afficher : version 4"
	
	Tester :

	{{IDE('scripts/input_int_v4')}}

???+ question "À vous de jouer"
	
	Corriger l'erreur. Votre programme doit afficher l'âge de Monsieur Dupont qui est le double du vôtre.

	{{IDE('scripts/age_faux')}}



??? note pliée "Que s'est-il passé ?"

	Si `age`  est de type chaine de caractères, il est possible de faire `2 * age`. Cela revient en fait à faire `age + age`.  

	On dit qu'on a fait une **concaténation** des chaines de caractères. Cela revient tout simplement à les juxtaposer.  
		

??? success "Solution"

	{{IDE('scripts/age_juste')}}



## VII. Les opérateurs

### Avec les nombres

!!! info "Tester les opérateurs sur les nombres"

	Le type d'un objet est important, car chaque objet dispose de méthodes. Les méthodes sont des fonctions spéciales, utilisable spécifiquement avec un type d'objet. Par exemple, les objet de type chaîne de caractères (type `str`) disposent d'une méthode `upper()` qui permet de convertir l'objet en majuscule.

	Recopier dans la console :

	```pycon
	>>> 3 * 2
	>>> 3 ** 2
	>>> 38 / 5
	>>> 38 // 5	
	>>> 38 % 5
	```
	{{ terminal() }}

!!! info "Pré-requis : division euclidienne"

	Rappelons ce qu'est une division euclidienne

	![Division euclidienne](images/div_eucl.png){ width=50% }


!!! info "Opérations avec des nombres"

	<table>
	<tr style="background-color:#eeeeee;">
	<td style="width:200px;">Nom</td>
	<td style="width:200px;">Symbole mathématique</td>
	<td style="width:200px;">syntaxe python</td>
	<td style="width:200px;">Exemple</td>
	</tr>
	<tr><td>Addition</td><td>+</td><td>+</td><td>3 + 2 = 5</td></tr>
	<tr><td>Soustraction</td><td>-</td><td>-</td><td>3 - 2 = 1</td></tr>
	<tr><td>Multiplication</td><td>$\times$</td><td>*</td><td>3 * 2 = 6</td></tr>
	<tr><td>Division</td><td>/</td><td>/</td><td>7 / 2 = 3.5</td></tr>
	<tr><td>Division entière (quotient)</td><td></td><td>//</td><td>7 // 2 = 3</td></tr>
	<tr><td>Modulo (reste de la division entière)</td><td></td><td>%</td><td>7 % 2 = 1</td></tr>
	</table>	

!!! abstract "A savoir"

    Prenez bien note des opérateurs \*\*, % et //, qui sont d'usage très fréquent.
    
	* $3\ \%\ 2$ se lit $3$ **modulo** $2$.   
	Par exemple $38 = 5 \times 7+3$ donc $38\ \%\ 5$ renvoie 3.<br><br>

	* $3\ //\ 2$ se lit $3$ **division entière par** $2$.   
	Par exemple $38 = 5 \times 7+3$ donc $38\ //\ 5$ renvoie 7.<br><br>

	* $3$**\*\***$2$ se lit $3$ **puissance** $2$ 


???+ question "Sur papier"

    Effectuez, sans calculatrice et sans Python, les opérations suivantes 

	* 14 // 3 = 
	* 5 % 2 = 
	* 1237 % 2 =
	* 1238 % 2 = 

    ??? success "Solution"

        * 14 // 3 = 4
		* 5 % 2 = 1
		* 1237 % 2 = 1
		* 1238 % 2 = 0


???+ question "À vous de jouer"
	
	Evaluer mentalement le résultat, puis excuter pour vérifier

    {{IDE('scripts/operations')}}


???+ question "À vous de jouer"
	
	Faire vos propres essais.

	Evaluer mentalement le résultat, puis excuter pour vérifier

    {{IDE('scripts/operations_vide')}}
	

!!! info "La racine carré"

	Comment calculer par exemple $\sqrt{2}$ ? On a besoin de la **fonction racine carré**. En python, elle existe, mais il faut pour en disposer, **importer** un module : le module **math** (sans s)

	On dispose alors d'une fonction `sqrt()`, mais nous reparlerons des fonctions prochainement.

	Tester :

    {{IDE('scripts/racine')}}


### Avec des chaînes de caractères

!!! info "Opérations avec des chaînes de caractères"

	<table>
	<tr style="background-color:#eeeeee;">
	<td style="width:300px;">Nom</td>
	<td style="width:500px;">Exemple</td>
	</tr>
	<tr><td>Concaténation</td><td>"Bonjour " + "!" = "Bonjour !"</td></tr>
	<tr><td>Répétition</td><td>"Aie"*3 = "AieAieAie"</td></tr>
	</table>

???+ question "À vous de jouer"
	
	Exécuter, puis expliquer ce qui s'est passé

    {{IDE('scripts/caracteres_1')}}	

    ??? success "Solution"

	    `abc == "abc"` est évalué à `False` car abc vaut "bonjour", mais l'expression est valide.

???+ question "À vous de jouer"
	
	Exécuter, puis expliquer ce qui s'est passé

    {{IDE('scripts/caracteres_2')}}	

    ??? success "Solution"

	    `"abc" == bonjour` : ici l'expression ne peut pas être évaluée, **bonjour est une variable** (qui n'existe pas) => <font color=red><b>NameError</b></font>

???+ question "Sur papier"

    Evaluer les expressions suivantes sur papier, sans utiliser Python

	* `"a" + "b"`
	* `"hahaha" == 3 * "ha"`

    ??? success "Solution"

		* `"ab"`
		* `True`

!!! info "la chaîne vide"

	Pour les nombres, il existe le **zéro** qui est un nombre particulier.

	Pour les chaines de caractères, il y a une sorte de zéro : **la chaine vide**

	Et de la même façon que **zéro** est souvent utilisé en arithmétique, la chaine vide est aussi fréquement utile dans le monde des chaînes de caractères.

	La chaîne de caractères vide s’écrit `""`

### Bilan

!!! abstract "Résumé"

    * Bien retenir les opérateurs // et % qui donnent respectivement le quotient et le reste de la division euclidienne.

	* Retenir aussi : 

		* concaténation `"Aie" + "!"` qui donne `"Aie!"` 
		* et répétition `"Aie"*3` qui donne `"AieAieAie"`.



## VIII. Des erreurs

???+ question "Variable non définie"

	Tester puis corriger l'erreur

    {{IDE('scripts/non_def')}}
	

    ??? success "Solution"

	    Effectivement la variable `nombre_3` n'est pas définie.

	    On peut par exemple définir `nombre_3` avec une affectation

	    ```python
	    nombre_3 = 5
	    print(nombre_3)
	    ```

???+ question " Erreur d'indentation"

    Tester puis corriger l'erreur :

    {{ IDE('scripts/indentation') }}

    On appelle indentation les décalages vers la droite. En Python ils font partie de la syntaxe.


    ??? success "Solution"

        ```python
        entier = 4
        print(entier + 1)
        ```  

???+ question " Erreur de type"

	Tester puis corriger l'erreur :

    {{ IDE('scripts/type') }}


    ??? success "Solution"

 	    Vous effectuez la division d'une chaine par un entier, cela n'a pas de sens.

	    ```python
	    nombre = 3
	    print(nombre / 2)
	    ```

## IX. Exercices

???+ question "Exercice 1 : la fête"

    Ecrivez un code qui compte les têtes, les épaules, les genoux et les orteils à une fête. 

    Le correcteur va automatiquement définir une variable `personnes` pour vous, elle contiendra le nombre de personnes à la fête.

    Cette variable se trouve dans du code caché.

	Votre code doit définir quatre variables :

	* une appelée `tetes`
	* une appelée `epaules`
	* une appelée `genoux`
	* et une appelée `orteils`

	Ces variables doivent respectivement être égales au nombre de têtes, épaules, genoux et orteils à la fête. Votre programme ne doit pas générer de sortie (pas d'affichage)

    {{IDE('scripts/fete')}}

    ??? tip "Indice"

	    La première ligne de votre programme pourrait être :

	    ```python
	    tetes = 1 * personnes
	    ```
	    En effet chaque personne a une tête ...  

	
???+ question "Exercice 2 : la vitesse"

	Vous assistez à une course cycliste qui monte et descend une colline. Le correcteur automatique définira trois variables pour vous : `distance_montee`, `distance_descente` donnant la distance (en km) des deux parties de la course, et `temps_total` donnant le temps (en minutes) mis au total. 
	
	On veut écrire un programme qui indique la vitesse moyenne (en km/min) pour toute la course.
	
	Cliquez et glissez avec la souris pour réarranger les lignes qui sont en désordre.

    <iframe src="https://www.codepuzzle.io/IP3PJZ" width="100%" height="600" frameborder="0"></iframe>

    ??? tip "Indice"

    	Le programme a défini trois variables. Assurez-vous que les lignes soient dans le bon ordre afin que chaque nouvelle variable soit bien définie avant d'être utilisée.


???+ question "Exercice 3 : échange"

	Compléter ci-dessous pour échanger les contenus de deux variables `x` et `y`

    {{IDE('scripts/echange')}}


    ??? success "Solution"

	    ```python
	    x = int(input("saisir x : "))
	    y = int(input("saisir y : "))
	    print("Avant échange : ")
	    print("x = ", x)
	    print("y = ", y)

	    temp = y
	    y = x
	    x = temp

	    print("Après échange : ")
	    print("x = ", x)
	    print("y = ", y)
	    ```


{{ multi_qcm(
    [
"""
Quel message affiche l’ordinateur lorsque l'utilisateur saisit 8.5 ?

```python title=''
nombre = int (input('Saisir un nombre') )
double = nombre * 2
print(double)
```
""", 
        ["L'ordinateur affiche une erreur", "17", "16.0", "16"], [1]
    ],
    [
"""
On a exécuté le code suivant :

```python title=''
nombre_d_invites = 25
nombre_de_courriers = 11
print(nombre_de_courrier)
```
On obtient le message suivant : 

`Traceback (most recent call last):`  
`File '<module1>', line 3, in <module>`  
`NameError: name 'nombre_de_courrier' is not defined`

Que signifie ce message d'erreur ?
""", 
         ["On a fait une erreur de syntaxe", "On effectue une opération incorrecte entre deux valeurs de types différents", "Problème d'indentation du code"
      , "On utilise une variable non définie"], [4]
    ],
    [
"""
On a exécuté le code suivant :

```python title=''
cote = input('quel est la longueur du coté du carré ?')
aire = cote ** 2
print(aire)
```
On obtient le message suivant : 

`Traceback (most recent call last):`  
`File '<module1>', line 2, in <module>`  
`TypeError: unsupported operand type(s) for ** or pow(): 'str' and 'int'`

Que signifie ce message d'erreur ?
""", 
        ["On effectue une opération impossible entre deux valeurs de types différents", "On a mal indenté le code", "On a utilisé une variable qui n'est pas définie","On a fait une erreur de syntaxe"], [1]
    ],
    [
"""
On a exécuté le code suivant :

```python title=''
cote = 5
    aire = cote ** 2
print(aire)
```

Quel type de message d'erreur va-t-on obtenir ?  
""", 
        ['TypeError ', 'NameError', 'SyntaxError', 'IndentError'], [4]
    ],
	[
"""
Quel message affiche l’ordinateur lorsque l'utilisateur saisit 5 ?

```python title=''
nombre = input('Saisir un nombre')
triple = nombre * 3
print(triple)
```
""", 
         ["15", "15.0", "555", "nombrenombrenombre"], [3]
    ],
multi = False,
qcm_title = "QCM à refaire tant que nécessaire ...",
shuffle = True,
) }}




	

## X. Expressions et affectations

!!! example "Exemple"

    ```python title=''
	ma_variable = 1
	print(ma_variable)
	ma_variable =  2 * 7
	print(ma_variable)
	```


	Lorsque l'on écrit en python :
	```python title=''
	ma_variable = 1
	```

	ou bien avec une expression dans le membre de droite :
	```python title=''
	ma_variable = 2 * 7
	```

	👉 Si `ma_variable` n'existe pas déjà, l'interpréteur python fait 3 choses :

	**1.** Il évalue le membre de droite. (ici, il vaut trivialement 1 dans le 1er exemple, ou 14 dans le second)	
	
	**2.** dans un emplacement de la mémoire, il écrit la valeur. Cet emplacement est repréré par une adresse, par exemple, l'adresse 3005392 qui est un simple 
	numéro qui situe un emplacement dans la mémoire.

	**3.** il crée aussi une **variable**  nommée `ma_variable` qui est un mot utilisable dans le code, qui réfère à l'emplacement mémoire.

	Le programmeur n'a pas à se soucier de l'emplacement mémoire, il n'utilise que le nom de la variable.


	👉 Si au contraire, `ma_variable` existe déjà :


	**1.**  Il évalue le membre de droite.

	**2.** Dans l'emplacement de la mémoire correspondant à `ma_variable`, il écrit la valeur.   

	⚠️ Ce qui se trouvait avant à cet emplacement là est donc supprimé. (On dit parfois **"écrasé"**)


	👉 Ce n'est pas Python qui choisit l'emplacement mémoire, c'est le système d'exploitation qui gère cela.
	En fait python envoie une requête à l'OS pour stocker une valeur, l'OS choisit un emplacement vide, stocke la valeur et renvoie à 
	l'interpréteur python l'adresse mémoire utilisée.

!!! info "Expression"

	Une Expression est quelque chose que python peut évaluer. 

	!!! example "Exemple"

		```python title=''
		1 
		3 + 2
		3 + x # si la variable x est définie
		x + y # si les 2 variables sont définies
		```

	Mais nous en verrons bien d'autres qui ressembleront par exemple à :

	!!! example "Exemple"

		```python title=''
		fct(x)
		mot.find()
		```

	Toutes ces expressions peuvent être evaluées, c'est à dire que l'interpréteur python peut calculer la valeur de l'expression.


!!! abstract "Résumé - Expression"

    une expression est quelque chose qui peut être évalué par l'interpréteur python.


!!! info "Affectation"

	!!! example "Exemple"

	```python title=''
	entier = 1
	```

	Une affectation est faite dans toutes les instructions du type `ma_variable = expression`

	Le signe `=` n'a donc pas la même signification qu'en maths, il signifie **affecter** le résultat de lexpression de droite à la variable indiquée à gauche.

	Cela consiste donc à :

    * Evaluer le membre de droite
    * stocker le résultat dans un emplacement mémoire
    * Cette valeur sera par la suite accessible en utilisant le nom de la variable


???+ question "Adresses mémoires"

    Exécuter :

    {{ IDE('scripts/affect_adr_memoire') }}


!!! example "Situation 1 : ="

	!!! danger "Attention"

		Ne perdez pas de vue que le signe `=` est une affectation, et non une égalité.

	Les instructions comme :

	```python title=''
	x = 2 * x
	i = i + 1
	mot = "a" + mot
	```

	sont très fréquentes dans nos codes.

	* Python évalue le membre de droite
	* puis affecte la valeur à la variable à gauche du signe `=`

	Ainsi : `x = x + 1` n'a pas du tout le sens d'une égalité. Si la valeur de `x` était 4, par exemple :

	* on évalue x + 1 (ce qui vaut 5)
	* on affecte le résultat à x (donc maintenant x vaut 5)

!!! example "Situation 2 : = et =="

	!!! danger "Attention"

		Ne perdez pas de vue que le signe `=` est une affectation, et non une égalité.

	
	Voila une affection avec une **expression booléenne** :
	```python title=''
	vrai_ou_faux =  x == 2
	```

	* `==` est **un test d'égalité**.
	* `x == 2` est **une expression booléenne**.

	L'expression ```x == 2``` vaut ```True``` si `x` vaut `2` et ```False``` sinon.

	Rapellons que, lors d'une affectation :

	 * Python évalue l'expression à droite du signe `=`
	* puis affecte la valeur à la variable à gauche du signe `=`

	Ainsi ici, python évalue l'expression  ```x == 2``` puis affecte le résultat  à **la variable**  ```vrai_ou_faux```

	* ```vrai_ou_faux```prendra la valeur ```True``` si `x` est bien égal à `2`
	* Sinon, ```vrai_ou_faux```prendra la valeur ```False```
      


## XI. Bonnes pratiques de nommage

###  Ce qui est autorisé et ce qui ne l'est pas

Pour nommer correctement une variable, il existe des règles à respecter.

!!! abstract "Les règles"
    - le nom de la variable peut contenir les caractères suivants :
        - des lettres **non accentuées** (attention, minuscule et majuscule sont des caractères différents)
        - des chiffres (mais pas comme premier caractère)
        - le tiret du bas _ (underscore, tiret du 8)
    
    - le nom de la variable **ne doit pas** commencer par un chiffre
    - le nom de la variable **ne doit pas** contenir d'espace
    - le nom de la variable **ne doit pas** être un mot-clé du langage.

    ??? info "Liste des mots-clés réservés par Python"

        <p align="center">
        <table>
            <tr><td>and</td><td>as </td><td>assert	</td><td>break</td><td>	class</td><td>	continue</td><td>	def</td><td>	del</td></tr> 
            <tr><td>elif</td><td>	else</td><td>	except</td><td> False </td><td> finally	</td><td>for</td><td>	from</td><td>	global  </td></tr>
            <tr> <td> if </td><td>	import</td><td>	in</td><td>	is	</td><td>lambda	</td><td>None </td><td>not </td><td>	or</td></tr>
            <tr><td> pass </td><td>raise</td><td>	return</td><td>	True </td><td>try	</td><td>while</td><td>	with	</td><td>yield </td></tr>
        </table>
        </p>



###  Du sens, du sens, du sens

Hormis pour les indices (de boucles, de tableaux...) un nom de variable (dans un programme destiné à être lu, par vous ou quelqu'un d'autre) doit **impérativement avoir du sens** :

```python
# PAS BIEN
if d == 1:
    cep += vm

# BIEN
if date == 1:
    compte_epargne += versement_mensuel
```


!!! tip "Règle d'or :heart:" 
    On ne donne jamais un nom de variable au hasard, on le choisit pour qu'il soit **explicite**.


![image](data/memevar1.jpg){: .center width=40%}

Oui mais pour donner du sens, il faut souvent plusieurs mots...
La longueur du nom de la variable (*«c'est trop long à taper»*) n'est plus un problème depuis que la grande majorité des IDE propose la complétion automatique.  
Mais comment former ces longs mots ?


###  Syntaxe des noms à rallonge

!!! abstract "Comment accoler des mots"
    - S'il est composé, le nom peut être de la forme:
        - ```snake_case``` : les mots sont séparés par des underscores. Conseillé en Python.
        - ```camelCase``` : les mots sont séparés par des majuscules mais la 1ère lettre est minuscule. Conseillé en Javascript.
        - ```PascalCase``` : les mots sont séparés par des majuscules et la 1ère lettre est majuscule. Conseillé en C.
        - ```kebab-case``` : les mots sont séparés par des tirets courts. Conseillé en HTML - CSS. 

Sans surprise, en Python, nous utiliserons donc le ```snake_case```.

![image](data/smart.jpg){: .center width=40%}










    






