import turtle as t
import math


######### Partie ex 6 (drapeau Algoréen) ##############
def drapeau(x,y,c) :
    t.ht()
    w=400
    l=300
    t.animation("off")
    t.color("red")
    t.up()
    t.goto(-200,120)
    t.down()
    rectangle(w,l)
    t.goto(-200,-30)
    t.color("black")
    rectangle(300,150)
    star(x,y,c)
    t.done()
    
def rectangle(w,l) :
    t.begin_fill()
    for i in range(2) :
        t.forward(w)
        t.right(90)
        t.forward(l)
        t.right(90)
    t.end_fill()


def star(x,y,c) :
    t.up()
    t.goto(-200+x,125-y)
    t.down()
    t.color(c)
     
    # size of star
    size = 8
     
    # object color
    t.color(c)
     
    # object width
    t.width(4)
     
    # angle to form star
    angle = 120
     
    # color to fill
    t.fillcolor(c)
    t.begin_fill()
     
    # form star
    for side in range(5):
        t.forward(size)
        t.right(angle)
        t.forward(size)
        t.right(72 - angle)
         
    # fill color
    t.end_fill()
    
    

def star_position() :
    from random import randint
    x,y = randint(15,385) , randint(15,280)
    while (x<300 and 135<y<165) or (280<x<320 and y>150) :
        x,y = randint(15,385) , randint(15,280)
    return x,y 


########### partie ex8 (bibliothèque) ##############

class pendule() :
    
    def __init__(self,h,mn,size,haut_bas) :

        self.h = h
        self.mn = mn
        self.size = size
        self.haut_bas = haut_bas

    def dessine(self) :
        
        if self.haut_bas == 0 :
            # dessin au dessus de l'axe
            x = (self.h-8) *50 - 290 + self.mn/60 * 50
            y = 100
        else :
            x = (self.h-8) *50 - 290 + self.mn/60 * 50
            y = -50
            
        t.up()
        t.goto(x,y)
        t.fillcolor("#aaaaaa")
        t.pencolor("#000000")
        t.up()
        t.forward(self.size)
        t.down()
        t.left(90)
        t.pensize(6)
        t.begin_fill()
        for _ in range(360) :
            t.left(1)
            t.forward(2*math.pi*self.size/360)
        t.end_fill()

        t.right(90)
        t.up()
        t.backward(5)
        t.left(90)
        t.down()

        t.fillcolor("#ffffff")
        t.pencolor("#ffffff")

        t.begin_fill()
        for _ in range(360) :
            t.left(1)
            t.forward(2*math.pi*(self.size-5)/360)
        t.end_fill()


        t.right(90)
        t.backward(self.size-3)
        t.left(90)
        t.pencolor("#aaaaaa")
        t.down()

        t.pensize(3)

        # petite aiguile
        t.right(self.h/12*360)
        t.pencolor("#000000")
        t.forward(self.size-10)
        t.backward(self.size-10)
        t.left(self.h/12*360)
        # grande aiguile
        t.right(self.mn/60*360)
        t.pencolor("#000000")
        t.forward(self.size-5)
        t.backward(self.size-5)
        t.left(self.mn/60*360)
        

        
def trace_axe() :
    t.up()
    t.goto(-290,30)
    t.down()
    t.left(90)
    t.forward(10)
    t.backward(20)
    t.forward(10)
    t.right(90)
    for i in range(8,20) :
        t.forward(50)
        t.left(90)
        t.forward(10)
        t.backward(20)
        t.forward(10)
        t.right(90)
    t.left(90)
    t.forward(10)
    t.backward(20)
    t.forward(10)
    t.right(90)
    t.up()
    t.goto(0,0)
    t.down()

def intervalle(size,dt_h,dt_mn,haut_bas) :

    if haut_bas == 0 :
        t.up()
        t.backward(size)
        t.down()
        t.backward(20)
        t.right(90)
    else :
        t.up()
        t.forward(size)
        t.down()
        t.forward(20)
        t.right(90)
    
    # dessin du segment :
    l = dt_h * 50 + dt_mn * 5/6
    t.forward(l)
    
    t.left(90)
    if haut_bas==0 :
        t.forward(20)
    else :
        t.backward(20)
    t.right(90)
    
def dessin(h1,m1,h2,m2,size,haut_bas) :
    if haut_bas == 0 :trace_axe()
    else :t.right(90)
    import math
    
    # dessin du dessus :
    
    p = pendule(h1,m1,size,haut_bas)
    p.dessine()
    if haut_bas == 0 : t.color("red")
    else : t.color("blue")
    intervalle(20, h2-h1, m2-m1, haut_bas)
    
    p = pendule(h2,m2,size,haut_bas)
    p.dessine()
    
def time_in_library(bob,alice) :
        
    
    t.animation("off")

    size=20
    h1,m1,h2,m2 = bob
    dessin(h1,m1,h2,m2,size,0)
    h1,m1,h2,m2 = alice
    dessin(h1,m1,h2,m2,size,1)

    t.ht()
    t.done()
############### partie ex 7 (palet Algoréen) ###########
#####################################################################################################################
########################################          NE RIEN MODIFIER ICI         ######################################
#####################################################################################################################
######################################  CETTE PARTIE DU CODE CREE UN MODULE    ######################################
######################################           A PARTIR DU CODE ELEVE        ######################################
#####################################################################################################################

import sys

def get_code_in_history() :
    # recherche une ligne de code identifiant le code que l'on veux tester
    # renvoie l'indice de ce code dans la liste  __main__.In
    import __main__
    for i in range(-1,-len(__main__.In),-1) :
        if  __main__.In[i].split("\n")[0] == "# Ecrivez le code ici (et ne pas effacer cette ligne)" :
            return i
    print("vous devez exécuter votre code avant de faire le test !")
        

    
def cre_py(f) :
    import __main__
    code = __main__.In[get_code_in_history()].split("\n")

    for line in code[:-1] :
        if not "input" in line :
            line = line.replace("print","return")
            f.write( "\t"+line+"\n")

    f.close()
    return 1

    
def check_code() :
    
    # vérifie que le code est bien présent dans l'historique (a été exécuté par l'élèves)
    idx_code = get_code_in_history()
    if idx_code is None : 
        return 
    
    f = definition_fonction()
    cre_py(f)
    import sys,importlib
    sys.path.append('.')
    import code_eleve
    importlib.reload(code_eleve)
    vos_tests(code_eleve.run_code)
    
def show_code() :
    # fonction utilisée seulement pour la mise au point du module 
    f = open("code_eleve.py","r")
    code = f.readlines()
    for line in code : print(line,end="")
    
    
#####################################################################################################################
##########################################           PARTIE A MODIFIER         ######################################
#####################################################################################################################

        
def definition_fonction() :
    # crée un fichier code_eleve.py disponible dans la session
    # ce fichier contient une fonction unique, nommée run_code()
    # qui contient le code de l'élève.
    # run_code a autant de paramètres qu'il y a d'input dans le code.
    # les print du code élève sont remplacés par des return
    # les input sont ignorés et les variables correspondantes devront être 
    # passées en arguments de l'appel de run_code
    
    ########## ne pas modifier ces 3 lignes ########
    
    import __main__
    f = open("code_eleve.py","w")
    code = __main__.In[get_code_in_history()].split("\n")
    
    ##### ligne à adapter : ici on a 2 paramètres x et y, correspondant à 2 inputs, cela peu être différent...) ######
    f.write( "def run_code(x,y) :\n")
    return f

def soluce(x,y) : # <-- dans mon exemple, le code faisait 2 input, pour les variables x et y (vous adapterez)
    
    # votre solution de l'exercice (avec des return au lieu des print)

    import PIL 
    img = PIL.Image.open("plateau.png")
    if x<0 or x>90 or y<0 or y>80 :
        #print("blanc")
        return("blanc")
    r,g,b = img.getpixel( (x,y))
    if b>220  :
        return "bleu"
    elif r>250 and g < 200 :
        return "rouge"
    elif r-g <10 and r>b-30 :
        return "jaune"
    else :
        return "?"

def vos_tests(code_eleve) :
    # vous pouvez appeler soluce() et code_eleve.run_code_eleve() pour comparer les sorties
    
    # début des tests
    test_num = 1
    out = ""
    n_error = 0
    for x in (-19,7,12,17,30,47,55,65,87,95) :
      for y in (-10,5,15,40,50,58,68,100):
        
        ###### APPEL DU CODE ELEVE ET DE SOLUCE (adaptez les paramètres) ########
        out_eleve = code_eleve(x,y).strip()
        out_soluce = soluce(x,y)
        ###### traitement des comparaisons #####
        if out_eleve != out_soluce :
            out +="test n°"+str(test_num)+"\tx = "+str(x)+"\ty ="+str(y)+"\n"
            out +="sortie de votre code :"+out_eleve+"\nsortie attendue :"+out_soluce+"\n"
            out += "LE TEST A ECHOUE\n"
            out +="---------------------\n"
            n_error += 1

        test_num += 1
    
    ##### affichage des résultats des tests #####
    
    print("Nous avons effectué ",test_num,"tests de votre code")
    if n_error !=  0 :
        print(test_num-n_error," tests ont donné le bon rÃ©sultat")
        print("liste des tests qui ont échoués :")
        print("---------------------\n")
        print(out)
    else :
        print("Tous les tests ont donné le bon résultat.\nvotre code semble correct")
