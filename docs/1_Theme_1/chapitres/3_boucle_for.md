---
author: Gilles Lassus, Jean-Louis Thirot et Mireille Coilhac
title: Chapitre 1.3 - Boucle For (pour)
---


## I. de simples répétitions


!!! example "Exemple : la situation"

     Les enfants du village vous ont posé beaucoup de questions sur ce que font les enfants sur Terre. Ils ont été supris d'apprendre que, comme eux, les enfants terriens doivent aller à l'école. Et ils ont trouvé très étonnant que ceux-ci doivent lever le doigt pour demander la permission de parler en classe, car eux doivent pencher la tête pour cela.

    Ce que vous n'aviez pas prévu, c'est qu'une petite fille trouverait amusant de lever le doigt comme une terrienne. Sa maîtresse n'a pas du tout apprécié car, pour cette tribu, lever le doigt de cette manière est une grave insulte au grand sorcier ! La petite fille a été sévèrement punie et vous la retrouvez en pleurs. Vous vous sentez un peu responsable et décidez de l'aider à faire sa punition.

!!! info "Mon info"

    Ainsi, imaginons que l'on souhaite écrire 5 fois « Coucou ». On pourrait le faire avec le programme suivant :

    ```python title=''
    print("Coucou")
    print("Coucou")
    print("Coucou")
    print("Coucou")
    print("Coucou")
    ```

    C'est plutôt convenable ici… Mais si on veut effectuer par exemple 1 000 affichages, cela va devenir bien plus fastidieux !

    Pour plus d'efficacité, on aimerait indiquer directement que l'on souhaite répéter l'affichage en boucle, de la même manière qu'on pourrait écrire en français :

    Répéter 5 fois :   
    $\hspace{2em}$ Afficher "Coucou"

    Comme pour les instructions conditionnelles, on utilise l'indentation pour définir le bloc d'instructions qui doit être répété.

    En Python, pour répéter 5 fois l'instruction qui affiche « Coucou », on va écrire le programme ci-dessous.
    En effet, en anglais "loop" signifie "boucle". On peut considérer que la variable `loop` contient le numéro de la boucle que l'on est en train de réaliser.

    ```python title=''
    for loop in range(5):
        print("Coucou")
    ```

    qui va produire cette sortie :

    ```pycon title=''
    Coucou
    Coucou
    Coucou
    Coucou
    Coucou
    ```

    Au lieu de la variable `loop`, on trouve souvent la variable `i`. Cela donne :

    ```python title=''
    for i in range(5):
        print("Coucou")
    ```

    Pour coder la répétition, nous avons utilisé la structure suivante :

    ```python title=''
    for i in range(5):
        bloc d'instructions
    ```

    Vous pouvez donc écrire une boucle de cette façon, en indiquant le nombre de répétitions à la place du chiffre 5.

    Nous éluciderons par la suite les mystères de cette écriture. Prenez garde à ne pas oublier le deux-points `:` à la fin de la ligne. 


???+ question "Tester - 1"

    Recopier à la main l'exemple donné ci-dessus avec la variable `loop`

    {{ IDE() }}



???+ question "Tester - 2"

    Recopier à la main l'exemple donné ci-dessus avec la variable `i`

    {{ IDE() }}



???+ question "Tester - 3"

    👉 Remarque : vous trouverez aussi des codes comme celui-ci

    ```python title=''
    for _ in range(5):
        print("Coucou")
    ```

    Recopier à la main l'exemple donné ci-dessus avec la variable `_`

    {{ IDE() }}


???+ question "À vous de jouer - 1"

    Votre programme doit écrire 7 fois la phrase : "Je dois respecter le Grand Sorcier.", en plaçant cette phrase exactement une fois sur chaque ligne. Attention, si votre programme n'affiche pas exactement cette phrase avec les points et la majuscule là où il faut, il faudra tout recommencer.

    Important : votre programme ne doit pas faire plus de 2 lignes

    {{ IDE() }}

    ??? success "Solution"

        ```python title=''
        for i in range(7):
            print("Je dois respecter le Grand Sorcier.")
        ```

??? note "Erreurs fréquentes"

    Il est facile de se tromper dans les boucles lorsqu'on n'a pas l'habitude. 
    
    * Si on oublie le `:` à la fin de la ligne :
    
    <font color=red><b>SyntaxError: invalid syntax</b></font>

    * Si l'on oublie d'indenter :
    
    <font color=red><b>SyntaxError: expected an indented block</b></font>


    😊 Face à ce type d'erreur, on pensera donc à vérifier que le deux-points est bien présent et que l'indentation a été faite.


???+ question "Corrigez l'erreur"

    Testez puis corrigez la ou les erreurs dans le script suivant :

    {{IDE('scripts/erreurs')}}

    ??? success "Solution"

        ```python title=''
        for i in range(10):  # (1)
            print("J'ai tout compris 😊 !")  # (2)
        ```
    
        1. Ne pas oublier `:` en fin de ligne
    
        2. Ne pas oublier l'indentation (grâce à une tabulation)  

        !!! warning "Cliquer sur les + pour lire les explications"


## II. `for i in range(...)`



???+ question "Le gourmand insatiable"

    Bob découvre un beau fraisier....

    * Il cueille une fraise et la mange.  
    * Y prenant goût, il y retourne et cette fois prend 2 fraises.  
    * N'y tenant plus, il y retourne et prend cette fois 3 fraises.  
    
    Et ainsi de suite, à chaque fois, il ne peut pas s'en empêcher, il y retourne. Et à chaque fois il prend une fraise de plus que la fois précédente !

    👉 Testez ci-dessous. Quel est le problème ?

    {{IDE('scripts/fraises_1')}}

    ??? success "Solution"

        L'affichage produit n'est pas le bon.

        On voudrait obtenir : 
        ```pycon title=''
        Bob mange  1 fraises
        Bob mange  2 fraises
        Bob mange  3 fraises
        Bob mange  4 fraises
        Bob mange  5 fraises
        Bob mange  6 fraises
        Bob mange  7 fraises
        Bob mange  8 fraises
        Bob mange  9 fraises
        Bob mange  10 fraises
        ```

???+ question "Rectifier le code"

    Rectifier ci-dessous pour obtenir l'affichage désiré.

    {{IDE('scripts/fraises_2')}}


    ??? success "Solution 1"

        ```python title=''
        nombre = 1

        for i in range(10) :
            print("Bob mange ", nombre, "fraises")
            nombre = nombre + 1  # à chaque cueillette, il mange une fraise de plus
        ```

    ??? success "Solution 2"

        ```python title=''
        nombre = 0

        for i in range(10) :
            nombre = nombre + 1  # à chaque cueillette, il mange une fraise de plus
            print("Bob mange ", nombre, "fraises")      
        ```
  

???+ question "Tester for i in range"

    Tester puis répondre à la questions suivante : Quelles sont les valeurs prises par la variables `i` lorsqu'on exécute l'instruction    
    `for i in range(10)` ?

    {{IDE('scripts/range_10')}}


    ??? success "Solution"

        `i` prend successivement les valeurs 0, 1, 2, 3, 4, 5, 6, 7, 8 et 9.

        Cela correspond à **10** valeurs différentes.

!!! info "Mon info"

    🖐️ Dans les activités précédentes, vous avez écrit des codes contenant des boucles, servant à répéter des instructions.  
    Mais la variable, nommée **loop** ou plus simplement **i**, n'était pas utilisée dans la boucle.

    💡 Quand on veut simplement répéter plusieurs fois un bloc d'instruction, on n'a en effet pas besoin d'utiliser cette variable. Mais dans de très nombreux cas, cette valeur nous est très utile.

???+ question "À vous de jouer 2"

    Ecrire un code qui affiche : 

    ```pycon title=''
    i = 0  
    i = 1  
    i = 2
    ...  
    i = 49 
    i = 50
    ```

    {{IDE()}}

    ??? success "Solution"

        ```python title=''
        for i in range(51):
            print("i = ", i)
        ```

???+ question "Les fraises de Bob"

    Pour résoudre le problèmes des cueillettes de Bob, nous n'avions en réalité pas besoin de la variable `nombre`

    Tester : 

    {{IDE('scripts/fraises_3')}}


???+ question "Doit-on toujours démarrer à 0 ?"

    😊 **Non !**

    Il est possible de modifier cela. Dans l'exemple qui précède, à la première cueillette, il mange 1 fraise. Ce serait donc plus pratique si le compteur `i` démarrait à 1, non ?

    On peut le faire, mais attention à bien conserver 10 itérations....

    Tester ci-dessous :

    {{IDE('scripts/fraises_4')}}

    Ce code doit afficher, à chaque cueillette, combien Bob mange de fraises. Mais le code n'est pas correct :

!!! warning "Remarque"

    Nous avons modifié l'appel de **`range()`**.

    Au lieu de **`range(10)`** nous avons écrit **`range(1, 10)`**. 


    + au début `i` prend la valeur 1
    + A la fin  `i` prend la valeur 9 (comme avant, ça n'a pas changé), et pas à 10, il nous manque donc une cueillette...

??? success "Solution : le code correct pour 10 cueillettes"

    Tester ci-dessous : 

    {{IDE('scripts/fraises_5')}}

???+ question "Doit-on toujours incrémenter de 1 ?"

    😊 **Non !**

    **Bob est encore plus gourmand**

    😂 On va changer un peu les données du problème. A la première cueillette, Bob mange 5 fraises.
    A la seconde, 10 fraises.  
    A la suivante, 15 fraises.  
    Et à chaque cueillette, il augmente de 5....  

    Le nombre de fraises est donc 5, puis 10, puis 15 etc... 

    On pourrait facilement s'en sortir si on avait une variable `i` qui commence à 5, augmente de 5 en 5, et s'arrête à .... ???

    Là encore, attention à bien conserver 10 itérations....

    👉 Le plus simple es de se poser les 3 questions qui importent :

    * quelle est la valeur de départ ?
    * quelle est l'incrément ?
    * quelle est la valeur finale ?

    😀 Les 2 premières sont déjà résolues : on commence à 5 et on incrémente de 5.

    🌵 La troisième question est plus délicate. Combien de fraises mange-t-il à la 10ème cueillette ?

    La réponse est $50$ :

    * à la 1ere cueillette : $1*5$ fraises
    * à la 2ème cueillette : $2*5$ fraises
    * à la 3ème cueillette : $3*5$ fraises  
    etc...

    💡 Nous allons donc faire une boucle pour laquelle :

    * au début `i = 5`
    * incrémente de $5$
    * la dernière est `i = 50`

    Tester ci-dessous : 

    {{IDE('scripts/fraises_6')}}

    Modifiez la valeur 55 dans le code ci-dessus, et comprenez ce qu'il se passe :   
    
    Essayer par exemple : 51, 52, 54, 49 ...

!!! info "Exemple"

    {{IDE('scripts/exemple_pas')}}



## III. La syntaxe par l'exemple

???+ question "`for i in range(n)`"

    !!! warning inline end "Important"

        Prenez le temps de lire les commentaires !
    
        Cliquez sur les +
    

	```python title=''
	for i in range(4):  # (1) (2)
	   print(i)  
    ```

    1. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

    2. :warning: i prend les valeurs entières de l'intervalle [0; 4[
    
    Tester :
    
    {{IDE('scripts/range_1_argument')}}


???+ question "Table de multiplication"

    Tester :
    
    {{IDE('scripts/table_mult')}}


??? note "Améliorer la table de multiplications ? (Cliquer)"

    D'habitude, on ne commence pas à $0 \times nombre$, mais à $1 \times nombre$ 

    Tester :

    {{IDE('scripts/table_mult_1')}}

???+ question "`for i in range(a, b)`"

    !!! warning inline end "Important"

        Prenez le temps de lire les commentaires !
    
        Cliquez sur les +
    

    ```python title=''
    for i in range(2, 5):  # (1)
        print(i)  
    ```

    1. :warning: i prend les valeurs entières de l'intervalle [2; 5[
    
    Tester :
    
    {{IDE('scripts/range_2_arguments')}}

???+ question "`for i in range(a, b, pas)`"

    !!! warning inline end "Important"

        Prenez le temps de lire les commentaires !
    
        Cliquez sur les +
    

    ```python title=''
    for i in range(1, 10, 2):  # (1)
    print(i)  
    ```

    1. :warning: i prend les valeurs entières de l'intervalle [1; 10[ avec un pas de 2
    
    Tester :
    
    {{IDE('scripts/range_3_arguments')}}


???+ question "Parcours par éléments"

    !!! warning inline end "Important"

        Prenez le temps de lire les commentaires !
    
        Cliquez sur les +
    

    ```python title=''
    for lettre in "NSI":  # (1)
        print(lettre)  
    ```

    1. :warning: lettre prend comme valeurs successivement tous les caractères de "NSI"
    
    Tester :
    
    {{IDE('scripts/for_elements')}}


## IV. Exercices

???+ question "Exercice 1"

    **Question 1**

    Vous devez faire une boucle qui affiche les numéros suivants, Utilise une boucle `for i in range(...)` avec **un seul** argument. :

    ```pycon title=''
    3
    6
    9
    12
    15
    18
    ```

    {{ IDE() }}

    ??? success "Solution : Question 1"

        ```python title=''
        for i in range(6) :
            print("numéro :", 3 * (i + 1))
        ```

    **Question 2**        

    Vous devez faire une boucle qui affiche les numéros suivants, Utilise une boucle `for i in range(...)` avec **deux** arguments. :

    ```pycon title=''
    3
    6
    9
    12
    15
    18
    ```

    {{ IDE() }}

    ??? success "Solution : Question 2"

        ```python title=''
        for i in range(1, 7) :
            print("numéro :", 3 * i)
        ```
    

    **Question 3**

    Vous devez faire une boucle qui affiche les numéros suivants, Utilise une boucle `for i in range(...)` avec **trois** arguments. :

    ```pycon title=''
    3
    6
    9
    12
    15
    18
    ```

    {{ IDE() }}

    ??? success "Solution : Question 3"

        ```python title=''
        for i in range(3, 19, 3) :
            print("numéro :", i)
        ```
    

???+ question "Exercice 2"

    **Question 1 : initialisation : Choix de `a` et `n`**

    Exécuter le script ci-dessous pour observer les variables `a` et `n` qui ont automatiquement été choisies.  

    {{IDE('scripts/exo_for_1')}}

    **Question 2**

    Vous devrez déterminer la valeur de `a` à la fin de la boucle `for`, **juste en réfléchissant**. Il est recommandé de réaliser un tableau de déroulé.  
    Les variables `a` et `n` ont été choisies automatiquement (et affichées) lorsque vous avez exécuté le code du cadre précédant.

    Pour savoir si vous avez trouvé la bonne réponse, exécuter le script suivant, et saisir la réponse que vous avez trouvée.

    {{IDE('scripts/exo_for_1_suite')}}

    ??? success "Solution"

        i prend toutes les valeurs entières de 0 à n - 1, soit n valeurs.  
        A chaque itération a augmente de 2, donc en tout a augmente de 2 $\times$n.  
        En sortie de boucle a vaut donc a + 2 $\times$n.  



???+ question "Exercices 3 à faire **sur papier**"

    **1.** Question 1.   
    Ecrire un code Python qui affiche les entiers de 0 à 11 

    ??? success "Solution"

        ```python title=''
        for i in range(12): 
            print(i)
        ```

    **2.** Question 2.   
    Ecrire un code Python qui affiche les entiers de 10 à 21 

    ??? success "Solution"

        ```python title=''
        for i in range(10, 22) : 
            print(i)
        ```

    **3.** Question 3.   
    Ecrire un code Python qui affiche Les carrés des entiers entre 1 et 10 compris

    ??? success "Solution"

        ```python title=''
        for i in range(1, 11) :
            print(i ** 2)
        ```

{{ multi_qcm(
    [
"""
On donne le script suivant : 

```python title=''
somme = 0
for i in range(1, 6):
    somme = somme + i
print(somme)
```
Que s'affiche-t-il ?
""", 
    ["15", "21", "10", "0", "Autre"], [1]],
    multi = False,
    qcm_title = "Exercice 4 : QCM",
    shuffle = True,

) }}

??? success "Solution"

    Ce code fait la somme 0+1+2+3+4+5

???+ question "Exercice 5"

    Compléter le code suivant. A la fin la variable `somme` vaut la somme des entiers de 1 à 100 (compris).

    Aucun affichage n'est demandé.

    ⚠️ **N'oubliez surtout pas** de valider ![valider](images/valider.png){ width=3% } **après** avoir exécuté ![play](images/play.png){ width=3% }

    {{ IDE('scripts/somme_100') }}


???+ question "Exercice 6"

    Compléter le code suivant. A la fin la variable `produit` vaut le produit des entiers de 1 à 10 (compris).

    Aucun affichage n'est demandé.

    ⚠️ **N'oubliez surtout pas** de valider ![valider](images/valider.png){ width=3% } **après** avoir exécuté ![play](images/play.png){ width=3% }

    {{ IDE('scripts/produit_10') }}


???+ question "Exercice 7"

    Compléter le code suivant. A la fin la variable `somme` vaut la somme des carrés des entiers de 1 à 5 (compris).

    Aucun affichage n'est demandé.

    ⚠️ **N'oubliez surtout pas** de valider ![valider](images/valider.png){ width=3% } **après** avoir exécuté ![play](images/play.png){ width=3% }

    {{ IDE('scripts/somme_carres') }}




???+ question "Exercice 8"

    Ecrire un programme qui provoque l’affichage ci-dessous :  

    ![triangle dièses](images/fig_dieses.png){ width=10% }

    Il y a 20 lignes, et la vingtième contient 20 symboles `#`

    {{ IDE() }}

    ??? tip "Astuce"

        Recopier pour tester :  

        ```pycon title=''
        >>> "#" + "#"
        >>> 3 * "#"
        >>> 1 * "#"
        ```

        ⚠️ Attention : `0 * "#"` n'a pas de sens !

        {{ terminal() }}

    ??? success "Solution"

        ```python title=''
        for i in range(1, 21):
            print(i*"#")
        ```

???+ question "Exercice 9"

    Alice a déposé ses économies à sa banque sur un compte rémunéré.  

    Le but de cet exercice est d'écrire un programme qui demande de saisir le capital initial déposé, le taux $t$ en %, puis calcule le capital final, au bout de 5 ans, sachant qu’il a été placé au taux d’intérêts composé $t$%.  

    **1.**  Le programme doit afficher le capital final au bout de 5 ans.   

    ??? tip "Astuce"

        Indication  :  chaque année le capital est multiplié par $(1+\dfrac{t}{100})$

    Compléter le code ci-dessous :

    {{ IDE('scripts/capital') }}

    ??? success "Solution"

        ```python title=''
        capital_initial = int(input("Capital initial : "))
        t = int(input("taux en %. Par exemple saisir 2 pour un taux de 2% : "))
        capital = capital_initial
        for i in range(5):
            capital = capital*(1 + t/100)
        print(capital)
        ```

    **2.** Recopier, puis adapter ci-dessous le programme pour qu’il affiche le résultat au bout de `n` années, et non au bout de 5 ans.

    {{ IDE() }}

    ??? tip "Astuce"

        Indication  :  il faut une variable supplémentaire `n`

    ??? success "Solution"

        ```python title=''
        capital_initial = int(input("Capital initial : "))
        t = int(input("taux en %. Par exemple saisir 2 pour un taux de 2% : "))
        n = int(input("nombre d'années"))
        capital = capital_initial
        for i in range(n):
            capital = capital*(1 + t/100)
        print(capital)
        ```

    **3.** Recopier, puis adapter votre programme pour qu'il affiche aussi les intérêts acquis au bout de n années.

    ??? tip "Astuce"
        Indication : les intérêts acquis sont la différence entre le capital final au bout de n années et le capital initial.

    Vos affichages devront comporter des messages comme par exemple : "Capital au bout de 5 années :"

    {{ IDE() }}

    ??? success "Solution"

        ```python title=''
        capital_initial = int(input("Capital initial : "))
        t = int(input("taux en %. Par exemple saisir 2 pour un taux de 2% : "))
        n = int(input("nombre d'années"))
        capital = capital_initial
        
        for i in range(n):
            capital = capital*(1 + t / 100)
        print("Capital au bout de ", n, " années : ", capital)
        interets = capital - capital_initial
        print("Interets acquis : ", interets)
        ```












<!---
???+ question "TP 2"

    Après avoir téléchargé le fichier, vous pourrez le lire à partir de [Basthon](https://notebook.basthon.fr/){ .md-button target="_blank" rel="noopener" }

    🌐 TD à télécharger : Fichier `boucle_for_serie_2_sujet.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/boucle_for_serie_2_sujet.ipynb)

    ⏳ La correction viendra bientôt ... 
-->
<!---
???+ question "TP 2"

    <div class="centre">
    <iframe 
    src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/boucle_for_serie_2_sujet.ipynb"
    width="900" height="700" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

    😊 La correction est arrivée : 


    <div class="centre">
    <iframe 
    src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/boucle_for_serie_2_correction.ipynb"
    width="900" height="700" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>
    
-->

<!--- La correction à télécharger plus tard A SORTIR DE L'ADMONITION
⏳ La correction viendra bientôt ... 
👉 Bien sortir ce commentaire de l'admonition en supprimant l'indentation
🌐 Fichier `boucle_for_serie_2_correction.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/boucle_for_serie_2_correction.ipynb)
-->

<!---
???+ question "TP accumulateurs et compteurs"

    Après avoir téléchargé le fichier, vous pourrez le lire à partir de [Basthon](https://notebook.basthon.fr/){ .md-button target="_blank" rel="noopener" }

    🌐 TD à télécharger : Fichier `3_bis_accumulateurs_compteurs_sujet.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/3_bis_accumulateurs_compteurs_sujet.ipynb)

    ⏳ La correction viendra bientôt ... 

-->

<!--
???+ question "TP accumulateurs et compteurs"

    <div class="centre">
    <iframe 
    src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/accum_cpt_sujet.ipynb"
    width="900" height="700" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

    😊 La correction est arrivée : 

    <div class="centre">
    <iframe 
    src="https://notebook.basthon.fr/?from={{ page.canonical_url }}../a_telecharger/accum_cpt_corr.ipynb"
    width="900" height="700" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>
    
-->

<!--- La correction à télécharger plus tard A SORTIR DE L'ADMONITION

⏳ La correction viendra bientôt ... 
👉 Bien sortir ce commentaire de l'admonition en supprimant l'indentation
🌐 Fichier `accum_cpt_corr.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/accum_cpt_corr.ipynb)
-->


## V. Une situation classique : la double boucle

Il est très souvent utile d'imbriquer une boucle dans une autre, notamment lors du parcours de tous les pixels d'une image.
Prenons pour l'instant un exemple numérique.

!!! note "Exemple fondateur :heart:"
    Le programme suivant :

    ```python linenums='1'
    for a in range(1,5):
        for b in range(1,4):
            p = a * b
            print(a, '*', b, '=', p)
    ```
    va donner ceci :
    ```python
    1 * 1 = 1
    1 * 2 = 2
    1 * 3 = 3
    2 * 1 = 2
    2 * 2 = 4
    2 * 3 = 6
    3 * 1 = 3
    3 * 2 = 6
    3 * 3 = 9
    4 * 1 = 4
    4 * 2 = 8
    4 * 3 = 12
    ```
!!! aide "Analyse grâce à PythonTutor"
    <iframe width="800" height="300" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=for%20a%20in%20range%281,5%29%3A%0A%20%20%20%20for%20b%20in%20range%281,4%29%3A%0A%20%20%20%20%20%20%20%20p%20%3D%20a%20*%20b%0A%20%20%20%20%20%20%20%20print%28a,%20'*',%20b,%20'%3D',%20p%29%0A&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>

!!! note "Un deuxième exemple"
    Tester le programme suivant :


    ```python linenums='1'
    for i in range(3):
        print("Début du bloc d'instructions de la première boucle")
        for j in range(5):
            print(f'i = {i} et j = {j}')
    ```

{{ IDE() }}

???+ question "Exercice 10"

	Ecrire un programme qui affichera la table de multiplication des nombres de 1 à 10. Un simple affichage, ligne à ligne, suffira.
    Le programme doit donner ceci :

    ```python
    --- Table du  1 ---
    1 * 1 = 1
    1 * 2 = 2
    1 * 3 = 3
    1 * 4 = 4
    1 * 5 = 5
    1 * 6 = 6
    1 * 7 = 7
    1 * 8 = 8
    1 * 9 = 9
    1 * 10 = 10
    --------------------
    --- Table du  2 ---
    2 * 1 = 2
    2 * 2 = 4
    etc...
    ```

{{ IDE() }}


??? success "Solution"
 	```python linenums='1'
    for a in range(1,11):
        print("-"*3,"Table du ",a,"-"*3)
        for b in range(1,11):
            p = a * b
            print(a, '*', b, '=', p)
        print("-"*20)
    ```

???+ question "Exercice 11"

    Le but de cet exercice est de parcourir des listes et d'utiliser des boucles imbriquées. 


    **1.**  Comprendre le script ci-dessous.

    Recopier dans le terminal :

    ```python linenums='1'
    adultes = ['Papa', 'Maman', 'Mamie']
    for parent in adultes:
        print(parent) 
    ```
    Que fait ce script ?


	{{ IDE() }}

    ??? success "Solution : Analyse grâce à PythonTutor"
        <iframe width="800" height="500" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=adultes%20%3D%20%5B'Papa',%20'Maman',%20'Mamie'%5D%0Afor%20parent%20in%20adultes%3A%0A%20%20%20%20print%28parent%29&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=8&heapPrimitives=nevernest&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>

    **2.**  A l'aide des listes ci-dessous :
        ```python
        adultes = ['Papa', 'Maman', 'Mamie']
        enfants = ['Riri', 'Fifi', 'Loulou']
        ```
        Écrire un programme qui affiche :
    ```python
    Papa dit : « et une cuillère pour Riri ! »
    Papa dit : « et une cuillère pour Fifi ! »
    Papa dit : « et une cuillère pour Loulou ! »
    Maman dit : « et une cuillère pour Riri ! »
    Maman dit : « et une cuillère pour Fifi ! »
    Maman dit : « et une cuillère pour Loulou ! »
    Mamie dit : « et une cuillère pour Riri ! »
    Mamie dit : « et une cuillère pour Fifi ! »
    Mamie dit : « et une cuillère pour Loulou ! »
    ```

    {{ IDE() }}

    ??? success "Correction" 
        ```python linenums='1'
        adultes = ['Papa', 'Maman', 'Mamie']
        enfants = ['Riri', 'Fifi', 'Loulou']

        for parent in adultes:
            for enfant in enfants:
                print(parent, 'dit : « et une cuillère pour', enfant, '! »')   
        ```

    **3.**  Rajouter à la phrase précédente le contenu de la cuillère
        ```python
        adultes = ['Papa', 'Maman', 'Mamie']
        enfants = ['Riri', 'Fifi', 'Loulou']
        nourriture  = ['purée', 'compote']
        ```
        Écrire un programme qui affiche :
    ```python
    Papa dit : « et une cuillère de purée pour Riri ! »
    Papa dit : « et une cuillère de compote pour Riri ! »
    Papa dit : « et une cuillère de purée pour Fifi ! »
    Papa dit : « et une cuillère de compote pour Fifi ! »
    Papa dit : « et une cuillère de purée pour Loulou ! »
    Papa dit : « et une cuillère de compote pour Loulou ! »
    Maman dit : « et une cuillère de purée pour Riri ! »
    Maman dit : « et une cuillère de compote pour Riri ! »
    ```

    {{ IDE() }}

    ??? success "Correction" 
        ```python linenums='1'
        for parent in ['Papa', 'Maman', 'Mamie']:
            for enfant in ['Riri', 'Fifi', 'Loulou']:
                for nourriture in ['purée', 'compote']:
                    print(parent, 'dit : « et une cuillère de', nourriture, 'pour', enfant, '! »')
        ```

## VI Un peu de graphisme...

???+ question "Exercice 12"
    Travail sur ```ipythonblocks``` :

    ![image](data/blocks.png){: .center width=40%}
    
    Exercice à faire sur Capytale : [https://capytale2.ac-paris.fr/p/basthon/n/?kernel=python3&mode=create&id=3980184](https://capytale2.ac-paris.fr/p/basthon/n/?kernel=python3&mode=create&id=3980184){. target="_blank"}

    ??? success "Correction" 

    Coorection sur Capytale : [https://capytale2.ac-paris.fr/p/basthon/n/?kernel=python3&mode=create&id=3980225](https://capytale2.ac-paris.fr/p/basthon/n/?kernel=python3&mode=create&id=3980225){. target="_blank"}
        


## VII. Bilan

!!! abstract "À retenir ❤"
    
    * La boucle `for ... in ...` s'utilise lorsque :
        * on veut parcourir un à un les éléments d'un objet itérable (une chaîne de caractère, une liste, un tuple, un dictionnaire...)
        * on veut répéter une action un nombre de fois connu à l'avance. On parle de boucle bornée.

    * Les instructions répétées peuvent **_mais ce n'est pas obligatoire_** faire appel à la variable de boucle, mais il ne faut pas que ces instructions la modifient.

    * Ne pas oublier les **`:`** et l'indentation !

    * `range(n)` génère une séquence de `n` nombres entiers : on s'en servira dès qu'on aura besoin de répéter `n` fois des instructions.

