---
author: Richardot C./Nowakowski D.
title: Accueil
---


## Thèmes en cours d'étude
!!! abstract "PROGRESSION"

    === "T1 Démarrer en Python"
        **Thème 1 : Démarrer en Python**

        - 1.1 [Variables (FAIT)](./1_Theme_1/chapitres/1_intro_python/)
        - 1.2 [Instruction conditionnelle If (FAIT)](./1_Theme_1/chapitres/2_tests/)
        - 1.3 [Boucle For (FAIT)](./1_Theme_1/chapitres/3_boucle_for/)
        - 1.4 [Accumulateurs et compteurs (FAIT)](./1_Theme_1/chapitres/4_accumulateurs_compteurs/)
        - 1.5 [Boucle While (FAIT)](./1_Theme_1/chapitres/5_while/)
        - 1.6 [Fonctions - découverte (FAIT)](./1_Theme_1/chapitres/6_1_decouverte_fonctions/)
        - 1.7 [Fonctions - Généralités](./1_Theme_1/chapitres/6_2_fonctions/)
        - 1-8 [Compléments Python (A LIRE)](./1_Theme_1/chapitres/complements/1_modules_python/)

    === "T2 Représentation des données"
        **Thème 2 :  Représentation des données**

        - 2.1 [Listes](./2_Theme_2/01_chapitre_1/cours/)



## Programme


!!! abstract "Programme"
    Nous traiterons les différentes entrées du programme dans les chapitres suivants :

    === "T1 Démarrer en Python"
        **Thème 1 : Démarrer en Python**

        - 1.1 [Variables](./1_Theme_1/chapitres/1_intro_python/)
        - 1.2 [Instruction conditionnelle If](./1_Theme_1/chapitres/2_tests/)
        - 1.3 [Boucle For](./1_Theme_1/chapitres/3_boucle_for/)
        - 1.4 [Accumulateurs et compteurs](./1_Theme_1/chapitres/4_accumulateurs_compteurs/)
        - 1.5 [Boucle While](./1_Theme_1/chapitres/5_while/)
        - 1.6 [Fonctions - découverte](./1_Theme_1/chapitres/6_1_decouverte_fonctions/)
        - 1.7 [Fonctions - Généralités](./1_Theme_1/chapitres/6_2_fonctions/)
        - 1-8 [Compléments Python](./1_Theme_1/chapitres/complements/1_modules_python/)


    === "T2 Représentation des données"
        **Thème 2 :  Représentation des données**

        - 2.1 [Listes](./2_Theme_2/01_chapitre_1/cours/)
        - 2.2 [Tuples](./2_Theme_2/02_chapitre_2/cours/)
        - 2.3 [Dictionnaires](./2_Theme_2/03_chapitre_3/01_quesako/)
        - 2.4 [Bases](./2_Theme_2/04_chapitre_4/cours/)
        - 2.5 [Booléens](./2_Theme_2/05_chapitre_5/cours/)
        - 2.6 [Codage des caractères](./2_Theme_2/06_chapitre_6/cours/)
        - 2.7 [Codage des entiers](./2_Theme_2/07_chapitre_7/cours/)
        - 2.8 [Codage des non-entiers](./2_Theme_2/08_chapitre_8/cours/)

    === "T3 Architecture matérielle"
        **Thème 3 : Architecture matérielle**

        - 3.1 [Microbit](./3_Theme_3/01_chapitre_1/cours/)
        - 3.2 [Architecture Von Neumann](./3_Theme_3/02_chapitre_2/cours/)
        - 3.3 [Architecture réseau](./3_Theme_3/03_chapitre_3/cours/)
        - 3.4 [Protocoles de communication dans un réseau](./3_Theme_3/04_chapitre_4/cours/)
        - 3.5 [Découverte des commandes Linux](./3_Theme_3/05_chapitre_5/cours/)

    === "T4 Algorithmique"
        **Thème 4 : Algorithmique**

        - 4.1 [Extremums et moyennes](./4_Theme_4/01_chapitre_1/cours/)
        - 4.2 [Complexité](./4_Theme_4/02_chapitre_2/cours/)
        - 4.3 [Tri par insertion](./4_Theme_4/03_chapitre_03/cours/)
        - 4.4 [Tri par sélection](./4_Theme_4/04_chapitre_4/cours/)
        - 4.5 [Dichotomie](./4_Theme_4/05_chapitre_5/cours/)
        - 4.6 [Algorithmes gloutons](./4_Theme_4/06_chapitre_06/cours/)
        - 4.7 [Algorithme KNN](./4_Theme_4/07_chapitre_07/cours/)

    === "T5 Traitement de données"
        **Thème 5 : Traitement de données**

        - 5.1 [Manipulation de fichiers csv](./5_Theme_5/01_chapitre_1/cours/)
        - 5.2 [Les requêtes](./5_Theme_5/02_chapitre_2/cours/)
        - 5.3 [Trier des données](./5_Theme_5/03_chapitre_3/cours/)
        - 5.4 [Fusions](./5_Theme_5/04-chapitre_4/cours/)
        - 5.5 [Utilisation du module Pandas](./5_Theme_5/05_chapitre_5/cours/)

    === "T6 IHM sur le web"
        **Thème 6 : IHM sur le web**

        - 6.1 [Les langages HTML5 et CCS3](./6_Theme_6/01_chapitre_1/cours/)
        - 6.2 [Le Javascript](./6_Theme_6/02_Chapitre_2/cours/)
        - 6.3 [Protocole HTTP : étude du chargement d'une page web](./6_Theme_6/03_chapitre_3/cours/)
        - 6.4 [Requêtes GET, POST et formulaires](./6_Theme_6/04_chapitre_4/cours/)

### Site de référence


Les différentes pages du site ont pour origines : 

- [https://mcoilhac.forge.apps.education.fr/site-nsi/](https://mcoilhac.forge.apps.education.fr/site-nsi/)

- [https://glassus.github.io/](https://glassus.github.io/)

- [https://nreveret.forge.apps.education.fr/donnees_en_table/](https://nreveret.forge.apps.education.fr/donnees_en_table/)